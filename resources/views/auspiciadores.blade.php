@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')

@push('css')
  <style>
    .s-pageheader{
      margin-bottom: -100px;
    }


    /********************/
    body{
      margin-top: 100px;

      line-height: 1.6
    }
    .container{
      width: 800px;
      margin: 0 auto;
    }



    ul.tabsp{
      margin: 0px;
      padding: 0px;
      list-style: none;
    }
    ul.tabsp li{
      background: none;
      color: #222;
      display: inline-block;
      padding: 10px 15px;
      cursor: pointer;
    }

    ul.tabsp li.current{
      background: #ededed;
      color: #9e2840;
    }

    .tab-content{
      display: none;
      background: #fff;
      padding: 15px;
      color: #666;;
    }

    .tab-content.current{
      display: inherit;
    }
    #page_wrap{
      max-width: 1160px;
    }
    .s-pageheader--home{
        min-height: 100px;
        height: 120px;
        padding-top: 0px;
        margin-bottom: 0px !important;
      }

  </style>
  <style>
    .slider-container {
        margin-top:35px;
        height:100%;
    }

    .carousel-captions {
        position: absolute;
        top: 0;
        text-align: left;
        left: inherit;
        right: inherit;
        width: 100%;
        color:#000;
        bottom:-95px;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-control {
        background: none repeat scroll 0 0 #000 !important;
        bottom: 0;
        color: #fff;
        font-size: 20px;
        height: 50px;
        opacity: 0.5;
        position: absolute;
        
        right: 108px !important;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
        top: 296px;
        width: 70px;
        z-index: 1 !important;
        width: 40px;
    }

    #sliderpri {
        padding-left: 0px;
        padding-right: 0px;
    }

    .lrslider1 {
        margin-top: -600px;
    }
    

    #slider_captionspri {
        top: -250px;
    }
    .sltxt1 {
        font-size: 34px;
    }
    .sltxt2 {
        font-size: 21px;
    }
    .imgslide {
        height: 420px !important;width: 700px;
    }
    
    #page_wrap3 {
        height: 420px;margin-top: 0px;
    }
    .s-pageheader {
        margin-bottom: -50px;
    }
    #top {
       margin-top: 0px;
    }
    @media only screen and (min-width:1300px) {
        #page_wrap3 {
            margin-top: 25px;
        }
    }
    @media only screen and (max-width:1300px) {
        .contendedorpat{
            width: 94%;
        }
        #page_wrap{
            width: 94%;padding-left: 20px;padding-right: 20px;
        }
        .post-thumb{
            width: 25% !important;
        }
        .post-title{
            width: 73% !important;
        }
        .descpat{
            width: 73% !important;
        }
    }
    @media only screen and (min-width:1200px) {
        .carousel-control{
            top: 370px !important;
        }
    }
    @media only screen and (max-width:1200px) {
          .andes {
              margin-left: 0%;
              margin-right: 0%;
              width: 100% !important;
          }
        .carousel-control{
            top: 390px !important;
        }
        #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 900px;
            height: 380px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
       #page_wrap3 {
            height: 385px;
        }
        .post-thumb{
            width: 25% !important;
        }
        .post-title{
            width: 73% !important;
        }
        .descpat{
            width: 73% !important;
        }
    }
    @media only screen and (max-width:1166px) {
        #page_wrap{
            margin-top: 40px;
        }
    }
    @media only screen and (max-width:1100px) {
        .post-thumb{
            width: 15% !important;
        }
        .post-title{
            width: 83% !important;
        }
        .descpat{
            width: 83% !important;
        }
         #page_wrap3 {
            height: 380px;
        }
        .post-thumb{
            width: 35% !important;
        }
        .post-title{
            width: 63% !important;
        }
        .descpat{
            width: 63% !important;
        }

    }
    @media only screen and (max-width:1000px) {
        .carousel-control{
            top: 405px !important;
        }
        #expert{
            width: 90% !important;margin-left: 5%;margin-right: 5%;
        }

         #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
        .imgslide {
            max-width: 1000px;
            height: 350px !important;
            width: 100% !important;
        }
        .header__nav-wrap{
            margin-top: 0px;
        }
        .tb.andes{
            margin-left: 0px;margin-right: 0px;
            width: 100% !important;
        }
        #page_wrap3 {
            height: 345px;
        }
        .post-thumb{
            width: 40% !important;
        }
        .post-title{
            width: 58% !important;
        }
        .descpat{
            width: 58% !important;
        }
    }
    @media only screen and (max-width:990px) {
        .header__nav-wrap{
            margin-top: 50px;
        }
        #page_wrap{
            margin-top: 100px;
        }
    }

    @media only screen and (max-width:900px) {
        .carousel-control{
            top: 405px !important;
        }
        .lrslider {
            margin-top: -500px !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }

        .imgslide {
            max-width: 900px;
            height: 340px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -105px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        #page_wrap{
            margin-top: 100px !important;
        }
        #page_wrap3 {
            height: 335px;
        }
        .post-thumb{
            width: 50% !important;
        }
        .post-title{
            width: 48% !important;
        }
        .descpat{
            width: 48% !important;
        }
        .desc-res{
          font-size: 13px;
          margin-top: 0px;
        }
        
    }

    @media only screen and (max-width:800px) {
        .desc-acordeon{
            width: 100% !important;
            float: none  !important;
        }
        .mapa-acordeon{
            width: 100% !important;
            float: none  !important;
        }
        .carousel-control{
            top: 400px !important;
        }
        .lrslider {
            margin-top: -450px !important;
        }
        .imgslide {
            max-width: 800px;
            height: 330px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .header {
            top: -90px;
        }
        #top {
            margin-top: 100px;
        }
        #page_wrap{
            margin-top: 0px !important;
        }
        .s-pageheader--home{
            min-height: 15px;
            height: 15px;
        }
        #page_wrap3 {
            height: 310px;
        }
        
    }
    
    @media only screen and (max-width:767px) {
        .header__nav-wrap{
            margin-top: 0px;
        }
    }

    @media only screen and (max-width:700px) {
        .carousel-control{
            top: 450px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
        .imgslide {
            max-width: 700px;
            height:300px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .post-thumb{
            width: 50% !important;
        }
        .post-title{
            width: 48% !important;
        }
        .descpat{
            width: 48% !important;
        }
    }

    @media only screen and (max-width:600px) {
        .carousel-control{
            top: 470px !important;
        }
       
        .imgslide {
            max-width: 600px;
            height: 225px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .post-thumb{
            width: 100% !important;
        }
        .post-title{
            width: 100% !important;
        }
        .descpat{
            width: 100% !important;
        }
         #page_wrap3 {
           height: 150px;
            margin-top: 50px;
            margin-top: 0px !important;
        }
        .resumens{
          display: block;
          display: -webkit-box;
          max-width: 100%;
          height: 49px;
          margin: 0 auto;
          line-height: 1;
          -webkit-line-clamp:3;
          -webkit-box-orient: vertical;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 16px;
        }
        .suspensiv{
            display: initial !important;
        }
    }
    @media only screen and (max-width:500px) {
        .carousel-control{
            top: 500px !important;
        }
        .imgslide {
            max-width: 600px;
            height: 180px !important;
            width: 100% !important;
        }
    }

    @media only screen and (max-width:400px) {
        .carousel-control {
            top: 85px !important;
        }

        #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 400px;
            height: 150px !important;
            width: 100% !important;
        }
        .lrslider1{
            margin-top: -180px;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
        }
        #slider_captionspri {
            top: 150px !important;
        }
        #page_wrap3 {
           height: 150px;
            margin-top: 50px;
            margin-top: 0px !important;
        }
        #tab-1{
            padding-left: 0px;
            padding-right: 0px;
        }
        
    }

    .left carousel-control {
        left: 5%;
    }
    
    .slider {
    max-width: 700px;
    }
    
</style>
@endpush

@section('menu')
    @include("fixed.menu2")
@endsection


@section('content')
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left;">
      <a class="sinsub">{{$dir}}</a>
  </h3>
</div>
<div class="contendedorpat" style="max-width: 1200px;margin-left: auto;margin-right: auto;padding-left: 20px;padding-right: 20px;">
  <!-- s-content
  ================================================= -->
  <div class="block-1-2 block-m-full popular__posts col-sm-12" style="padding-left: 0px !important;padding-right: 0px !important;">
      <div class="accordion-list col-sm-12" style="padding-left: 0px;padding-right: 0px;">
        @foreach($patrocinadores as $p)
          <div class="accordion indigo" id="ausp{{$p->id}}">
              <div class="accordion-title" style="cursor: pointer;">
                <div style="margin-top: 0px;">              
                    <div class="post-container">                
                      <div class="post-thumb" style="width: 15%;">
                        @foreach($p->images->where('type','p') as $i)
                            <img class="imgp" src="<?=Croppa::url(asset($i->url), 470,272)?>" / style="width: 100%; height: auto;">
                        @endforeach
                            
                      </div>
                      <a style="width: 100%;">
                        <div class="post-title" style="font-size: 18px;color: #4b4b4b;width: 83%;">{{$p->name}}</div>
                      </a>
                      <div class="descpat" style="font-size: 16px;color: #474646cc;width: 83%;float: left;margin-left: 10px;font-size: ;">
                        <p class="desc-res" style="font-family: calibri;font-weight: normal;padding-right: 10px;">
                          <span class="resumens">{{substr($p->description, 0,425).'... '}}</span><span class="suspensiv" style="display: none;">...</span>
                          <a class="vermas" name="{{$p->id}}" style="font-style: italic; font-weight: bold;font-size: 13px">Ver más</a>
                        </p>
                      </div>
                    </div>
                </div>
              </div>
              <div class="accordion-content" style="margin-top: -1px;">
                  <div class="container" style="width: 100%;padding-left: 0px;padding-right: 0px;margin-left: 0px !important;margin-right: 0px !important;max-width: 1200px;">
                      <ul class="tabsp" style="border-bottom: 1px solid #91d8f6;margin-top: -20px;">
                        <li class="tab-link current" data-tab="tab-1{{$p->id}}" style="padding-bottom: 0px;padding-top: 0px;font-size: 16px;font-family: calibri;">
                          <span class="hidden-xs">Fotos</span><i class="fa fa-picture-o hidden-lg hidden-md hidden-sm"></i>
                        </li>
                        <li class="tab-link" data-tab="tab-2{{$p->id}}" style="padding-bottom: 0px;padding-top: 0px;font-size: 16px;font-family: calibri;">
                          <span class="hidden-xs">Información</span><i class="fa fa-map-marker hidden-lg hidden-md hidden-sm"></i>
                        </li>
                         <li class="tab-link descdiv" data-tab="tab-3{{$p->id}}" style="padding-bottom: 0px;padding-top: 0px;font-size: 16px;font-family: calibri;">
                          <span class="hidden-xs">Descripción</span><i class="fa fa-info hidden-lg hidden-md hidden-sm"></i>
                        </li>
                        <i class="fa fa-close close-btn pull-right" style="margin-top: 8px;margin-bottom: 0px;margin-left: 0px;font-size: 17px;"></i>
                      </ul>
                      <div id="tab-1{{$p->id}}" class="tab-content current">
                        <div id="page_wrap3" class="slider">
                            <div id="sliderpri{{$p->id}}" class="carousel slide col-sm-12  padd-lt0" data-ride="carousel" data-interval="10000">
                                <div class="carousel-inner">
                                      @php
                                          $i=0;
                                      @endphp
                                      @foreach($p->images as $img)
                                        <div class="item {{($i == 0)?'active':''}}">
                                            <img alt="{{$img->id}} slide" src="<?=Croppa::url(asset($img->url), 700,420)?>" class="imgslide" style="max-height: 700px; max-width: 700px; width: 100% !important; height: 100%">   
                                        </div>
                                        @php
                                            $i++;
                                        @endphp
                                      @endforeach
                                    
                                </div>
                                <div class="col-sm-12 col-md-12 col-xs-12 lrslider1">
                                    <a data-slide="prev" role="button" class="left carousel-control" href="#sliderpri{{$p->id}}">
                                        <span class="glyphicon glyphicon-chevron-left" style="left: 10px;top: 15px;"></span>
                                    </a>
                                    <a data-slide="next" role="button" href="#sliderpri{{$p->id}}" class="right carousel-control" style="float: left;padding-right: 0px !important;right: 0px !important;">
                                        <span class="glyphicon glyphicon-chevron-right" style="left: 10px;top: 15px;"></span>
                                    </a>
                                </div>
                                

                            </div>


                        </div>

                      </div>
                      <div id="tab-2{{$p->id}}" class="tab-content">
                        <div class="desc-acordeon" style="float: left;width: 50%;display: inline-block;">
                           <div style="font-size: 16px;font-family: calibri;"><strong>Dirección:</strong> {{$p->address}}</div>
                           <div style="font-size: 16px;font-family: calibri;"><strong>Teléfono 1:</strong> {{$p->telephone1}}</div>
                           <div style="font-size: 16px;font-family: calibri;"><strong>Teléfono 2:</strong> {{$p->telephone2}}</div>
                           <div style="font-size: 16px;font-family: calibri;"><strong>Página web:</strong>
                              <a href="{{$p->web}}">{{$p->web}}</a>
                           </div>
                           <div style="font-size: 16px;font-family: calibri;"><strong>Email:</strong> {{$p->email}}</div>
                        </div>
                        <div class="mapa-acordeon" style="float: right;width: 50%;display: inline-block; max-width: 540px;">
                          @php 
                          echo html_entity_decode($p->ubication);
                          @endphp
                        </div>
                      </div>
                       <div id="tab-3{{$p->id}}" class="tab-content" style="font-family: calibri;font-size: 16px;">
                         {{$p->description}}
                       </div>
                  </div><!-- container -->
              </div>
          </div>
        @endforeach
      
        <!--button class='close-btn'>Close All</button-->
      </div>
  </div> <!-- end popular_posts -->
</div>  
@endsection

@push('js')
<script type="text/javascript">
    $("#sliderpri").on('slide.bs.carousel', function(evt) {

        var step = $(evt.relatedTarget).index();

        $('#slider_captionspri .carousel-captions:not(#captions-'+step+')').fadeOut('fast', function() {

            $('#captions-'+step).fadeIn();

        });

    });

    $("iframe").css('width', '100%');
    $("iframe").css('height', 'auto');
</script>
<script>
  $(".accordion-title").click(function(){
    let $content = $(this).next();
    if(!$content.hasClass("open")){
      let $opened = $(".accordion-content.open");
      $opened.removeClass("open");
      $content.addClass("open");

        var tab_id = $content.find('ul.tabsp li').attr('data-tab');
        $('ul.tabsp li').removeClass('current');
        $('.tab-content').removeClass('current');

        $('ul.tabsp li').addClass('current');
        $("#"+tab_id).addClass('current');
        
    }
  });

  $(".close-btn").click(function(){
    $(".accordion-content.open").removeClass("open");
  })

  /*-----------------*/
  function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    
$(document).ready(function(){
    var provId = getParameterByName('id');
    if (provId > 0) {
        $('html,body').animate({
            scrollTop: $("#ausp"+provId).offset().top
        }, 1000);

        let $content = $("#ausp"+provId).find('.accordion-title').next();
        if(!$content.hasClass("open")){
          let $opened = $(".accordion-content.open");
          $opened.removeClass("open");
          $content.addClass("open");

            var tab_id = $content.find('ul.tabsp li').attr('data-tab');
            $('ul.tabsp li').removeClass('current');
            $('.tab-content').removeClass('current');

            $('ul.tabsp li').addClass('current');
            $("#"+tab_id).addClass('current');
            
        }
    }

  
  $('ul.tabsp li').click(function(){
    var tab_id = $(this).attr('data-tab');
    $('ul.tabsp li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })

})
$('.vermas').click(function(event) {
    $('ul.tabsp li').removeClass('current');
    $('.tab-content').removeClass('current');

    $('.descdiv').addClass('current');
    $("#tab-3"+event.target.name).addClass('current');
});

</script>
@endpush