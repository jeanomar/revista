
<div id="modal" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            {!! Form::open(['method'=>'POST', 'route'=>'auspiciadores', 'id'=>"articleform", 'class'=>'form-horizontal' ]) !!}
                <div class='modal-body'>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Nombre</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Nombre' type='text' name='name' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Web</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Web' type='text' name='web' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Email</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Email' type='text' name='email' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Teléfono 1</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Teléfono 1' type='text' name='tlf1' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Teléfono 2</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Teléfono 2' type='text' name='tlf2' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Dirección</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Dirección' type='text' name='address' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Ubicación</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Ubicación google maps' type='text' name='ubication' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Estado</label>
                        <div class='col-sm-10'>
                            <input type='checkbox' name='state' checked value="1"> Activo</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Descripción</label>
                        <div class='col-sm-10'>
                            <textarea class='form-control' rows="10" name='descripcion' required></textarea>
                        </div>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                    <button type='submit' class='btn btn-flat bg-aqua'>Aceptar</button>
                </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>


<div id="modal_img" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class='form-horizontal'>
                <div class='modal-body'>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Patrocinador</label>
                        <div class='col-sm-10'>
                            <label class='form-control' name='title'></label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Imágenes</label>
                        <div class='col-sm-10'>
                            <!-- The file upload form used as target for the file upload widget -->
                            <form id="fileupload"  method="POST" enctype="multipart/form-data">
                                @csrf
                                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                <noscript><input type="hidden" name="redirect" ></noscript>
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-10">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Agregar fotos...</span>
                                            <input type="file" name="files[]" multiple>
                                        </span>
                                        <button type="submit" class="btn btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Subir</span>
                                        </button>
                                        <button type="reset" class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancelar</span>
                                        </button>
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Eliminar</span>
                                        </button>
                                        <input type="checkbox" class="toggle">
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                    </div>
                                    <!-- The global progress state -->
                                    <div class="col-lg-2 fileupload-progress fade">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <!-- The extended global progress state -->
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table id="tb_imgs" role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                    <button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>
                </div>
            </div>  
        </div>
    </div>
</div>


<div id="modalpub" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class='modal-body'>
                <div class='form-group col-md-12'>
                    <label class='col-sm-3 col-md-3 control-label'>Patrocinador</label>
                    <div class='col-sm-9 col-md-9'>
                        <input class='form-control inp' type='text' name='patrocinador' disabled>
                        <input class='form-control inp' type='hidden' name='patrocinadoridp'>
                    </div>
                </div>
                <div class="clearfix"></div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne" style="padding: 0;">
                            <h4 class="panel-title">
                       
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Listado de publicidades
                                                <a id="btn-tambahp" class="btn btn-default pull-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="padding-top: 4px;padding-bottom: 4px;margin-top: -5px;">
                                                    Nueva publicidad <i class="glyphicon glyphicon-new-window"></i>
                                                </a>
                                            </div>
                                                                          
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTable2"> 
                                                    <thead>
                                                        <tr> 
                                                            <th></th> 
                                                            <th>Nombre</th> 
                                                            <th>Imagen</th> 
                                                            <th>Accionesa</th> 
                                                        </tr> 
                                                    </thead> 
                                                    <tbody id="detPublicidad"> 

                                                        <tr> 
                                                            <td scope="row"></td> 
                                                            <td style="max-width: 100px">
                                                                <p class="resumen" ></p>
                                                            </td> 
                                                            <td>
                                                                <img src="" style="max-width: 80px;height: auto;">
                                                            </td> 
                                                            <td class="center">
                                                                

                                                            </td> 
                                                        </tr>
                                                        
                                                    </tbody> 
                                                </table>
                                                
                                                <!-- /.table-responsive -->
                                                                                
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                            
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-heading" role="tab" id="headingTwo" style="padding: 0;">
                                <h4 class="panel-title">
                                    <button type="button" class="close"  data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">&times;</button>
                                </h4>
                            </div>
                            <div class="panel-body">
                                {!! Form::open(['method'=>'POST', 'route'=>'publicidad.save', 'id'=>"articleformp", 'class'=>'form-horizontal', 'enctype'=>"multipart/form-data", 'files' => true ]) !!}
                                    <input class="idf" type="hidden" name="id">
                                    <input type="hidden" name="patrocinadorid">
                                    <div class="panel-body">
                                    
                                        <div class='form-group col-md-12'>
                                            <label class='col-sm-3 col-md-3 control-label'>Nombre de publicidad</label>
                                            <div class='col-sm-9 col-md-9'>
                                                <input class='form-control inp' placeholder='Nombre de publicidad' type='text' name='title' required>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="col-sm-3 col-md-3 control-label">Lugar</label>
                                            <div class="col-md-9 col-sm-9">
                                                <label class="col-md-6">
                                                    <input type="radio" id="statei0" name="statei" value="0" onclick="show1();"  checked/> Artículo
                                                </label>
                                                <label class="col-md-6">
                                                    <input type="radio" id="statei1" name="statei" value="1" onclick="show2();" /> Inicio
                                                </label>
                                            </div>
                                        </div>
                                        <div id="div1" >
                                           
                                            <div class='form-group col-md-12'>
                                                <label class='col-sm-3 col-md-3 control-label'>Ubicación</label>
                                                <div class='col-sm-9 col-md-9'>
                                                    <select name="statea" class="form-control inp">
                                                        <option value="0">Inferior</option>
                                                        <option value="1">Derecha</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class='form-group col-md-12'>
                                                <label class='col-sm-3 col-md-3 control-label'>Articulo</label>
                                                <div class="form-group input-group col-sm-9" style="margin-left: 0px;margin-right: 0px;padding-left: 15px;padding-right: 15px;">
                                                    <span class="input-group-addon">{{$_SERVER['SERVER_NAME']}}/articulo/ver/</span>
                                                    <input class='form-control inp' type='text' name='articleid' placeholder="0" required>
                                                </div>
                                            </div>   
                                        </div>
                                        <div id="div2" style="display: none;">
                                            <div class='form-group col-md-12'>
                                                <label class='col-sm-3 col-md-3 control-label'>Posición</label>
                                                <div class='col-sm-9 col-md-9'>
                                                    <input class='form-control inp' placeholder='0' type='text' name='position' required >
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class='form-group col-md-12'>
                                            <label class='col-sm-3 col-md-3 control-label'>Estado</label>
                                            <div class='col-sm-9'>
                                                <label>
                                                <input type='checkbox' name='statep' checked value="1"> Activo</label>
                                            </div>
                                        </div>
                                        <div class='form-group col-md-12'>
                                            <label class='col-sm-3 col-md-3 control-label'></label>
                                            <div class="col-sm-9">
                                                <div class="file-upload" style="width: 100%;">
                                                  <button class="file-upload-btn d3" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Agregar imagen</button>

                                                  <div class="image-upload-wrap">
                                                    <input name="imgdatos" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                                    <div class="drag-text">
                                                      <h3>Arrastra y suelta un archivo o selecciona Agregar imagen</h3>
                                                    </div>
                                                  </div>
                                                  <div class="file-upload-content">
                                                    <img name="img_datos" class="file-upload-image" src="#" alt="your image" style="width: 100%;"/>
                                                    <div class="image-title-wrap">
                                                      <button type="button" onclick="removeUpload()" class="remove-image d3">Quitar <span class="image-title">imagen</span></button>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='form-group col-md-12'>
                                            <a class='btn btn-default pull-right' data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="margin-right: 15px;">Cancelar</a>
                                            <button id="btnSaveP" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" type='button' class='btn btn-flat bg-primary pull-right' style="margin-right: 15px;">Guardar</button>
                                        </div>
                                    </div>
                                          
                                {!!Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                                                                
            </div>
            <div class='modal-footer'>
            </div>
            
        </div>
    </div>
</div>
