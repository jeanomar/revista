<?php

namespace App\Http\Controllers;
use Omar\Entities\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos =  Menu::withTrashed()->where('menu_id',null)->whereNotIn('id', [1,5,6,7])->get();
        return view('admin.menu', compact('datos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->get('id')) {
            $dato = Menu::find($request->get('id'));
            $dato->menu_id = $request->get('menu_id'); 
	        $dato->title = $request->get('title');
	        $dato->url = $request->get('title');
        }else{
            $dato = new Menu;
            $dato->menu_id = $request->get('menu_ids');
            $dato->title = $request->get('titles');
	        $dato->url = $request->get('titles');
        }

        $dato->description = "";
        $dato->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p =Menu::withTrashed()->find($id);
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        return back();
    }
}
