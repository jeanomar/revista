@extends('admin.layouts.plantilla')
@push('css')
    <!-- DataTables CSS -->
    <link href="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet" />

    <style>
        .resumen{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis; 
        }
    </style>
    <link href="{{ asset('admin/dist/css/imgupload.css') }}" rel="stylesheet" />
@endpush
@section('content')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gastronomía</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Listado de platos típicos
                            <button id="btn-tambah" class="btn btn-default pull-right" style="padding-top: 4px;padding-bottom: 4px;margin-top: -5px;">
                                Nuevo plato <i class="glyphicon glyphicon-new-window"></i>
                            </button>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($platos as $item)
                                    <tr >
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td><p class="resumen" style="width: 300px">{{$item->descripcion}}</p></td>
                                        <td>{{($item->deleted_at == null)?'Activo':'Inactivo'}}</td>
                                        <td class="center" style="min-width: 40px;">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['gastronomia.state', $item->id], 'class' => 'FormDeleteTime form-group']) !!}
                                                @if($item->deleted_at == null)
                                                    <button type="button" id="btn-edit" class="btn btn-default btn-circle btn-xs" title="Editar" data-id="{{$item->id}}" data-name="{{$item->name}}" data-descripcion="{{$item->descripcion}}" data-state="{{$item->deleted_at}}" data-url="{{$item->url}}"> 
                                                        <i class="fa fa-pencil"></i>
                                                    </button>

                                                @endif

                                                {!! Form::button('<i class="fa '.(($item->deleted_at == null)?'fa-ban':'fa-check-circle ').'"></i>', array('type' => 'submit', 'class' => 'specialButton btn btn-default btn-circle btn-xs', 'title'=>(($item->deleted_at == null)?'Desactivar':'Activar') )) !!}

                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
  
    @include("admin.modals.gastronomia_modal")

@endsection

@push('js')
<!-- DataTables JavaScript -->
    <script src="{{ asset('admin/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('admin/js/jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    

    <script>
         $('#dataTable').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]],
            createdRow:function(row, data, dataindex){
                if (data[3]== 'Inactivo') {
                    $(row).css({'color':"rgb(150, 0, 0)"});
                }
            }
        });

        $(".FormDeleteTime").submit(function (event) {
            var x = confirm("Desea "+(($(this).data("item")==0)?'activar':'desactivar')+' este patrocinador?');
            if (x) {
                return true;
            }
            else {
                event.preventDefault();
                return false;
            }
        });
        /************************************************/

if ($('.file-upload-image').attr('src') != "") {
      $('.image-upload-wrap').hide();

      $('.file-upload-content').show();

}

function readURL(input) {
    if (input.files[0].size/1024 > 2000) {
        removeUpload();
        alert("Solo imagenes menores a 2 Mb.");
        $('.image-title').html('Error');
    }
    else if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image-upload-wrap').hide();

          $('.file-upload-image').attr('src', e.target.result);
          $('.file-upload-content').show();

          $('.image-title').html(input.files[0].name);
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload();
    }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
});


    </script>
    
    <script src="{{ asset('admin/js/sets/gastronomia.js') }}"></script>
   
@endpush

