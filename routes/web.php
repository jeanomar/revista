<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses'=>'InicioController@index',
    'as'=>'index'
]);

Route::get('/search', [
    'uses'=>'InicioController@search',
    'as'=>'search'
]);

Route::post('/send', [
    'uses'=>'InicioController@send',
    'as'=>'send'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth', 'prefix'=>'articulos'], function(){
    Route::get('/',[
        'uses'=>'ArticleController@listado',
        'as'=>'articulos'
    ]);

    Route::post('/save',[
        'uses'=>'ArticleController@store',
        'as'=>'articulos'
    ]);

    Route::delete('/state/{id}',[
        'uses'=>'ArticleController@state',
        'as'=>'articulos.state'
    ]);

    Route::post('/img/upload',[
        'uses'=>'ArticleController@imgUpload',
        'as'=>'articulos.imgs.upload'
    ]);

    Route::get('/img/upload',[
        'uses'=>'ArticleController@getimg',
        'as'=>'articulos.imgs.upload'
    ]);

    Route::post('/img/principal',[
        'uses'=>'ArticleController@principal',
        'as'=>'articulos.imgs.principal'
    ]);

    Route::post('/editimg',[
        'uses'=>'ArticleController@editimg',
        'as'=>'articulos.imgs.editimg'
    ]);

    Route::resource('pictures', 'ArticleController', ['only' => ['destroy']]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'auspiciadores'], function(){
    Route::get('/',[
        'uses'=>'AuspiciadorController@index',
        'as'=>'auspiciadores'
    ]);

    Route::post('/save',[
        'uses'=>'AuspiciadorController@store',
        'as'=>'auspiciadores'
    ]);

    Route::delete('/state/{id}',[
        'uses'=>'AuspiciadorController@destroyp',
        'as'=>'auspiciadores.state'
    ]);

    Route::post('/img/upload',[
        'uses'=>'AuspiciadorController@imgUpload',
        'as'=>'auspiciadores.imgs.upload'
    ]);

    Route::get('/img/upload',[
        'uses'=>'AuspiciadorController@getimg',
        'as'=>'auspiciadores.imgs.upload'
    ]);

    Route::post('/img/principal',[
        'uses'=>'AuspiciadorController@principal',
        'as'=>'auspiciadores.imgs.principal'
    ]);

    Route::resource('picturesp', 'AuspiciadorController', ['only' => ['destroy']]);

    
});

Route::group(['middleware'=>'auth', 'prefix'=>'larevista'], function(){

    Route::get('editar',[
        'uses'=>'ArticleController@larevista',
        'as'=>'larevista.editar'
    ]);

    Route::post('update',[
        'uses'=>'ArticleController@larevistaupdate',
        'as'=>'larevista.update'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'home'], function(){

    Route::post('update',[
        'uses'=>'HomeController@terminosypoliticasupdate',
        'as'=>'terminosypoliticas.update'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'menu'], function(){
    Route::get('/',[
        'uses'=>'MenuController@index',
        'as'=>'menu'
    ]);

    Route::post('/',[
        'uses'=>'MenuController@store',
        'as'=>'menu'
    ]);

    Route::delete('/{id}',[
        'uses'=>'MenuController@destroy',
        'as'=>'menudel'
    ]);

});

Route::group(['middleware'=>'auth', 'prefix'=>'travel'], function(){

    Route::get('datosutiles',[
        'uses'=>'TravelController@datosutiles',
        'as'=>'travel.datosutiles'
    ]);

    Route::get('atractivosdentro',[
        'uses'=>'TravelController@atractivosdentro',
        'as'=>'travel.atractivosdentro'
    ]);

    Route::post('atractivosdentro',[
        'uses'=>'TravelController@atractivosdentrosave',
        'as'=>'travel.atractivosdentro'
    ]);

    Route::get('atractivosfuera',[
        'uses'=>'TravelController@atractivosfuera',
        'as'=>'travel.atractivosfuera'
    ]);

    Route::post('atractivosfuera',[
        'uses'=>'TravelController@atractivosfuerasave',
        'as'=>'travel.atractivosfuera'
    ]);

    Route::get('gastronomia',[
        'uses'=>'TravelController@gastronomia',
        'as'=>'travel.gastronomia'
    ]);

    Route::post('gastronomia',[
        'uses'=>'TravelController@gastronomiasave',
        'as'=>'travel.gastronomia'
    ]);

    Route::get('festividades',[
        'uses'=>'TravelController@festividades',
        'as'=>'travel.festividades'
    ]);

    Route::post('festividades',[
        'uses'=>'TravelController@festividadessave',
        'as'=>'travel.festividades'
    ]);

    Route::post('climaupdate',[
        'uses'=>'TravelController@climaupdate',
        'as'=>'clima.update'
    ]);

    Route::post('accesoaereo',[
        'uses'=>'TravelController@accesoaereo',
        'as'=>'accesoaereo.update'
    ]);
    Route::post('accesoter',[
        'uses'=>'TravelController@accesoter',
        'as'=>'accesoter'
    ]);
    Route::delete('deletedato/{id}',[
        'uses'=>'TravelController@delete',
        'as'=>'deletedato'
    ]);

    Route::post('distancias',[
        'uses'=>'TravelController@distancias',
        'as'=>'distancias'
    ]);

    Route::post('imgdatos',[
        'uses'=>'TravelController@imgdatos',
        'as'=>'imgdatos'
    ]);

    Route::delete('/atractivostate/{id}',[
        'uses'=>'TravelController@destroyatractivo',
        'as'=>'atractivos.state'
    ]);

    Route::delete('/gastronomiastate/{id}',[
        'uses'=>'TravelController@destroygastronomia',
        'as'=>'gastronomia.state'
    ]);

    Route::delete('/festividadesstate/{id}',[
        'uses'=>'TravelController@destroyfestividad',
        'as'=>'festividades.state'
    ]);

});

Route::group(['middleware'=>'auth', 'prefix'=>'publicity'], function(){

    Route::get('/',[
        'uses'=>'PublicityController@index',
        'as'=>'publicity.index'
    ]);

    Route::get('/show',[
        'uses'=>'PublicityController@show',
        'as'=>'auspiciadores.show'
    ]);

    Route::delete('/publicidadstate/{id}',[
        'uses'=>'PublicityController@destroy',
        'as'=>'publicidad.state'
    ]);

    Route::post('/',[
        'uses'=>'PublicityController@store',
        'as'=>'publicidad.save'
    ]);
});

Route::get('patrocinadores',[
    'uses'=>'InicioController@auspiciadores',
    'as'=>'patrocinadores'
]);

Route::get('larevista',[
    'uses'=>'InicioController@larevista',
    'as'=>'larevista'
]);

Route::get('terminos/{id}',[
    'uses'=>'InicioController@terminosypoliticas',
    'as'=>'terminos'
]);

Route::get('politicas/{id}',[
    'uses'=>'InicioController@terminosypoliticas',
    'as'=>'terminos'
]);

Route::group(['prefix'=>'articulo'], function(){
    Route::get('/',[
        'uses'=>'ArticleController@index',
        'as'=>'articulo'
    ]);
    Route::get('ver/{id}',[
        'uses'=>'ArticleController@ver',
        'as'=>'articulo.ver'
    ]);

});  

Route::group(['prefix'=>'categoria'], function(){

    Route::get('ver/{id}',[
        'uses'=>'ArticleController@verxcategory',
        'as'=>'categoria.articulo'
    ]);

    Route::get('subcategoria/{id}',[
        'uses'=>'ArticleController@verxcategorydetail',
        'as'=>'categoria.subcategoria'
    ]);
});    
    
