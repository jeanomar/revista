
var vm = new Vue({
    el:"#wrapper",

    data: {
        cantTotal:0,
        carritoHeader:[],
        carCookie:[]
    },
    methods:{
        prueba: function(){
            var vm = this;
            vm.cantTotal++;
        },

        getCarHeader: function(ids){

            var url = window.location.host;
            var vmh=this;
            var route= "//"+url+"/getCarritoHeader";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'GET',
                datatype: 'json',
                data: {ids: ids},
                success:function(response){
                    for(var i = 0; i < response.length; i++) {
                        for (var j = vmh.carCookie.length - 1; j >= 0; j--) {
                            if (vmh.carCookie[j].id == response[i].id) {
                                vmh.carritoHeader.push({"id":response[i].id, "name":response[i].name, "precio":response[i].precio, "descuento":response[i].descuento, "imagen":response[i].imagen, "cantidad":vmh.carCookie[j].cantidad, "recomendador":vmh.carCookie[j].recomendador});
                            }
                        }
                    }
                },
                error:function(msj){

                }
            });  
        }

    },
    created () {
        
        $('#dataTables-example').DataTable({
            responsive: true
        });
    }

});