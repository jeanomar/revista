<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableImgPatrocinador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('img_patrocinador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1000);
            $table->string('url', 500);
            $table->enum('type',['p','s'])->default('s');
            $table->integer('patrocinador_id')->unsigned()->nullable();
            $table->foreign('patrocinador_id')->references('id')->on('patrocinador')->onDelete('set null');
            $table->enum('state',[1,0])->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('img_patrocinador', function (Blueprint $table) {
            //
        });
    }
}
