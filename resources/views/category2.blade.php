@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')
@push('css')
    <style>
      .s-pageheader--home{
        min-height: 100px;
        height: 120px;
        padding-top: 0px;
      }
      #page_wrap {max-width: 1160px;}
    </style>
@endpush

@section('menu')
    @include("fixed.menu2")
@endsection
 
@section('content')
@php
    $caracteres_subtitle=150;
@endphp
<div id="page_wrap" class="limit">
    <h3 class="tb andes" style="text-align: left;">
        <a class="sinsub">{{$dir}}</a>
    </h3>
        @foreach($sub as $s)
          <div id="zona_3" class="portafolio">
            @php
              $articulos = $s->articulos;
            @endphp
            <div id="editors">
            @if($articulos->count() >= 1)
              <article class="impar">
                <a href="{{URL::action('ArticleController@ver', $articulos[0]->id) }}">
                     @foreach($articulos[0]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url({{asset($im->url)}}); -webkit-filter: grayscale(0%);"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $articulos[0]->id) }}" class="sinsub">
                            @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $articulos[0]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena1}}</div>
                        @if($cadena2 != "")
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena2}}</div>
                        @endif
                        <div id="excerpt" class="rn" style="margin-top: 5px;">
                            {{substr($articulos[0]->subtitle,0,$caracteres_subtitle).((strlen($articulos[0]->subtitle)>$caracteres_subtitle)?"...":"")}}
                        </div>
                    </a>
                    <div class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $articulos[0]->category->id) }}" class="sinsub" style="font-size: 14px;font-weight: bold;">
                            {{$articulos[0]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @endif
            @if($articulos->count() >= 2)
            <article class="par">
                <a href="{{URL::action('ArticleController@ver', $articulos[1]->id) }}">
                    @foreach($articulos[1]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url({{asset($im->url)}}); -webkit-filter: grayscale(0%);"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $articulos[1]->id) }}" class="sinsub">
                            @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $articulos[1]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena2}}</div>
                        <div id="excerpt" class="rn" style="margin-top: 5px;">
                            {{substr($articulos[1]->subtitle,0,$caracteres_subtitle).((strlen($articulos[1]->subtitle)>$caracteres_subtitle)?"...":"")}}
                        </div>
                    </a>
                    <div class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $articulos[1]->category->id) }}" class="sinsub" style="font-size: 14px;font-weight: bold;">
                            {{$articulos[1]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @endif
            @if($articulos->count() >= 3)
            <article class="impar">
                <a href="{{URL::action('ArticleController@ver', $articulos[2]->id) }}">
                     @foreach($articulos[2]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url({{asset($im->url)}}); -webkit-filter: grayscale(0%);"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $articulos[2]->id) }}" class="sinsub">
                         @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $articulos[2]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena2}}</div>
                        <div id="excerpt" class="rn" style="margin-top: 5px;">
                            {{substr($articulos[2]->subtitle,0,$caracteres_subtitle).((strlen($articulos[2]->subtitle)>$caracteres_subtitle)?"...":"")}}
                        </div>
                    </a>
                    <div class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $articulos[2]->category->id) }}" class="sinsub" style="font-size: 14px;font-weight: bold;">
                            {{$articulos[2]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @endif

            </div>
          </div>
        @endforeach
      
</div>
<ul>
  <li v-for="sub in s">
    @{{a}}
  </li>
</ul>
<v-paginator :options="options" :resource_url="resource_url" @update="updateResource"></v-paginator>
@endsection

@push('js')
<script>
    var vm1 = new Vue({
    el:"#page_wrap",
    components: {
        VPaginator: VuePaginator
    },
    data: {
        a:"sss",
        sub:[],
        resource_url: '/api/animals',
        options: {
          remote_data: 'nested.data',
          remote_current_page: 'nested.current_page',
          remote_last_page: 'nested.last_page',
          remote_next_page_url: 'nested.next_page_url',
          remote_prev_page_url: 'nested.prev_page_url',
          next_button_text: 'Go Next',
          previous_button_text: 'Go Back'
        }
    },
    methods: {
        updateResource(data){
          this.animals = data
        }
    },
    created () {
        var vm = this;
        vm.sub = {!! json_encode($sub) !!};
    }

});

</script>
@endpush