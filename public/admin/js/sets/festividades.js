/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */

$(function(){

    $('input[name=desde]').inputmask({mask: "99-99"});
    $('input[name=hasta]').inputmask({mask: "99-99"});

    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $("#articleform")[0].reset();
        $('.modal-title').text("Nuevo atractivo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});  
        $("#modal").find("img[name=img_datos]").attr('src', '#');
    });

    //EDIT
    $('#dataTable tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var lugar = $(this).data('lugar');
        var descripcion = $(this).data('descripcion');
        var state = ($(this).data('state') == "")?true:false;
        var url  = $(this).data('url');
        var desde  = $(this).data('desde');
        var hasta  = $(this).data('hasta');

        $('.modal-title').text("Editar atractivo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input class="idf" type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=name]").val(name);
        $("#modal").find("input[name=lugar]").val(lugar);
        $("#modal").find("textarea[name=descripcion]").val(descripcion);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modal").find("img[name=img_datos]").attr('src', url);
        $("#modal").find("input[name=desde]").val(desde);
        $("#modal").find("input[name=hasta]").val(hasta);
        
        $("#modal").on('hidden.bs.modal', function(e){
           $("#modal").find("input[name=id]").remove();
        });
    });

});
