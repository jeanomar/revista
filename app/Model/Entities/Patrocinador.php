<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Patrocinador extends Model
{
	use SoftDeletes;
    protected $table = 'patrocinador';
    public $timestamps = true;
    protected $fillable = ['id', 'name', 'description', 'telephone1', 'telephone2', 'web', 'email', 'address', 'ubication', 'user_id'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];

    public function images()
    {
        return $this->hasMany(ImgPatrocinador::class, 'patrocinador_id');
    }

    public function publicies()
    {
        return $this->hasMany(Publicity::class, 'patrocinador_id');
    }
}
