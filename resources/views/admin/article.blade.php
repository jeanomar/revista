@extends('admin.layouts.plantilla')
@push('css')
    <!-- DataTables CSS -->
    <link href="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet" />

    <style>
        .resumen{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis; 
        }
    </style>

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="{{ asset('admin/js/jQuery-File-Upload-9.27.0/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/js/jQuery-File-Upload-9.27.0/css/jquery.fileupload-ui.css') }}">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
@endpush
@section('content')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Artículos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Listado de artículos
                            <button id="btn-tambah" class="btn btn-default pull-right" style="padding-top: 4px;padding-bottom: 4px;margin-top: -5px;">
                                Nuevo artículo <i class="glyphicon glyphicon-new-window"></i>
                            </button>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Título</th>
                                        <th>Categoría</th>
                                        <th>Subtítulo</th>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($articulos as $art)
                                    <tr >
                                        <td>{{$art->id}}</td>
                                        <td>{{$art->title}}</td>
                                        <td>{{$art->category->title}}</td>
                                        <td><p class="resumen" style="width: 200px">{{$art->subtitle}}</p></td>
                                        <td><p class="resumen" style="width: 300px">{{$art->descripcion}}</p></td>
                                        <td>{{($art->state == 1)?'Activo':'Inactivo'}}</td>
                                        <td class="center" style="min-width: 108px;">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['articulos.state', $art->id], 'class' => 'FormDeleteTime form-group','data-art' => $art->state]) !!}
                                                @if($art->state == 1)
                                                    <button type="button" id="btn-edit" class="btn btn-default btn-circle btn-xs" title="Editar" data-id="{{$art->id}}" data-title="{{$art->title}}" data-subtitle="{{$art->subtitle}}" data-descripcion="{{$art->descripcion}}" data-state="{{$art->state}}" data-category_id="{{$art->category_id}}" data-principal="{{$art->principal}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" id="btn-img" class="btn btn-default btn-circle btn-xs" title="Imágenes" data-id="{{$art->id}}" data-title="{{$art->title}}">
                                                        <i class="fa fa-file-photo-o"></i>
                                                    </button>
                                                @endif

                                                {!! Form::hidden('art_id', $art->state, ['class' => 'form-control']) !!}

                                                {!! Form::button('<i class="fa '.(($art->state == 1)?'fa-ban':'fa-check-circle ').'"></i>', array('type' => 'submit', 'class' => 'specialButton btn btn-default btn-circle btn-xs', 'title'=>(($art->state == 1)?'Desactivar':'Activar') )) !!}

                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
  
    @include("admin.modals.articles_modal")

@endsection

@push('js')
<!-- DataTables JavaScript -->
    <script src="{{ asset('admin/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('admin/js/jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    

    <script>
         $('#dataTable').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]],
            createdRow:function(row, data, dataindex){
                if (data[5]== 'Inactivo') {
                    $(row).css({'color':"rgb(150, 0, 0)"});
                }
            }
        });

        $(".FormDeleteTime").submit(function (event) {
            var x = confirm("Desea "+(($(this).data("art")==0)?'activar':'desactivar')+' este articulo?');
            if (x) {
                return true;
            }
            else {
                event.preventDefault();
                return false;
            }
        });

    </script>
    
    <script src="{{ asset('admin/js/sets/articulo.js') }}"></script>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name" style="margin-bottom: 0px;">Descripción</p>
                <input type="text" class="form-control name" placeholder="Escriba una descripción" name="description">
                 <div class="form-group" style="margin-bottom: 0px;margin-top: 5px;">
                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio"  name="typeimg"></label>
                </div>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Subir</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name" id="lbl{%=file.value%}">
                    {% if (file.url) { %}
                        <a id="sa{%=file.value%}" href="{%=file.url%}" class="hidden-xs" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        <a id="sb{%=file.value%}" href="{%=file.url%}" class="hidden-sm hidden-md hidden-lg" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name.substring(0, 30)+"..."%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                <input type="text" id="txt{%=file.value%}" value="{%=file.name%}" class="form-control name hidden" placeholder="Escriba una descripción" />
                {% if (file.type == 'p') { %}
                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio" value="{%=file.value%}" name="typeimg" checked></label>
                {% } else { %}
                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio" value="{%=file.value%}" name="typeimg"></label>
                {% } %}
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                     <button class="btn btn-success edit pushme" onclick="myFunction(event)" data-type="EDIT" id="btn{%=file.value%}" name="{%=file.value%}" type="button" state="0">
                        <i class="glyphicon glyphicon-pencil" name="{%=file.value%}"></i>
                        <span name="{%=file.value%}">Editar</span>
                    </button>
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Eliminar</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle col-md-12">
                   
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/vendor/jquery.ui.widget.js') }}"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/tmpl.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/canvas-to-blob.min.js') }}"></script>
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.blueimp-gallery.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-image.js') }}"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-audio.js') }}"></script>
    <!-- The File Upload video preview plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-video.js') }}"></script>
    <!-- The File Upload validation plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-validate.js') }}"></script>
    <!-- The File Upload user interface plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-ui.js') }}"></script>
<script>
    function myFunction(e) {
        var id = $(e.target).attr('name');
        
        if ($("#btn"+id).attr("state")=='0') {
            $('#txt'+id).removeClass( "hidden" );
            $('#lbl'+id).addClass( "hidden" );
            $("#btn"+id).attr("state","1");
            $("#btn"+id).html('<i class="glyphicon glyphicon-save" name="'+id+'"></i> <span name="'+id+'">Guardar</span>');
        }else{
            $.ajax({
                type: 'post',
                url : "articulos/editimg",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'name'      : $('#txt'+id).val(),
                    'id'        : id
                },
                success: function(){
                    $("#btn"+id).attr("state","0");
                    $('#lbl'+id).removeClass( "hidden" );
                    $('#txt'+id).addClass( "hidden" );
                    $("#sa"+id).text($('#txt'+id).val());
                    $("#sb"+id).text($('#txt'+id).val().substring(0, 30)+"...");
                     $("#btn"+id).html('<i class="glyphicon glyphicon-pencil" name="'+id+'"></i> <span name="'+id+'">Editar</span>');
                },
                error: function(request){
                    
                }
            });
        }
      
    }
</script>
@endpush

