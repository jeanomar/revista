<?php

namespace App\Http\Controllers;
use Omar\Entities\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $terminos =  Article::find(22);
        return view('admin.terminosypoliticas', compact('terminos'));
        //return view('admin.home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function terminosypoliticasupdate(Request $request)
    {
        if ($request->get('id') == 22) {
            $articulo =  Article::find($request->get('id'));
            $articulo->title = $request->get('title');
            $articulo->descripcion = $request->get('descripcion');
            $articulo->save();
        }
        
        return back();
    }
}
