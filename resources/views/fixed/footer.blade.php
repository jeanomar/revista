<!-- s-footer================================================== -->
<style>

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact textarea,
#contact button[type="submit"] {
  font: 400 12px/16px "Roboto", Helvetica, Arial, sans-serif;
}

#contact {
  padding: 25px;

}

#contact h3 {
  display: block;
  font-size: 30px;
  font-weight: 300;
  margin-bottom: 10px;
}

#contact h4 {
  margin: 5px 0 15px;
  display: block;
  font-size: 13px;
  font-weight: 400;
}

fieldset {
  border: medium none !important;
  margin: 0 0 10px;
  min-width: 100%;
  padding: 0;
  width: 100%;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact textarea {
  width: 100%;
  border: 1px solid #ccc;
  background: #FFF;
  margin: 0 0 5px;
  padding: 10px;
}

#contact input[type="text"]:hover,
#contact input[type="email"]:hover,
#contact input[type="tel"]:hover,
#contact textarea:hover {
  -webkit-transition: border-color 0.3s ease-in-out;
  -moz-transition: border-color 0.3s ease-in-out;
  transition: border-color 0.3s ease-in-out;
  border: 1px solid #aaa;
}

#contact textarea {
  height: 100px;
  max-width: 100%;
  resize: none;
}

#contact button[type="submit"] {
  cursor: pointer;
  width: 100%;
  border: none;
  background: #9e2840;
  color: #FFF;
  margin: 0 0 5px;
  padding: 10px;
  font-size: 15px;
}

#contact button[type="submit"]:hover {
  background: #565656;
  -webkit-transition: background 0.3s ease-in-out;
  -moz-transition: background 0.3s ease-in-out;
  transition: background-color 0.3s ease-in-out;
}

#contact button[type="submit"]:active {
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

.copyright {
  text-align: center;
}

#contact input:focus,
#contact textarea:focus {
  outline: 0;
  border: 1px solid #aaa;
}

::-webkit-input-placeholder {
  color: #888;
}

:-moz-placeholder {
  color: #888;
}

::-moz-placeholder {
  color: #888;
}

:-ms-input-placeholder {
  color: #888;
}
</style>
<footer class="s-footer">
    <div class="row top">
        <div class="col-full f-pad">
            <div class="site-info clearfix">
                <a href="/" class="site-logo sinsub">
                     <img src="<?=Croppa::url(asset(config('app.logoimg')), 170,38)?>" alt="">
                </a> 
                <div class="site-info-text">
                    <p class="site-director">
                        <span>{{config('app.sponsor1')}}</span>
                        <br>
                        <span class="univ2s">{{config('app.sponsor2')}}</span>
                    </p>
                    <p class="site-address developersd">
                        <span><a title="Contactar" href="{{config('app.facebook')}}" style="color: #454444;">{{config('app.developer')}}</a></span>
                        <br>
                        <span >{{config('app.typedev')}}</span>
                        <br>
                    </p>
                    <p class="site-copyright">
                        {{config('app.dr')}} {{config('app.name')}} 
                        <br> {{config('app.drs')}} 
                        <br>
                    </p>
                </div> 
            </div>
            <div class="site-sections hidden-xs"> 
                <h4 class="col-title" style="width: 100%;">
                    Secciones
                    <a href="" data-target="#group-footer" id="" class="ui-toggle">
                        <i class="icon-down"> </i>
                    </a>  
                </h4>
                <div id="group-footer" class="toggle-content">
                    @php $c = 1; @endphp
                    @foreach($menu as $m)
                        @foreach($m->submenus as $sm)
                            @if($c <= 13)

                                @if($c <= 3)
                                    @if ($c == 1)
                                        <div class="box-left">
                                            <ul class="ul-footer"> 
                                    @endif
                                    <li class="li-footer l-pri">
                                        <a class="a-footer {{($c==3)?'a-fin':''}}" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a>
                                    </li> 
                                    @if($c == 3)
                                        </ul>
                                    @endif
                                @endif
                                
                                @if($c >= 4 and $c <= 6)
                                    @if ($c == 4)
                                        <ul class="ul-footer l-pri ul-ri"> 
                                    @endif
                                    <li class="li-footer">
                                        <a class="a-footer {{($c==6)?'a-fin':''}}" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a>
                                    </li> 
                                    @if ($c == 6)
                                        </ul>
                                        </div>
                                    @endif
                                @endif
                                
                                @if($c >=7 and $c <= 9)
                                    @if ($c == 7)
                                        <div class="box-right">
                                            <ul class="ul-footer">
                                    @endif
                                    <li class="li-footer">
                                        <a class="a-footer {{($c==9)?'a-fin':''}}" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a>
                                    </li> 
                                     @if($c == 9)
                                        </ul>
                                    @endif
                                @endif
                                
                                @if($c >= 10 and $c <= 12)
                                    @if ($c == 10)
                                        <ul class="ul-footer ul-ri"> 
                                    @endif
                                    <li class="li-footer" style="padding-right: 0px;">
                                        <a class="a-footer {{($c==12)?'a-fin':''}}" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a>
                                    </li>
                                    @if ($c == 12)
                                            </ul>
                                        </div>
                                    @endif
                                @endif
                                
                                @php
                                    $c++;
                                @endphp
                            @endif
                        @endforeach
                    @endforeach

                    @if ($c == 9)
                        <li class="li-footer">
                           <div style="margin-top: 20px;"><br></div>
                        </li>
                       </ul>

                        <ul class="ul-footer ul-ri"> 
                       </ul>
                    </div>
                    @endif  
                    @if ($c == 10)
                       </ul>
                    </div>
                    @endif 
                    @if ($c == 11)

                       </ul>
                    </div>
                    @endif 
                     @if ($c == 12)
                     </ul>
                     </div>
                    @endif 
                </div>
            </div>
            <div class="site-contact-social hidden-xs">
                <div class="fm-column"> 
                    <h4 class="col-title" style="width: 100%;">Contacto</h4>
                    <ul class="ul-footer2"> 
                        <li class="li-footer2"><a class="a-footer" href="/larevista" style="margin-top: 35px;">¿Quiénes somos? </a></li> 
                        <li class="li-footer2"><a class="glyphicon glyphicon-envelope a-footer" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"> Buzón</a></li> 
                        <li class="li-footer2"><a class="a-footer a-fin" href="/terminos/22">TÉRMINOS DE USO </a></li> 
                    </ul>
                </div> 
                <div class="fm-column drigth">
                    <h4 class="col-title" style="width: 100%;">Síguenos </h4>
                    <ul class="ul-footer2"> 
                        <li class="li-footer2"> 
                            <a class="a-footer" href="{{config('app.fb')}}" target="_blank" style="margin-top: 35px;"><i class="fa fa-facebook-square"></i><span> Facebook</span></a> 
                        </li>
                        <li class="li-footer2"> 
                            <a class="a-footer2" href="{{config('app.insta')}}" target="_blank"><i class="fa fa-twitter-square"></i><span> Instagram</span></a>  
                        </li>
                        <li class="li-footer2">
                             
                        </li>
                    </ul> 
                </div>
            </div>
        </div>
    </div>
    <div class="s-footer__bottom">
        <div class="row">
            <div class="col-full">
                <div class="go-top">
                    <a class="smoothscroll" title="Back to Top" href="#top"></a>
                </div>
            </div>
        </div>
    </div> <!-- end s-footer__bottom -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom: 30px;">
                    <h5 class="modal-title" id="exampleModalLabel" style="width: 80%;float: left;color: #545454;">Mensaje</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height: 20px;float: right;">
                      <span aria-hidden="true" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'route'=>'send', 'id'=>'contact','style'=>'background-color: #fff;']) !!}
                        <fieldset>
                          <input name="name" placeholder="Tu nombre" type="text" tabindex="1" style="height: 50px;" required autofocus>
                        </fieldset>
                        <fieldset>
                          <input name="email" placeholder="Tu email" type="email" tabindex="2" style="height: 50px;" required>
                        </fieldset>
                        <fieldset>
                          <input name="tlf" placeholder="Tu número de teléfono (opcional)" type="tel" tabindex="3" style="height: 50px;" required>
                        </fieldset>
                        <fieldset>
                          <textarea name="desc" placeholder="Escribe tu mensaje aquí...." tabindex="5" style="min-height: 10rem" required></textarea>
                        </fieldset>
                        <fieldset>
                          <button type="submit" id="contact-submit" data-submit="...Enviando" style="height: 60px;">Enviar</button>
                        </fieldset>
                    {!!Form::close() !!}
                </div>
               
            </div>
        </div>
    </div>
</footer> <!-- end s-footer -->
