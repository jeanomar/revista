
<div id="modal" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            {!! Form::open(['method'=>'POST', 'route'=>'articulos', 'id'=>"articleform", 'class'=>'form-horizontal' ]) !!}
                <div class='modal-body'>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Subcategoria</label>
                        <div class='col-sm-10'>
                            <select name='subcat' class='form-control' required>
                                <option value='' disabled selected>Seleccione una opción</option>
                                @foreach($subcategorias as $sc)
                                    @if( $sc->padre or $sc->id == 5)
                                        <option value='{{$sc->id}}'>{{$sc->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Estado</label>
                        <div class='col-sm-10'>
                            <input type='checkbox' name='state' checked value="1"> Activo</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Art. principal</label>
                        <div class='col-sm-10'>
                            <input type='checkbox' name='principal' > Activo</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Título</label>
                        <div class='col-sm-10'>
                            <input class='form-control' placeholder='Título' type='text' name='title' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Subtítulo</label>
                        <div class='col-sm-10'>
                            <textarea class='form-control' placeholder='Subtítulo' type='text' name='subtitle' rows="4" required></textarea>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Descripción</label>
                        <div class='col-sm-10'>
                            <textarea class='form-control' rows="12" name='descripcion' required></textarea>
                        </div>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                    <button type='submit' class='btn btn-flat bg-aqua'>Aceptar</button>
                </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>


<div id="modal_img" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class='form-horizontal'>
                <div class='modal-body'>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Artículo</label>
                        <div class='col-sm-10'>
                            <label class='form-control' name='title'></label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>Imágenes</label>
                        <div class='col-sm-10'>
                            <!-- The file upload form used as target for the file upload widget -->
                            <form id="fileupload"  method="POST" enctype="multipart/form-data">
                                @csrf
                                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                <noscript><input type="hidden" name="redirect" ></noscript>
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-10">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Agregar fotos...</span>
                                            <input type="file" name="files[]" multiple>
                                        </span>
                                        <button type="submit" class="btn btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Subir</span>
                                        </button>
                                        <button type="reset" class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancelar</span>
                                        </button>
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Eliminar</span>
                                        </button>
                                        <input type="checkbox" class="toggle">
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                    </div>
                                    <!-- The global progress state -->
                                    <div class="col-lg-2 fileupload-progress fade">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <!-- The extended global progress state -->
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table id="tb_imgs" role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                    <button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>
                </div>
            </div>  
        </div>
    </div>
</div>