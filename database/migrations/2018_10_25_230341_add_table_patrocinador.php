<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePatrocinador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrocinador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500);
            $table->string('description', 10000);
            $table->string('telephone1', 11);
            $table->string('telephone2', 11);
            $table->string('web', 300);
            $table->string('email', 100);
            $table->string('address', 400);
            $table->string('ubication', 2000);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patrocinador', function (Blueprint $table) {
            //
        });
    }
}
