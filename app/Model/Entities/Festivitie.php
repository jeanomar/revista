<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Festivitie extends Model
{
	use SoftDeletes;
    protected $table = 'festivities';
    public $timestamps = true;
    protected $fillable = ['id', 'name', 'desde', 'hasta',  'lugar', 'descripcion', 'url', 'user_id'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
