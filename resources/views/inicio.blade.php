
@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')

@push('css')
    <style>
    .subtitulo2{
      display: block;
      display: -webkit-box;
      max-width: 100%;
      height: 50px;
      margin: 0 auto;
      line-height: 1;
      -webkit-line-clamp:3;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    @media only screen and (max-width:1048px) {
        .red-of.leftred{
            margin-left: 110px;
        }
    }
    </style>
@endpush

@section('menu')
    @include("fixed.menu")
@endsection

@section('banner')
    <div id="page_wrap" class="limit">
        <div id="zona_2" style="margin-top: 40px;">
            
            <div id="exper" style="width: 100% !important;">
                <article style="width: 100% !important;">
                    <a href="{{URL::action('ArticleController@ver', $principal->id) }}">
                        @foreach($principal->images->where('type','p') as $im)
                            <div id="wrap_im" style=" max-width: 1200px !important; width: 100% !important; height: auto !important;" >
                                <img src="<?=Croppa::url(asset($im->url), 1200, 700)?>" style=" max-width: 1200px !important; width: 100% !important; height: auto !important;" >
                            </div>
                        @endforeach 
                    </a>
                    <div id="text_s" class="fusiongente" style="margin-top: -15px;padding-left: 185px;padding-right: 185px;margin-bottom: 25px;">
                        <a href="{{URL::action('ArticleController@ver', $principal->id) }}" class="sinsub">
                             @php
                                    $cadena1 = "";
                                    $cadena2 = "";
                                    $cadena = $principal->title;
                                    $max=0;
                                    for($i=0;$i<(strlen ($cadena));$i++){
                                        if (ctype_upper(substr($cadena,$i,1))) {
                                            $max=$i;
                                        }
                                    } 
                                    $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                    $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                                @endphp
                            <span id="princtt" style="position: relative;bottom: 10px;">
                                <div id="title" class="tn princt1" style="font-family: lora;margin-bottom: 0px;font-size: 42px;color: #9d2940;">
                                    <a href="{{URL::action('ArticleController@ver', $principal->id) }}" style="text-decoration: underline;color: #91d8f6 !important;">
                                        <span style="color: #9d2940;">{{$cadena1}}</span>
                                    </a>
                                </div>
                                <div id="title" class="tn princt2" style="font-family: lora;margin-bottom: 5px;margin-top: 0px;font-size: 34px;">
                                    <a href="{{URL::action('ArticleController@ver', $principal->id) }}" class="sinsub" style="color: #9d2940 !important;">{{$cadena2}}</a>
                                </div>
                            </span>
                            
                            <div id="excerpt excerpt2" class="rn pincsub" style="margin-top: 0px;margin-bottom: 20px;font-size: 20px;">
                                <a href="{{URL::action('ArticleController@ver', $principal->id) }}" class="sinsub">{{$principal->subtitle}}</a>
                            </div>
                        </a>
                        <div class="rn">
                           
                        </div>
                    </div>
                </article>     
                
            </div>
            @foreach($publicidad->where('position',1) as $pub)
            <h3 class="tb andes" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
                </a>
            </h3>
            @endforeach
        </div>
    </div>
    
    @include("fixed.banner2")
@endsection

@section('content')

@php
$caracteres_subtitle=175;
$caracteres_subtitle2=160;
@endphp
<style>
    #top{
        padding-top: 5px !important;
    }
</style>
<div id="page_wrap" class="limit">
    <div id="zona_1" style="margin-top: 150px;">
        @foreach($publicidad->where('position',2) as $pub)
        <h3 class="tb andes" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
            <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
            </a>
        </h3>
        @endforeach
        <h3 class="tb andes" style="margin-top: 0px;text-align: left;">
            <a href="{{URL::action('ArticleController@verxcategory', 1) }}" class="sinsub">Tradición</a>
        </h3>

        <div class="wrap_second">
            @for($i=0; $i<$tradicion->count(); $i++)
            <article class="second_wrap articlo {{($i==2)?'trest':''}}">
                <a href="{{URL::action('ArticleController@ver', $tradicion[$i]->id) }}">
                    <div id="wrap_article">
                        @foreach($tradicion[$i]->images->where('type','p') as $im)
                           <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url),600,null)?>)"></div>
                        @endforeach
                        
                        <div id="wrap_t">
                            <div id="title" class="tn">
                                @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $tradicion[$i]->title;
                                $max=0;
                                for($j=0;$j<(strlen ($cadena));$j++){
                                    if (ctype_upper(substr($cadena,$j,1))) {
                                        $max=$j;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                                @endphp
                                <div style="font-family: lora;line-height: 1;">{{$cadena1}}</div>
                                <div style="font-family: lora;line-height: 1;padding-bottom: 3px;">{{$cadena2}}</div>
                            </div>
                        </div>
                    </div>
                </a>
                <div id="wrap_in">
                    <a href="{{URL::action('ArticleController@ver', $tradicion[$i]->id) }}" class="sinsub">
                        <div id="excerpt" class="rn excerpt2" style="margin-top: 0px;">
                            {{substr($tradicion[$i]->subtitle,0,$caracteres_subtitle2).((strlen($tradicion[$i]->subtitle)>$caracteres_subtitle2)?"...":"")}}
                        </div>
                    </a>
                    <div id="author" class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $tradicion[$i]->category->id) }}" class="sinsub fbrite" style="font-weight: bold;">
                            {{$tradicion[$i]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @if($i == 0)
            @foreach($publicidad->where('position',3) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @if($i == 1)
            @foreach($publicidad->where('position',4) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @if($i == 2)
            @foreach($publicidad->where('position',5) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @endfor
        </div>
    </div>

    <div id="zona_3" class="portafolio">
        @foreach($publicidad->where('position',3) as $pub)
        <h3 class="tb andes hidden-xs" style="margin-top: 20px; height: auto;border-bottom-width: 0px;border-top-width: 0px;">
            <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
            </a>
        </h3>
        @endforeach
        <h3 class="tb andes" style="margin-top: 20px;text-align: left;">
            <a href="{{URL::action('ArticleController@verxcategory', 2) }}" class="sinsub">Andes World</a>
        </h3>
        <div id="editors">
            <article class="impar">
                <a href="{{URL::action('ArticleController@ver', $andes[0]->id) }}">
                     @foreach($andes[0]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url), 600,null)?>)"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $andes[0]->id) }}" class="sinsub">
                            @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $andes[0]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 0px;line-height: 1;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 2px;line-height: 1;padding-bottom: 3px;">{{$cadena2}}</div>
                        <div id="excerpt" class="rn excerpt2" style="margin-top: 2px;">
                            {{substr($andes[0]->subtitle,0,$caracteres_subtitle2).((strlen($andes[0]->subtitle)>$caracteres_subtitle2)?"...":"")}}
                        </div>
                    </a>
                    <div id="author" class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $andes[0]->category->id) }}" class="sinsub fbrite" style="font-weight: bold;">
                            {{$andes[0]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @foreach($publicidad->where('position',6) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            <article class="par">
                <a href="{{URL::action('ArticleController@ver', $andes[1]->id) }}">
                    @foreach($andes[1]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url), 600,null)?>)"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $andes[1]->id) }}" class="sinsub">
                            @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $andes[1]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 0px;line-height: 1;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 2px;line-height: 1;padding-bottom: 3px;">{{$cadena2}}</div>
                        <div id="excerpt" class="rn excerpt2" style="margin-top: 2px;">
                            {{substr($andes[1]->subtitle,0,$caracteres_subtitle2).((strlen($andes[1]->subtitle)>$caracteres_subtitle2)?"...":"")}}
                        </div>
                    </a>
                    <div id="author" class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $andes[1]->category->id) }}" class="sinsub fbrite" style="font-weight: bold;">
                            {{$andes[1]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @foreach($publicidad->where('position',7) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @if($andes->count() == 3)
            <article class="impar">
                <a href="{{URL::action('ArticleController@ver', $andes[2]->id) }}">
                     @foreach($andes[2]->images->where('type','p') as $im)
                        <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url), 600,null)?>)"></div>
                    @endforeach
                    <!--div class="category">
                        <div class="category-children">
                            <h3></h3>
                        </div>
                    </div-->
                </a>
                <div id="text_s" style="margin-top: 10px;">
                    <a href="{{URL::action('ArticleController@ver', $andes[2]->id) }}" class="sinsub">
                         @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $andes[2]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 0px;line-height: 1;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 2px;line-height: 1;padding-bottom: 3px;">{{$cadena2}}</div>
                        <div id="excerpt" class="rn excerpt2" style="margin-top: 2px;">
                            {{substr($andes[2]->subtitle,0,$caracteres_subtitle2).((strlen($andes[2]->subtitle)>$caracteres_subtitle2)?"...":"")}}
                        </div>
                    </a>
                    <div id="author" class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $andes[2]->category->id) }}" class="sinsub fbrite" style="font-weight: bold;">
                            {{$andes[2]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @foreach($publicidad->where('position',8) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
        </div>
    </div>

    <div id="zona_1" style="margin-top: 20px;">
        @foreach($publicidad->where('position',4) as $pub)
        <h3 class="tb andes hidden-xs" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
            <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
            </a>
        </h3>
        @endforeach
        <h3 class="tb andes" style="margin-top: 0px;text-align: left;">
            <a href="{{URL::action('ArticleController@verxcategory', 1) }}" class="sinsub">Fusión cultural</a>
        </h3>

        <div class="wrap_second">
            @for($i=0; $i<$fusion->count(); $i++)
            <article class="second_wrap articlo {{($i==2)?'trest':''}}">
                <a href="{{URL::action('ArticleController@ver', $fusion[$i]->id) }}">
                    <div id="wrap_article">
                        @foreach($fusion[$i]->images->where('type','p') as $im)
                           <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url),600,null)?>)"></div>
                        @endforeach
                        
                        <div id="wrap_t">
                            <div id="title" class="tn">
                                @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $fusion[$i]->title;
                                $max=0;
                                for($j=0;$j<(strlen ($cadena));$j++){
                                    if (ctype_upper(substr($cadena,$j,1))) {
                                        $max=$j;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                                @endphp
                                <div style="font-family: lora;line-height: 1;">{{$cadena1}}</div>
                                <div style="font-family: lora;line-height: 1;padding-bottom: 3px;">{{$cadena2}}</div>
                            </div>
                        </div>
                    </div>
                </a>
                <div id="wrap_in">
                    <a href="{{URL::action('ArticleController@ver', $fusion[$i]->id) }}" class="sinsub">
                        <div id="excerpt" class="rn excerpt2" style="margin-top: 0px;">
                            {{substr($fusion[$i]->subtitle,0,$caracteres_subtitle2).((strlen($fusion[$i]->subtitle)>$caracteres_subtitle2)?"...":"")}}
                        </div>
                    </a>
                    <div id="author" class="rn">
                        <a href="{{URL::action('ArticleController@verxcategorydetail', $fusion[$i]->category->id) }}" class="sinsub fbrite" style="font-weight: bold;">
                            {{$fusion[$i]->category->title}}
                        </a>
                    </div>
                </div>
            </article>
            @if($i == 0)
            @foreach($publicidad->where('position',9) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @if($i == 1)
            @foreach($publicidad->where('position',10) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @if($i == 2)
            @foreach($publicidad->where('position',11) as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            @endif
            @endfor
        </div>
    </div>

    <div id="zona_2">
        @foreach($publicidad->where('position',5) as $pub)
        <h3 class="tb hidden-xs" style="margin-top: 50px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
            <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
            </a>
        </h3>
        @endforeach
        <h3 class="tb gente" style="margin-bottom: 50px !important;margin-top: 20px;text-align: left;">
            <a href="{{URL::action('ArticleController@verxcategorydetail', 5) }}" class="sinsub">Gente</a>
        </h3>
        <div id="exper" style="width: 100% !important;">
            <article style="width: 100% !important;">
                <a href="{{URL::action('ArticleController@ver', $gente[0]->id) }}">
                    @foreach($gente[0]->images->where('type','p') as $im)
                        <div id="wrap_im" style=" max-width: 1200px !important; width: 100% !important; height: auto !important;" >
                            <img src="<?=Croppa::url(asset($im->url), 1200, 700)?>" style=" max-width: 1200px !important; width: 100% !important; height: auto !important;" >

                        </div>
                    @endforeach 
                </a>
                <div id="text_s" class="fusiongente" style="margin-top: -15px;padding-left: 185px;padding-right: 185px;">
                    <a href="{{URL::action('ArticleController@ver', $gente[0]->id) }}" class="sinsub">
                         @php
                                $cadena1 = "";
                                $cadena2 = "";
                                $cadena = $gente[0]->title;
                                $max=0;
                                for($i=0;$i<(strlen ($cadena));$i++){
                                    if (ctype_upper(substr($cadena,$i,1))) {
                                        $max=$i;
                                    }
                                } 
                                $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                            @endphp
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 0px;border-bottom: 1px solid #91d8f6;font-size: 32px;color: #9d2940;">{{$cadena1}}</div>
                        <div id="title" class="tn" style="font-family: lora;margin-bottom: 5px;margin-top: -5px;font-size: 32px;color: #9d2940;">{{$cadena2}}</div>
                        <div id="excerpt excerpt2" class="rn" style="margin-top: 20px;">{{$gente[0]->subtitle}}</div>
                    </a>
                    <div class="rn">
                       
                    </div>
                </div>
            </article>     
            
        </div>
    </div>
    
</div>

<div id="page_wrap2" class="limit">
    <div id="zona_9">
        <div id="wrap_inst" class="gente">
            @foreach($publicidad->where('position','>=',12)->sortBy('position') as $pub)
            <h3 class="tb andes hidden-lg hidden-sm hidden-md" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 690,null)?>" alt="">
                </a>
            </h3>
            @endforeach


            @foreach($publicidad->where('position','>=',6)->sortBy('position') as $pub)
            <h3 class="tb hidden-xs" style="margin-top: 20px; height: auto; border-bottom-width: 0px;border-top-width: 0px;">
                <a href="/patrocinadores?id={{$pub->patrocinador_id}}" class="sinsub">
                    <img style="width: 100%; max-width: 972px; height: auto;" src="<?=Croppa::url(asset($pub->url), 972,null)?>" alt="">
                </a>
            </h3>
            @endforeach
            
            <h3 class="tb" style="margin-top: 20px;text-align: left;">
                <a  class="sinsub">Comunidad Revista</a>
            </h3>
            <div id="facebookfp" class="col-md-4 col-sm-6 col-xs-12" style="padding-bottom: 15px;padding-top: 15px;padding-left: 0px;padding-right: 0px;">
                
            </div>
            <div id="twidfp" class="col-md-4 col-sm-6 col-xs-12" style="padding-bottom: 15px;padding-top: 15px; height: 700px; overflow-y: scroll;">
                
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-bottom: 15px;padding-top: 15px;height: 700px; overflow-y: scroll;">
                <!-- InstaWidget -->
                <a href="https://instawidget.net/v/user/cajamarcadondetodoempezo" id="link-421c4c252266da3d3e1dac5b3aac8567c92113926d7978a3dbe79edc3b0ed803">@cajamarcadondetodoempezo</a>
                <script src="https://instawidget.net/js/instawidget.js?u=421c4c252266da3d3e1dac5b3aac8567c92113926d7978a3dbe79edc3b0ed803&width=300px"></script>
            </div>
            
        </div>
    </div>
</div>
@endsection

@push('js')
 
<script>
    $( window ).on( "load", function() {
       document.getElementById('facebookfp').innerHTML = '<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FRevistaCajamarcadondetodoempezo%2F&tabs=timeline%2Cmessages%2Cevents&width=340&height=685&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="685" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>';

        document.getElementById('twidfp').innerHTML='<a class="twitter-timeline" href="https://twitter.com/cajamarcadte?ref_src=twsrc%5Etfw">Tweets by cajamarcadte</a> ';

        $.getScript("js/embed.js", function (data, estado) {

        });

        $.getScript("js/widgets.js", function (data, estado) {
            
        });
    });
</script>

@endpush

</body>
</html>
