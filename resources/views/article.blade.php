@extends('layouts.plantilla')

@section('title')
{{$articulo->title}}
@endsection
@push('css')
    <style>
        .col-full{

        }
        
        .row.masonry-wrap{
            margin-left: 5%;
            margin-right: 5%;
            width: 90%;
        }

        .post-thumb{
            margin-left: 10px;
        }

        @media only screen and (max-width:1200px) {
            .row.top{
                padding-left: 35px;padding-right: 35px;
            }
            .descriptc{
                padding-right: 25px !important;
                padding-left: 0px;
            }
            .news-text{
                padding-right: 0px;
                padding-left: 0px;
            }

            .mod-mxm-news{
                height: 800px !important;
            }
            .w-mxm-items{
                height: 100%!important;
            }
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                max-width: 1200px;

            }
            .imgpub1{
                max-height: 200px !important;
                width: 100% !important;
            }
            .row.top {
                padding-left: 0;
                padding-right: 0;
            }

        }
        @media only screen and (max-width: 910px) {
            .mod-mxm-news{
                height: 100%!important;
            }
        }
        @media only screen and (max-width: 900px) {
            .imgpub1{
                padding-right: 0px !important;
                max-height: 180px !important;
            }
        }
        @media only screen and (max-width: 800px) {
            .imgpub1{
                max-height: 150px !important;
            }
            .header__content {
                margin-left: 0px;
                margin-right: 0px;
                width: 100%;
            }
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                margin-left: 5% !important;
                margin-right: 5% !important;
                max-width: 800px;
                width: 90%;
            }
            .masonry-wrap {
                max-width: 800px;
            }

            .pageheader-content.row{
                padding-left: 0px;
                padding-right: 0px;
                margin-left: 0px;   
                margin-right: 0px;
                max-width: 800px;
                width: 100%;
            }
            
            .entry.format-standard{
                padding-left: 0px;
            }

            .row .row {
                margin-left: 0px;
                margin-right: 0px;
                padding-left: 0;
                padding-right: 0;
                width: 100%;
                max-width: 800px;
            }
            #relaciona {
                margin-left: 0px !important;
                margin-right: 0px !important;
                width: 100% !important;
            }
            .reli{
                margin-left: 0px !important;
                margin-right: 0px !important;
                width: 100% !important;
            }
            .entry__text{
                padding-left: 0px !important;
            }

            .mod-mxm-news{
                height: 100% !important;
            }
            .w-mxm-items{
                height: 100% !important;
            }
        }
        @media only screen and (max-width:767px) {
            .imgpub1{
                max-height: 250px !important;
            }
            .descriptc{
                padding-right: 0px !important;
                padding-left: 0px !important; 
            }
            .news-text{
                padding-right: 0px !important;
                padding-left: 0px !important;
            }
            .mod-mxm-news{
                height: 100% !important;
            }
            .w-mxm-items{
                height: 100% !important;
            }
            .featured__column .entry {
                height: 800px;
                width: 70%;
                margin-left: 15%;
                margin-right: 15%;
            }
        }

        @media only screen and (max-width:600px) {
            .imgpub1{
                max-height: 220px !important;
            }
            .mod-mxm-news{
                height: 800px !important;
            }
            .w-mxm-items{
                height: 800px !important;
            }

        }
        @media only screen and (max-width:500px) {
            .imgpub1{
                max-height: 200px !important;
            }
            .masonry-wrap {
                max-width: 500px;
            }

            .mod-mxm-news{
                height: 850px !important;
            }
            .w-mxm-items{
                height: 850px !important;
            }

        }
        @media only screen and (max-width: 400px) {
            .imgpub1{
                max-height: 180px !important;
            }
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                margin-left: 5% !important;
                margin-right: 5% !important;
                max-width: 800px;
                width: 90% !important;
            }

            .str2{
                margin-top: -5px !important;
            }

            .entry__excerpt{
                padding-left: 0px;
                padding-right: 0px;
                margin-left: 5%;
                margin-right: 5%;
            }
            .publi{
                margin-left: 5%;
                margin-right: 5%;
            }
            .relacionados{
                margin-top: -40px;
            }
            .reli{
                margin-left: 40px;
                margin-right: 40px;
            }
            .row.masonry-wrap{
                margin-left: 0px;
                margin-right: 0px;
            }
            .entry__header{
                margin-left: 5%;
                margin-right: 5%;
                width: 90% !important;
                max-width: 400px;
            }
            #relaciona {
                float: left !important;
            }
            #h3tb{
                margin-top: 45px;
            }
            .post-title{
                width: 100% !important;
            }
            .reltitle1{
                margin-top: 3px !important;
            }
            .reltitle2{
                margin-top: -2px !important;
            }
            .postcontainer{
                margin-bottom: 20px !important;
            }
        }
        /**********Redes Sociales*/

        a {
            cursor: pointer;
            text-decoration: none;
        }

        .contenedor-redes-sociales {
            width: 0;
            left: 0;
            position: fixed;
            bottom: 20px;
            margin-left: 10px;
            z-index: 100;
        }

        .icono {
            top: 50%;
            position: relative;
            margin-bottom: 10px;
        }

        .icono-primary {
            display: inline-block;
            text-decoration: none;
            color: #fff;
            background: #0009;
            border-radius: 50%;
            position: relative;
            z-index: 1;
            height: 40px;
            width: 40px;
            text-align: center;
            line-height: 40px;

            -webkit-box-shadow: 1px 0 1px rgba(0,0,0,0.5);
            box-shadow: 1px 0 1px rgba(0,0,0,0.5);
        }

        .contenedor-descripcion {
            overflow: hidden;
            position: absolute;
            top: 0;
            left: 24px;
        }

        .icono-descripcion {
            width: 250px;
            display: inline-block;
            text-decoration: none;
            color: #fff;
            background: #9e2840 !important;
            height: 40px;
            line-height: 40px;
            padding-left: 40px;
            border-radius: 0 20px 20px 0;

            -webkit-box-shadow: 1px 0 1px rgba(0,0,0,0.5);
            box-shadow: 1px 0 1px rgba(0,0,0,0.5);

            -webkit-transform: translate3d(-110%, 0, 0);
            -ms-transform: translate3d(-110%, 0, 0);
            -o-transform: translate3d(-110%, 0, 0);
            transform: translate3d(-110%, 0, 0);

            -webkit-transition: all 0.5s;
            -o-transition: all 0.5s;
            transition: all 0.5s;
        }

        .icono-primary:hover ~ .contenedor-descripcion .icono-descripcion {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
        }

        /*-------Colores Para Redes Sociales-------*/
        .color-twitter {
            background: #0009;
        }

        
    </style>
@endpush

@section('menu')
    @include("fixed.menu1")
@endsection

@section('banner')

 @include("fixed.banner6")
    
@endsection

@section('content')
    @php
        $caracteres_subtitle=155;
    @endphp

    <article class="contenedor-redes-sociales">
        <div class="icono">
            <a href="http://www.facebook.com/sharer.php?u={{'https://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]}}&amp;t={{$articulo->title}}" class="icono-primary" target="_blank" style="color: #fff !important;"><i class="fa fa-facebook"></i></a>

            <div class="contenedor-descripcion">
                <a href="#" class="icono-descripcion" style="color: #fff !important;">Compartir Facebook</a>
            </div>
        </div>
        <div class="icono">
            <a href="http://twitter.com/?status={{'https://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]}}" class="icono-primary color-twitter" target="_blank" style="color: #fff !important;"><i class="fa fa-twitter"></i></a>

            <div class="contenedor-descripcion">
                <a href="#" class="icono-descripcion color-twitter" style="color: #fff !important;">Compartir Twitter</a>
            </div>
        </div>
       
    </article>
    
    <img src="<?=Croppa::url(asset($articulo->images[0]->url), 600,350)?>" alt="" hidden>

    <!-- s-content
    ================================================== -->
    <section class="s-content content" style="padding-top: 5px;">
        
        <div class="row masonry-wrap" style="max-width: 1200px;margin-left: auto;margin-right: auto;">
            <div class="masonry article-content">
                <div class="entry__header pri" id="titlees" style="margin-left: 20px;">
                    @php
                        $cadena1 = "";
                        $cadena2 = "";
                        $cadena = $articulo->title;
                        $max=0;
                        for($i=0;$i<(strlen ($cadena));$i++){
                            if (ctype_upper(substr($cadena,$i,1))) {
                                $max=$i;
                            }
                        } 
                        $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                        $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                    @endphp
                    <h1 class="entry__title" style="font-size: 58px;color: #9d2940;line-height: 1;font-family: 'lora';">{{$cadena1}}</h1>
                    <h1 class="entry__title" style="font-size: 58px;color: #9d2940;font-size: 58px;margin-top: 0px;margin-top: -25px;line-height: 1;font-family: 'lora';">{{$cadena2}}</h1>
                </div>
        

                <article class="entry format-standard col-sm-9 descriptc" data-aos="fade-up" style="padding-right: 0px;background-color: #f000;">
                        
                    <div class="entry__thumb">
                        
                    </div>

                    @if($publicidadi->count()>0)
                        <!-- s-extra
                        ================================================== -->
                        <section class="s-extra publi hidden-sm hidden-md hidden-lg" style="background-color: #fff;padding-top: 0px;padding-bottom: 0px;">

                            <div class="row top" style="margin-left: 0px;margin-right: 0px;">
                               
                                <div class="tab-full popular col-sm-12" style="padding-left: 0px;padding-right: 0px;margin-bottom: 0rem;">
                                    <img src="{{($publicidadi->count()>0)?$publicidadi[0]->url:""}}" alt="" class="col-sm-12 imgpub1" style="max-height: 250px;padding-left: 0px;padding-right: 2.8rem;">
                                </div> <!-- end popular -->

                            </div> <!-- end row -->

                        </section> <!-- end s-extra -->
                    @endif
    
                    <div class="entry__text" style="padding-left: 0px;padding-top: 0px;padding-bottom: 5px;">
                        <div class="entry__header">
                            <h3 class="entry__title str2" style="color:#4b4b4b;margin-bottom: 15px;font-family: times;">{{$articulo->subtitle}}</h3>
                            
                        </div>
                        <div class="entry__excerpt">
                            <p style="font-family: 'calibri';font-weight: 400;color: #4b4b4b;font-size: 19px;margin-bottom: 35px;text-align: justify;letter-spacing: 0.07em;">
                                
                                @php 
                                    echo (nl2br($articulo->descripcion));
                                @endphp
                            </p>
                        </div>
                        <div class="entry__meta" style="display: none;">
                            <span class="entry__meta-links">
                                <a href=""></a>
                            </span>
                        </div>
                    </div>
                    @if($publicidadi->count()>0)
                    <!-- s-extra
                    ================================================== -->
                    <section class="s-extra publi hidden-xs" style="background-color: #fff;padding-top: 0px;padding-bottom: 0px;">

                        <div class="row top" style="margin-left: 0px;margin-right: 0px;">
                           
                            <div class="tab-full popular col-sm-12" style="padding-left: 0px;padding-right: 0px;margin-bottom: 0rem;">
                                <img src="{{($publicidadi->count()>0)?$publicidadi[0]->url:""}}" alt="" class="col-sm-12 imgpub1" style="max-height: 250px;padding-left: 0px;padding-right: 2.8rem;">
                            </div> <!-- end popular -->

                        </div> <!-- end row -->

                    </section> <!-- end s-extra -->
                    @endif
                    <section class="s-extra" style="padding-right: 2.8rem;">
                        <div class="row top relacionados">
                             <div class="col-full" id="relaciona">
                                 <div class="header__nav reli">
                                    <h3 class="tb" id="h3tb" style="margin-bottom: 20px !important; color: #4b4b4b;text-align: left; ">Relacionados</h3>
                                </div>
                                <div style="border-left: 1px solid #91d8f6;margin-top: 70px;">
                                    @foreach($relacionados as $r)
                                        @php
                                            $cadena1 = "";
                                            $cadena2 = "";
                                            $cadena = $r->title;
                                            $max=0;
                                            for($i=0;$i<(strlen ($cadena));$i++){
                                                if (ctype_upper(substr($cadena,$i,1))) {
                                                    $max=$i;
                                                }
                                            } 
                                            $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                            $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                                        @endphp
                                        <div class="post-container">                
                                           <div class="post-thumb" style="width: 30%;">
                                            @foreach($r->images->where('type','p') as $i)
                                                <img src="<?=Croppa::url(asset($i->url), 100,70)?>" />
                                            @endforeach
                                            </div>
                                            <a href="{{URL::action('ArticleController@ver', $r->id) }}">
                                                <div class="post-title" style="width: 70%;margin-top: 2px;">
                                                    <p class="reltitle1" style="margin-top: 0px;margin-bottom: 0px;font-size: 18px;line-height: 1.1;">
                                                        {{$cadena1}}
                                                    </p>
                                                    <p class="reltitle2" style="margin-top: 0px;margin-bottom: 0px;font-size: 18px;line-height: 1.1;">
                                                        {{$cadena2}}
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                             </div>                
                        </div>
                        <div class="row top">
                        </div> <!-- end row -->
                    </section style="padding-top: 0px;"> <!-- end s-extra -->
                </article> <!-- end article -->

                <article class="entry format-standard col-sm-3 news-text" data-aos="fade-up" style="padding-left: 0px;padding-right: 0px;background-color: #f000;">
    
                    <div class="entry__text" style="padding-left: 10px;padding-right: 20px;padding-top: 0px;">

                        @if($publicidadd->count()>0)
                        <div class="featured">

                            <div class="featured__column featured__column--big" style="width: 100%;">
                                <div class="entry imgpub2" style="background-image: url({{($publicidadd->count()>0)?$publicidadd[0]->url:''}});">
                                    
                                    <div class="entry__content">

                                        <h1 class="h1_entry"></h1>

                                    </div> <!-- end entry__content -->
                                    
                                </div> <!-- end entry -->
                                <p>
                                    
                                </p>
                                
                            </div> <!-- end featured__big -->

                        </div> <!-- end featured -->
                        @endif
                        
                    </div> <!-- end about -->
    
                </article> <!-- end article -->


            </div> <!-- end masonry -->
        </div> <!-- end masonry-wrap -->

    </section> <!-- s-content -->

@endsection

@push('js')

    <script>
         // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
        
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();        
        
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

      $(".tab_drawer_heading").removeClass("d_active");
      $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
      
    });
    /* if in drawer mode */
    $(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
      
      $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
      
      $("ul.tabs li").removeClass("active");
      $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
    
    
    /* Extra class "tab_last" 
       to add border to right side
       of last tab */
    $('ul.tabs li').last().addClass("tab_last");
    
    </script>
@endpush