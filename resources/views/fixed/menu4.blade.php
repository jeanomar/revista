<!-- pageheader
    ================================================== -->

        <header class="header">
            <div class="header__content row">

            
                    

                <div class="header__search">

                    <form role="search" method="get" class="header__search-form" action="#">
                        <label>
                            <span class="hide-content">Buscar:</span>
                            <input type="search" class="search-field" placeholder="Type Keywords" value="" name="s" title="Search for:" autocomplete="off">
                        </label>
                        <input type="submit" class="search-submit" value="Search">
                    </form>
        
                    <a href="#0" title="Close Search" class="header__overlay-close">Close</a>

                </div>  <!-- end header__search -->
                <a class="header__toggle-menu" href="#0" title="Menu">
                    <span>Menu</span>
                    <div class="header__logo  hidden-md hidden-lg" style="margin-top: -15px;margin-bottom: 0px;height: 20px;width: 100%;" >
                        <a class="logo logo2" href="{{URL::action('InicioController@index') }}" style="margin-left: 0px;height: 20px;margin-top: -2px">
                            <h1 style="font-size: 23px;height: 12px;margin-bottom: 3px;letter-spacing: 0.5px;" class="row site-name sizm21">
                                 <!--img src="images/logo.svg" alt="Homepage"-->
                                {{config('app.name1')}}
                            </h1>
                            <h3 class="row site-name sizm31" style="font-size: 14px;">
                                {{config('app.name2')}}
                            </h3>
                        </a>
                    
                    </div> <!-- end header__logo -->
                </a>
                
                <nav class="header__nav-wrap"><!--id="navbar"-->
                    <h1 class="header__nav-heading h6"></h1>

                    <ul class="header__nav menus" style="background-color: rgb(235, 235, 235);   margin-bottom: 0px; transition: margin 700ms ease 0s;padding-left: 0px;margin-left: 20px;margin-right: 20px;">
                        <li class="hidden-sm hidden-xs col-md-2" style="height: 40px !important;background-color: #fff !important;width: 165px;">
                            <div class="header__logo" style="margin-top: -15px;margin-bottom: 0px;height: 20px;width: 100%;">
                                <a class="logo" href="{{URL::action('InicioController@index') }}" style="margin-left: 0px;height: 20px;margin-top: -2px">
                                    <h1 class="row site-name sizm2" style="font-size: 23px;height: 12px;margin-bottom: 3px;letter-spacing: 0.5px;">
                                         <!--img src="images/logo.svg" alt="Homepage"-->
                                        {{config('app.name1')}}
                                    </h1>
                                    <h3 class="row site-name sizm3" style="font-size: 14px;">
                                        {{config('app.name2')}}
                                    </h3>
                                </a>
                                
                            </div> <!-- end header__logo -->
                        </li>
                        @foreach($menu as $m)
                            <li class="{{ ($m->url == $_SERVER["REQUEST_URI"])?'current':(($m->submenus->count()>0)?'':'') }}" style="font-size: 15px !important;"><!--has-children-->
                                <a class="menuletter mayusc sinsub" href="{{($m->submenus->count()>00)?URL::action('ArticleController@verxcategory', $m->id):URL::action('ArticleController@verxcategorydetail', $m->id) }}" title="">{{$m->title}}</a>
                                @if($m->submenus->count() >0 )
                                <ul class="sub-menu">
                                    @foreach($m->submenus as $sm)
                                    <li><a class="menuletter sinsub" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                        @endforeach
                        
                    </ul> <!-- end header__nav -->
                    
                        
                    <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

                </nav> <!-- end header__nav-wrap -->
                <div class="row top">
                    <div class="col-full" style="padding-left: 40px;padding-right: 40px;">
                        <div class="header__nav">
                            <h3 class="tb dir2" style="text-align: left;letter-spacing: normal;font-family:lucida_sans; color: #8d6b41e6;margin-top: 50px;">{{$dir}}</h3>
                        </div>
                    </div>
                </div>
            </div> <!-- header-content -->
        </header> <!-- header -->
