<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePublicity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publicity', function (Blueprint $table) {
            $table->integer('patrocinador_id')->after('article_id')->unsigned()->nullable();
            $table->foreign('patrocinador_id')->references('id')->on('patrocinador')->onDelete('set null');
            $table->integer('position')->after('state')->nullable();
            $table->enum('state_ini',[1,0])->after('state')->default(0);
            $table->integer('user_id')->nullable();
            $table->dropColumn('name');
            $table->dropColumn('email');
            $table->dropColumn('tlf1');
            $table->dropColumn('tlf2');
            $table->dropColumn('link');
            $table->dropColumn('article');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publicity', function (Blueprint $table) {
            //
        });
    }
}
