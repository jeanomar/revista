<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Attractive extends Model
{
	use SoftDeletes;
    protected $table = 'attractive';
    public $timestamps = true;
    protected $fillable = ['id', 'name', 't1', 't2', 't3', 't4', 'article', 'lugar', 'ubication', 'descripcion', 'url', 'type', 'user_id'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
