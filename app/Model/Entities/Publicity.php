<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Publicity extends Model
{
	use SoftDeletes;
    protected $table = 'publicity';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'url', 'position', 'state_ini', 'patrocinador_id', 'article_id', 'state', 'deleted_at'];
    protected $hidden = ['updated_at', 'created_at', 'user_id'];
    protected $appends = ['action'];

    public function articulo()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }

    public function patrocinador()
    {
        return $this->belongsTo(Patrocinador::class, 'patrocinador_id');
    }

    public function getActionAttribute()
    {    $str='
    		<form class="FormDeleteTime form-group">  
                '.(($this->deleted_at == null)?'<button type="button" id="btn-editp" class="btn btn-default btn-circle btn-xs" title="Editar" data-id="'.$this->id.'" data-title="'.$this->title.'" data-statea="'.$this->state.'" data-url="'.$this->url.'" data-position="'.$this->position.'" data-state="'.(($this->deleted_at == null)?1:0).'" data-articleid="'.$this->article_id.'" data-patrocinadorid="'.$this->patrocinador_id.'" data-statei="'.$this->state_ini.'" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"> 
                    <i class="fa fa-pencil"></i>
                </button>':'').'
                <button type="button" id="btn-deletp" title="'.(($this->deleted_at == null)?'Desactivar':'Activar').'" class="specialButton btn btn-default btn-circle btn-xs" id="btn-delete" data-id="'.$this->id.'" data-title="'.$this->title.'" data-state="'.(($this->deleted_at == null)?1:0).'"><i class="fa '.(($this->deleted_at == null)?'fa-ban':'fa-check-circle ').'"></i></button>
            </div>
        ';
        return $this->attributes['action'] = $str;
    }


}
