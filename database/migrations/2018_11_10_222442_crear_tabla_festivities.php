<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaFestivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festivities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 1000);
            $table->string('lugar', 100);
            $table->string('desde', 20);
            $table->string('hasta', 20);
            $table->string('descripcion', 5000);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festivities');
    }
}
