 <!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Administrador: {{ucwords(strtolower(auth()->user()->name))}}</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out fa-fw"></i>{{ __('Cerrar Sesión') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                
                <li>
                    <a href="{{ url('/home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('/larevista/editar') }}"><i class="fa fa-file-o fa-fw"></i> La Revista</a>
                </li>
                 <li>
                    <a href="{{ url('/articulos') }}"><i class="fa fa-list-alt fa-fw"></i> Artículos</a>
                </li>
                <li>
                    <a href="{{ url('/auspiciadores') }}"><i class="glyphicon glyphicon-sunglasses fa-fw"></i> Patrocinadores</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fighter-jet fa-fw"></i> Travel<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ url('/travel/datosutiles') }}">Datos útiles</a>
                        </li>
                        <li>
                            <a href="{{ url('/travel/atractivosdentro') }}">Atractivos dentro de la ciudad</a>
                        </li>
                        <li>
                            <a href="{{ url('/travel/atractivosfuera') }}">Atractivos fuera de la ciudad</a>
                        </li>
                        <li>
                            <a href="{{ url('/travel/gastronomia') }}">Gastronomía</a>
                        </li>
                        <li>
                            <a href="{{ url('/travel/festividades') }}">Fechas de festividades</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('/menu') }}"><i class="fa fa-list-ul fa-fw"></i> Menu</a>
                </li>
                <li>
                    <a href="https://blue76.dnsmisitio.net:2096/"><i class="glyphicon glyphicon-envelope fa-fw"></i> Mi correo</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>