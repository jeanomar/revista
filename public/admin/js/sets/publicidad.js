/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */

$(function(){

    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $("#articleform")[0].reset();
        $('.modal-title').text("Nueva publicidad");
        $('#modal').modal({keyboard: false, backdrop: 'static'});  
        $("#modal").find("img[name=img_datos]").attr('src', '#');
    });

    //EDIT
    $('#dataTable tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var title = $(this).data('title');
        var state = ($(this).data('state') == "")?true:false;
        var url  = $(this).data('url');
        var web  = $(this).data('article');
        var link  = $(this).data('link');
        var ubication  = $(this).data('ubication');

        $('.modal-title').text("Editar publicidad");
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input class="idf" type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=name]").val(name);
        $("#modal").find("input[name=title]").val(title);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modal").find("img[name=img_datos]").attr('src', url);
        $("#modal").find("input[name=web]").val(web);
         $("#modal").find("input[name=link]").val(link);
        $("#modal").find("select[name=ubication]").val(ubication);
        
        $("#modal").on('hidden.bs.modal', function(e){
           $("#modal").find("input[name=id]").remove();
        });
    });

});
