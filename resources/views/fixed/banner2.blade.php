<style>
    .slider-container {
        margin-top:35px;
        height:100%;
    }

    .carousel-captions {
        position: absolute;
        top: 0;
        text-align: left;
        left: 0;
        right: inherit;
        width: 100%;
        color:#000;
        bottom:-95px;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-control {
        background: none repeat scroll 0 0 #000 !important;
        bottom: 0;
        color: #fff;
        font-size: 20px;
        height: 700px;
        opacity: 0.5;
        position: absolute;
        
        right: 108px !important;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
        top: 296px;
        z-index: 1 !important;
        width: 4%;
    }

    #sliderpri {
        padding-left: 0px;
        padding-right: 0px;
    }

    .lrslider1 {
        margin-top: -600px;
    }
    

    #slider_captionspri {
        top: -250px;
    }
    .sltxt1 {
        font-size: 42px;
    }
    .sltxt2 {
        font-size: 21px;
    }
    .imgslide {
        height: 700px !important;width: 1200px;
    }
    
    #page_wrap3 {
        height: 700px;margin-top: 35px;
    }
    .left{
        left: 0%;
    }

    .right{
        right: 0%;
        left: 96% !important;
    }
    @media only screen and (max-width:1200px){
        .carousel-captions {
            width: 112%;
            left: -6%;
        }
        .carousel-control{
            height: 520px;
            top: 80px !important;
        }
        .trest{
            margin-left: 0px !important;
        }
        .gente{
            width: 90% !important;
            margin-left: 5%; 
            margin-right: 5%;  
        }
        .wrap_second{
          padding-left: 5% !important;
          padding-right: 5% !important;  
        }
        #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 900px;
            height: 520px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
    }
    @media only screen and (max-width:1000px) {
        .carousel-control{
            height: 550px;
            top: 50px !important;
        }
        #text_s{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .gente{
            width: 100% !important;
            margin-left: 0%; 
            margin-right: 0%;  
        }
        #expert{
            width: 90% !important;margin-left: 5%;margin-right: 5%;
        }

         #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
        .imgslide {
            max-width: 1000px;
            height: 550px !important;
            width: 100% !important;
        }
        #text_s{
            padding-left: 0% !important; */
        }
        
    }

    @media only screen and (max-width:900px) {
        .carousel-control{
            height: 520px;
            top: 80px !important;
        }
        .lrslider {
            margin-top: -500px !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }

        .imgslide {
            max-width: 900px;
            height: 520px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -1px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        
    }

    @media only screen and (max-width:800px) {
        .carousel-control{
            height: 470px;
            top: 130px !important;
            width: 6%;
        }
        .right {
            left: 94% !important;
        }
        .lrslider {
            margin-top: -450px !important;
        }
        .imgslide {
            max-width: 800px;
            height: 470px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }

        #slider_captionspri {
            top: -1px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    }

    .sltxt2{color: #fff !important;}
    .sltxt1{color: #fff !important;}

    @media only screen and (max-width:767px) {
        .sltxt2{font-size: 20px !important;}
        .sltxt1{font-size: 20px !important;}
         #slider_captionspri {
            top: 0px !important;
        }
        
        .s-pageheader {
            height: 700 !important;
        }

        /* mixin for multiline */
        .text-center.sltxt3.redus {
          overflow: hidden;
          position: relative;
          line-height: 1.2em;
          max-height: 4.8em;
          text-align: justify;
          margin-right: -1em;
          padding-right: 1em;
        }
        .text-center.sltxt3.redus:before {
          content: '...';
          position: absolute;
          right: 0;
          bottom: 0;
        }
        .text-center.sltxt3.redus:after {
          content: '';
          position: absolute;
          right: 0;
          width: 1em;
          height: 1em;
          margin-top: 0.2em;
          background: white;
        }
    }
    @media only screen and (max-width:700px) {
        #excerpt{
            margin-top: -60px !important;
        }
        #excerpt.excerpt2{
            margin-top: 0px !important;
        }
        .princt1{
            font-size: 36px !important;
        }
        .princt2{
            font-size: 32px !important;
        }
        #zona_2{
            margin-top: 0px !important;
        }
        #page_wrap3 {
            margin-top: -10px !important;
        }
        .carousel-control{
            height: 400px;
            top: 200px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
        .imgslide {
            max-width: 700px;
            height: 400px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    
        .s-pageheader {
            height: 650 !important;
        }
    }

    @media only screen and (max-width:600px) {
        #princtt{
            bottom: 10px !important;
        }
        .princt1{
            font-size: 32px !important;
        }
        .princt2{
            font-size: 28px !important;
        }
        #zona_1 article.second_wrap {
            margin-left: 0px;
        }
        .carousel-control{
            height: 250px;
            top: 350px !important;
        }
        .imgslide {
            max-width: 600px;
            height: 250px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: 0px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .s-pageheader {
            height: 540 !important;
        }
        
    }

    @media only screen and (max-width:500px) {
        #excerpt{
            margin-top: -50px !important;
        }
        #princtt{
            bottom: 0px !important;
        }
        .princt1{
            font-size: 26px !important;
        }
        .princt2{
            font-size: 22px !important;
        }
    }
    
    @media only screen and (max-width:400px) {
        #princtt{
            bottom: 10px !important;
        }
        .princt1{
            font-size: 22px !important;
        }
        .princt2{
            font-size: 18px !important;
        }
        .carousel-control{
            height: 170px;
            top: 215px !important;
            width: 10%;
        }
        .right {
            left: 90% !important;
        }
        #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 400px;
            height: 170px !important;
            width: 100% !important;
        }
        .lrslider1{
            margin-top: -385px;
            height: 1px;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
        }
        #slider_captionspri {
            top: 168px !important;
        }
        #page_wrap3 {
           height: 300px;
            margin-top: 50px;
        }
        .s-pageheader {
            height: 450 !important;
        }
        .minsld{
            height: 259px;
        }
    }
    @-moz-document url-prefix() {
        @media only screen and (max-width:400px){
            .carousel-control{
                height: 170px;
                top: 0px !important;
            }
        }
    }
    @media only screen and (max-width:300px) {
        #princtt{
            bottom: 70px !important;
        }
        .princt1{
            font-size: 18px !important;
        }
        .princt2{
            font-size: 14px !important;
        }
        .pincsub{
            margin-top: -60px !important;
        }
        
    }  
    
</style>
<div id="page_wrap3" class="slider">
    <div class="col-sm-12 col-md-12 col-xs-12 minsld" style="padding-left: 0px;padding-right: 0px;background-color:#343434;">

        <h3 class="tb andes" style="margin-left: 4%;width:92% !important;margin-top: 11px;margin-bottom: 20px !important;text-align: left;background-color:rgba(0, 0, 0,0);">
            <a style="color: #fff !important;" href="{{URL::action('ArticleController@verxcategory', 1) }}" class="sinsub">Recomendados</a>
        </h3>
        <div id="sliderpri" class="carousel slide col-sm-12  padd-lt0" data-ride="carousel" data-interval="10000">
            
            <div class="carousel-inner">
                
                @php
                    $i=0;
                @endphp
                @foreach($imgs as $img)
                    <div class="item {{($i == 0)?'active':''}}">
                        <img class="imgslide" style="background-image:url(<?=Croppa::url(asset($img->url), 1200,700)?>); max-height: 700px; max-width: 1200px; width: 100% !important; height: 100%; background-position: center;background-size: cover;">   
                    </div>
                    @php
                        $i++;
                    @endphp
                @endforeach
                
            </div>
            <div class="col-sm-12 col-md-12 col-xs-12 lrslider1">
                <a data-slide="prev" role="button" class="left carousel-control" href="#sliderpri" style="top: -100px;">
                    <span class="glyphicon glyphicon-chevron-left" style="left: 10px !important;color: #fff !important"></span>
                </a>
                <a data-slide="next" role="button" href="#sliderpri" class="right carousel-control" style="top: -100px;">
                    <span class="glyphicon glyphicon-chevron-right" style="left: 10px !important;color: #fff !important"></span>
                </a>
            </div>
            
        </div>

        <div id="slider_captionspri" class="col-sm-12 col-md-12 col-xs-12" style="top: -1px;">
            <div  style="margin-left: 3%;margin-right: 3%;">
                @php
                    $j=0;
                @endphp
                @foreach($imgs as $img)
                    <div id="captions-{{$j}}" class="carousel-captions" style="background-color: #343434; padding-top: 0px; margin-bottom: 0px; padding-bottom: 15px; border-bottom-style: solid; border-bottom-width: 0px; bottom: auto;">
                        @php
                            $cadena1 = "";
                            $cadena2 = "";
                            $cadena = $img->article->title;
                            $max=0;
                            for($i=0;$i<(strlen ($cadena));$i++){
                                if (ctype_upper(substr($cadena,$i,1))) {
                                    $max=$i;
                                }
                            } 
                            $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                            $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                        @endphp
                        <a href="{{URL::action('ArticleController@ver', $img->article->id) }}" class="sinsub">
                            <h3 class="text-center sltxt1 fbrite" style="margin-top: 10px;">
                                <span style="border-bottom: 1px solid #91d8f6;font-family: 'lora' !important;">{{$cadena1}}</span>
                            </h3>
                            @if($cadena2 != "")
                            <p class="text-center sltxt2 fbrite" style="width: 100%;font-size: 28px;font-family: 'lora' !important;">{{$cadena2}}</p>
                            @endif
                        </a>
                    </div>
                    
                    @php
                        $j++;
                    @endphp
                @endforeach
                
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">

    $("#sliderpri").on('slide.bs.carousel', function(evt) {
        var step = $(evt.relatedTarget).index();

        $('#slider_captionspri .carousel-captions:not(#captions-'+step+')').fadeOut('slow', function() {
            $('#captions-'+step).fadeIn();
        });

    });

</script>