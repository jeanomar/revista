<?php

use Illuminate\Database\Seeder;
use Omar\Entities\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu1 = new Menu();
        $menu1->title = 'La Revista';
        $menu1->url = '/';
        $menu1->save();

        $menu2 = new Menu();
        $menu2->title = 'Tradición';
        $menu2->url = 'tradicion';
        $menu2->save();

        $menu3 = new Menu();
        $menu3->title = 'Fusión cultural';
        $menu3->url = 'fusion-cultural';
        $menu3->save();

        $menu4 = new Menu();
        $menu4->title = 'Andes World';
        $menu4->url = 'andes-world';
        $menu4->save();

        $menu5 = new Menu();
        $menu5->title = 'Gente';
        $menu5->url = 'gente';
        $menu5->save();

        $menu6 = new Menu();
        $menu6->title = 'Patrocinadores';
        $menu6->url = 'patrocinadores';
        $menu6->save();

        $menu7 = new Menu();
        $menu7->title = 'Travel';
        $menu7->url = 'travel';
        $menu7->save();

    }
}
