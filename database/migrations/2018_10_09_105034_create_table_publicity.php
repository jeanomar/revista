<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePublicity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1000);
            $table->string('name', 300);
            $table->string('url', 500);
            $table->string('link', 500);
            $table->string('email', 100);
            $table->string('tlf1', 11);
            $table->string('tlf2', 11);
            $table->integer('article_id')->unsigned()->nullable();
            $table->foreign('article_id')->references('id')->on('article')->onDelete('set null');
            $table->enum('state',[1,0])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicity');
    }
}
