<?php

namespace App\Http\Controllers;
use Omar\Entities\Menu;
use Omar\Entities\Image;
use Omar\Entities\Article;
use Omar\Entities\Patrocinador;
use Omar\Entities\Publicity;
use Mail;
use Illuminate\Http\Request;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$menu = Menu::whereNull('menu_id')->where('access','G')->get();
    	$imgs = Image::where('type','p')->where('banner',1)->orderByRaw("RAND()")->take(6)->get();
    	$actualidad =  Article::where('principal','0')->where('state',1)->where('id','!=',21)->where('id','!=',22)->where('id','!=',23)->orderBY('id','DESC')->take(5)->get();
    	$tradicion =  Article::whereIn('category_id',Menu::where('menu_id',2)->pluck('id'))->orderBY('id','DESC')->take(3)->get();
    	//$andes = Menu::where('menu_id',3)->get();
    	$fusion = Article::whereIn('category_id',Menu::where('menu_id',4)->pluck('id'))->orderBY('id','DESC')->take(3)->get();
    	$gente = Article::where('category_id',5)->orderBY('id','DESC')->take(1)->get();
        $andes = Article::whereIn('category_id',Menu::where('menu_id',3)->pluck('id'))->orderBY('id','DESC')->take(3)->get();
        $publicidad = Publicity::where('state_ini','1')->where('position','>',0)->orderBY('position')->get();
        $principal = Article::where('principal','1')->get()->first();

        $data = [
            'imgs'  => $imgs,
            'andes' =>$andes,
            'tradicion' => $tradicion,
            'fusion' => $fusion,
            'gente' => $gente,
            'actualidad' => $actualidad,
            'principal' => $principal,
            'publicidad' => $publicidad
        ];
        return view('inicio', compact('menu'))->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $articulos =  Article::where('title','like','%'.$request->get('s')."%")->orderBY('id','DESC')->get();

        $menu = Menu::whereNull('menu_id')->get();
        $dir = "Busqueda para: '".$request->get('s')."'";
        $data = [
            'dir'  => $dir,
            'articulos' => $articulos
        ];

        return view('category-detail', compact('menu'))->with($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function auspiciadores()
    {
    	$menu = Menu::whereNull('menu_id')->where('access','G')->get();
    	$dir = "Patrocinadores";
        $patrocinadores = Patrocinador::whereNull('deleted_at')->orderBY('id','DESC')->get();

        $data = [
            'dir'  => $dir,
            'patrocinadores' =>  $patrocinadores
        ];

        return view('auspiciadores', compact('menu'))->with($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function larevista()
    {
        $articulo =  Article::find(21);
        $menu = Menu::whereNull('menu_id')->where('access','G')->get();
        $dir = $articulo->title;

        $data = [
            'articulo' => $articulo,
            'dir'  => $dir
        ];

        return view('larevista', compact('menu'))->with($data);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function terminosypoliticas($id)
    {
        if($id != 22 and $id != 23){
            return "No disponible";
        }
        $articulo =  Article::find($id);
        $menu = Menu::whereNull('menu_id')->where('access','G')->get();
        $dir = $articulo->title;

        $data = [
            'articulo' => $articulo,
            'dir'  => $dir
        ];

        return view('terminosypoliticas', compact('menu'))->with($data);
    }

    public function send(Request $request)
    {
        $title = $request->input('name');
        $content = $request->input('desc');
        $tlf = $request->input('tlf');
        $email_from = 'admin@revistacajamarcadondetodoempezo.com.pe';
        $email = $request->input('email');
        $subject = 'Buzón de correo: '.$request->input('name');

        Mail::send('emails.send', ['title' => $title, 'content' => $content, 'tlf' => $tlf, 'email' => $email], function ($message) use($email_from, $title, $subject)
        {
            $message->from($email_from, $title);
            $message->to('contacto@revistacajamarcadondetodoempezo.com.pe');
            $message->subject($subject);
        });

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
