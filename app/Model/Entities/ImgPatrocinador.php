<?php

namespace Omar\Entities;

use Illuminate\Database\Eloquent\Model;

class ImgPatrocinador extends Model
{
    protected $table = 'img_patrocinador';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'url',  'type', 'patrocinador_id', 'user_id', 'state'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];


    public function article()
    {
        return $this->belongsTo(Patrocinador::class, 'patrocinador_id');
    }
}
