<?php

namespace App\Http\Controllers;

use Omar\Entities\Data;
use Croppa;
use File;
use FileUpload;
use Omar\Entities\Attractive;
use Omar\Entities\Gastronomy;
use Omar\Entities\Festivitie;
use Omar\Entities\Menu;
use Illuminate\Http\Request;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datosutiles()
    {
        $datos =  Data::all();
        return view('admin.datosutiles', compact('datos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function atractivosdentro()
    {
        $atractivos =  Attractive::withTrashed()->where('type', 'dentro')->get();
        return view('admin.atractivosdentro',compact('atractivos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function atractivosfuera()
    {
        $atractivos =  Attractive::withTrashed()->where('type', 'fuera')->get();
        return view('admin.atractivosfuera',compact('atractivos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gastronomia()
    {
        $platos =  Gastronomy::withTrashed()->get();
        return view('admin.gastronomia',compact('platos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function festividades()
    {
        $festividades =  Festivitie::withTrashed()->get();
        return view('admin.festividades',compact('festividades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function climaupdate(Request $request)
    {
        $dato =  Data::find(1);
        $dato->name = $request->get('title');
        $dato->valor1 = $request->get('descripcion');
        $dato->save();
        $dato =  Data::find(2);
        $dato->valor1 = $request->get('t1');
        $dato->valor2 = $request->get('t2');
        $dato->save();
        $dato =  Data::find(3);
        $dato->valor1 = $request->get('t3');
        $dato->valor2 = $request->get('t4');
        $dato->save();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accesoaereo(Request $request)
    {
        $dato =  Data::find(4);
        $dato->valor1 = $request->get('name');
        $dato->save();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accesoter(Request $request)
    {
        $dato = new Data;
        $dato->name = $request->get('lugar');
        $dato->valor1 = $request->get('km');
        $dato->valor2 = $request->get('tiempo');
        $dato->user_id = auth()->user()->id;
        $dato->father_id = 5;
        $dato->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $dato = Data::find($id);
        $dato->user_id = auth()->user()->id;
        $dato->save();
        $dato->delete();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function distancias(Request $request)
    {
        $dato = new Data;
        $dato->name = $request->get('lugar');
        $dato->valor1 = $request->get('km');
        $dato->valor2 = $request->get('tiempo');
        $dato->user_id = auth()->user()->id;
        $dato->type_id = 25;
        $dato->save();
        return back();
    }

    public $folder = '/uploads/datosutiles';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imgdatos(Request $request)
    {
        // create upload path if it does not exist
        $path = public_path($this->folder."/");
        if(!File::exists($path)) {
            File::makeDirectory($path);
        };
        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple($path);
        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();
        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['imgdatos'], $_SERVER);
        $slugGenerator = new FileUpload\FileNameGenerator\Slug();
        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);
        $fileupload->setFileNameGenerator($slugGenerator);
        
        // Doing the deed
        list($files, $headers) = $fileupload->processAll();
        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }
        foreach($files as $file){
            //Remember to check if the upload was completed
            if ($file->completed) {
                // set some data
                $filename = $file->getFilename();
                $url = $this->folder."/". $filename;
                
                // save data
                $picture = Data::find(13);
                $picture->valor1 = $url;
                $picture->save();
                
                // output uploaded file response
                return back();
            }
        }
        
        return response()->json(['files' => $files]);
    }

    public $folder2 = '/uploads';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atractivosdentrosave(Request $request)
    {
        if ($request->get('id')) {
            $dato = Attractive::find($request->get('id'));
        }else{
            $dato = new Attractive;
        } 
        $dato->type = 'dentro';
        $dato->name = $request->get('name');
        $dato->lugar = $request->get('lugar');
        $dato->ubication = $request->get('ubication');
        $dato->t1 =  "";
        $dato->t2 =  "";
        $dato->t3 =  "";
        $dato->t4 =  "";
        $dato->article = ($request->get('article'))?$request->get('article'):"";
        $dato->descripcion = $request->get('descripcion');


        if ($_FILES['imgdatos']['size'] > 0){
            // create upload path if it does not exist
            $path = public_path($this->folder2."/atractivos/");
            if(!File::exists($path)) {
                File::makeDirectory($path);
            };
            // Simple validation (max file size 2MB and only two allowed mime types)
            $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
            // Simple path resolver, where uploads will be put
            $pathresolver = new FileUpload\PathResolver\Simple($path);
            // The machine's filesystem
            $filesystem = new FileUpload\FileSystem\Simple();
            // FileUploader itself
            $fileupload = new FileUpload\FileUpload($_FILES['imgdatos'], $_SERVER);
            $slugGenerator = new FileUpload\FileNameGenerator\Slug();
            // Adding it all together. Note that you can use multiple validators or none at all
            $fileupload->setPathResolver($pathresolver);
            $fileupload->setFileSystem($filesystem);
            $fileupload->addValidator($validator);
            $fileupload->setFileNameGenerator($slugGenerator);
            
            // Doing the deed
            list($files, $headers) = $fileupload->processAll();
            // Outputting it, for example like this
            foreach($headers as $header => $value) {
                header($header . ': ' . $value);
            }
            foreach($files as $file){
                //Remember to check if the upload was completed
                if ($file->completed) {
                    // set some data
                    $filename = $file->getFilename();
                    $url = $this->folder2."/atractivos/". $filename;

                    $dato->url = $url;
                }
            }
        }
        
        $dato->save();
        if (!$request->get('state')) {
            $dato->delete();
        }
        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atractivosfuerasave(Request $request)
    {
        if ($request->get('id')) {
            $dato = Attractive::find($request->get('id'));
        }else{
            $dato = new Attractive;
        } 
        $dato->type = 'fuera';
        $dato->name = $request->get('name');
        $dato->lugar = $request->get('lugar');
        $dato->ubication = $request->get('ubication');
        $dato->t1 = $request->get('t1');
        $dato->t2 = $request->get('t2');
        $dato->t3 = $request->get('t3');
        $dato->t4 = $request->get('t4');
        $dato->article = ($request->get('article'))?$request->get('article'):"";
        $dato->descripcion = $request->get('descripcion');


        if ($_FILES['imgdatos']['size'] > 0){
            // create upload path if it does not exist
            $path = public_path($this->folder2."/atractivos/");
            if(!File::exists($path)) {
                File::makeDirectory($path);
            };
            // Simple validation (max file size 2MB and only two allowed mime types)
            $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
            // Simple path resolver, where uploads will be put
            $pathresolver = new FileUpload\PathResolver\Simple($path);
            // The machine's filesystem
            $filesystem = new FileUpload\FileSystem\Simple();
            // FileUploader itself
            $fileupload = new FileUpload\FileUpload($_FILES['imgdatos'], $_SERVER);
            $slugGenerator = new FileUpload\FileNameGenerator\Slug();
            // Adding it all together. Note that you can use multiple validators or none at all
            $fileupload->setPathResolver($pathresolver);
            $fileupload->setFileSystem($filesystem);
            $fileupload->addValidator($validator);
            $fileupload->setFileNameGenerator($slugGenerator);
            
            // Doing the deed
            list($files, $headers) = $fileupload->processAll();
            // Outputting it, for example like this
            foreach($headers as $header => $value) {
                header($header . ': ' . $value);
            }
            foreach($files as $file){
                //Remember to check if the upload was completed
                if ($file->completed) {
                    // set some data
                    $filename = $file->getFilename();
                    $url = $this->folder2."/atractivos/". $filename;

                    $dato->url = $url;
                }
            }
        }
        
        $dato->save();
        if (!$request->get('state')) {
            $dato->delete();
        }
        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function gastronomiasave(Request $request)
    {
        if ($request->get('id')) {
            $dato = Gastronomy::find($request->get('id'));
        }else{
            $dato = new Gastronomy;
        } 
        $dato->name = $request->get('name');
        $dato->descripcion = $request->get('descripcion');


        if ($_FILES['imgdatos']['size'] > 0){
            // create upload path if it does not exist
            $path = public_path($this->folder2."/gastronomia/");
            if(!File::exists($path)) {
                File::makeDirectory($path);
            };
            // Simple validation (max file size 2MB and only two allowed mime types)
            $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
            // Simple path resolver, where uploads will be put
            $pathresolver = new FileUpload\PathResolver\Simple($path);
            // The machine's filesystem
            $filesystem = new FileUpload\FileSystem\Simple();
            // FileUploader itself
            $fileupload = new FileUpload\FileUpload($_FILES['imgdatos'], $_SERVER);
            $slugGenerator = new FileUpload\FileNameGenerator\Slug();
            // Adding it all together. Note that you can use multiple validators or none at all
            $fileupload->setPathResolver($pathresolver);
            $fileupload->setFileSystem($filesystem);
            $fileupload->addValidator($validator);
            $fileupload->setFileNameGenerator($slugGenerator);
            
            // Doing the deed
            list($files, $headers) = $fileupload->processAll();
            // Outputting it, for example like this
            foreach($headers as $header => $value) {
                header($header . ': ' . $value);
            }
            foreach($files as $file){
                //Remember to check if the upload was completed
                if ($file->completed) {
                    // set some data
                    $filename = $file->getFilename();
                    $url = $this->folder2."/gastronomia/". $filename;

                    $dato->url = $url;
                }
            }
        }
        
        $dato->save();
        if (!$request->get('state')) {
            $dato->delete();
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function festividadessave(Request $request)
    {
        if ($request->get('id')) {
            $dato = Festivitie::find($request->get('id'));
        }else{
            $dato = new Festivitie;
        }

        $dato->name = $request->get('name');
        $dato->lugar = $request->get('lugar');
        $dato->desde =$request->get('desde')."-2018";
        $dato->hasta = $request->get('hasta')."-2018";
        $dato->descripcion = $request->get('descripcion');
        $dato->user_id = auth()->user()->id;

        if ($_FILES['imgdatos']['size'] > 0){
            // create upload path if it does not exist
            $path = public_path($this->folder2."/fiestas/");
            if(!File::exists($path)) {
                File::makeDirectory($path);
            };
            // Simple validation (max file size 2MB and only two allowed mime types)
            $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
            // Simple path resolver, where uploads will be put
            $pathresolver = new FileUpload\PathResolver\Simple($path);
            // The machine's filesystem
            $filesystem = new FileUpload\FileSystem\Simple();
            // FileUploader itself
            $fileupload = new FileUpload\FileUpload($_FILES['imgdatos'], $_SERVER);
            $slugGenerator = new FileUpload\FileNameGenerator\Slug();
            // Adding it all together. Note that you can use multiple validators or none at all
            $fileupload->setPathResolver($pathresolver);
            $fileupload->setFileSystem($filesystem);
            $fileupload->addValidator($validator);
            $fileupload->setFileNameGenerator($slugGenerator);
            
            // Doing the deed
            list($files, $headers) = $fileupload->processAll();
            // Outputting it, for example like this
            foreach($headers as $header => $value) {
                header($header . ': ' . $value);
            }
            foreach($files as $file){
                //Remember to check if the upload was completed
                if ($file->completed) {
                    // set some data
                    $filename = $file->getFilename();
                    $url = $this->folder2."/fiestas/". $filename;

                    $dato->url = $url;
                }
            }
        }
        
        $dato->save();
        if (!$request->get('state')) {
            $dato->delete();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyatractivo($id)
    {
        $p =Attractive::withTrashed()->find($id);
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroygastronomia($id)
    {
        $p =Gastronomy::withTrashed()->find($id);
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyfestividad($id)
    {
        $p =Festivitie::withTrashed()->find($id);
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        return back();
    }

}
