@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')
@push('css')
    <style>
      .s-pageheader--home{
        min-height: 100px;
        height: 120px;
        padding-top: 0px;
      }
      #page_wrap {max-width: 1160px;}
    </style>
<style>
  h2 { margin:30px auto;}
  .container { margin:150px auto;}
  #mixedSlider {
    position: relative;
  }
  #mixedSlider .MS-content {
    white-space: nowrap;
    overflow: hidden;
    margin: 0 5%;
  }
  #mixedSlider .MS-content .item {
    display: inline-block;
    width: 33.3333%;
    position: relative;
    vertical-align: top;
    overflow: hidden;
    height: 100%;
    white-space: normal;
    padding: 0 10px;
  }
  @media (max-width: 991px) {
    #mixedSlider .MS-content .item {
      width: 50%;
    }
  }
  @media (max-width: 767px) {
    #mixedSlider .MS-content .item {
      width: 100%;
    }
  }
  #mixedSlider .MS-content .item .imgTitle {
    position: relative;
  }

  #mixedSlider .MS-content .item .imgTitle img {
    height: auto;
    width: 100%;
  }
  #mixedSlider .MS-content .item p {
    font-size: 16px;
    margin: 2px 10px 0 5px;
    text-indent: 15px;
  }
  #mixedSlider .MS-content .item a {
    float: right;
    margin: 0 20px 0 0;

    transition: linear 0.1s;
  }

  #mixedSlider .MS-controls button {
    position: absolute;
    border: none;
    background-color: transparent;
    outline: 0;
    font-size: 50px;
    top: 95px;
    color: rgba(0, 0, 0, 0.4);
    transition: 0.15s linear;
  }
  #mixedSlider .MS-controls button:hover {
    color: rgba(0, 0, 0, 0.8);
  }
  @media (max-width: 992px) {
    #mixedSlider .MS-controls button {
      font-size: 30px;
    }
    .MS-left{
      left: 20px !important;
    }
    .MS-right{
      margin-right: 20px !important;
    }
  }

  #mixedSlider .MS-controls .MS-left {
    left: 0px;
  }
  @media (max-width: 767px) {
    #mixedSlider .MS-controls button {
      font-size: 20px;
    }
    #mixedSlider .MS-controls .MS-left {
      left: -10px;
    }
    #mixedSlider .MS-controls .MS-right {
      right: -10px;
    }
  }
   @media (max-width: 450px) {
        #directions{
            font-size: 18px !important;
        }
   }
  @media (max-width: 400px) {
    #zona_3.portafolio #editors article {
      width: 90%;
    }
    #mixedSlider .MS-controls button {
      top: 65px;
    }
  }
  #mixedSlider .MS-controls .MS-right {
    right: 0px;
  }    
</style>
@endpush

@section('menu')
    @include("fixed.menu2")
@endsection
 
@section('content')
@php
    $caracteres_subtitle=150;
@endphp
<div id="page_wrap" class="limit">
    <h3 class="tb andes" style="text-align: left;">
        <a class="sinsub" id="directions">{{$dir}}</a>
    </h3>
    <div id="zona_3" class="portafolio">
        <div id="editors">
            <div id="mixedSlider">
                @php
                    $k=0;
                @endphp
                <div class="MS-content">
                @foreach($articulos->where('state','1') as $art)

                    @php
                        $k++;
                    @endphp
                <div class="item">
                    <article >
                        <a href="{{URL::action('ArticleController@ver', $art->id) }}" style="width: 100%;">
                             @foreach($art->images->where('type','p') as $im)
                                <div id="wrap_im" style="background-image:url(<?=Croppa::url(asset($im->url), 600,null)?>); -webkit-filter: grayscale(0%);"></div>
                            @endforeach
                            <!--div class="category">
                                <div class="category-children">
                                    <h3></h3>
                                </div>
                            </div-->
                        </a>
                        <div id="text_s" style="margin-top: 20px;;margin-left: -20px;">
                            <a href="{{URL::action('ArticleController@ver', $art->id) }}" class="sinsub" style="margin-right: 0px;">
                                    @php
                                        $cadena1 = "";
                                        $cadena2 = "";
                                        $cadena = $art->title;
                                        $max=0;
                                        for($i=0;$i<(strlen ($cadena));$i++){
                                            if (ctype_upper(substr($cadena,$i,1))) {
                                                $max=$i;
                                            }
                                        } 
                                        $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                                        $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                                    @endphp
                                <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;">{{$cadena1}}</div>
                                <div id="title" class="tn" style="font-family: lora;line-height: 1;margin-bottom: 0px;padding-bottom: 3px;">{{$cadena2}}</div>
                                <div id="excerpt" class="rn" style="margin-top: 2px;">
                                    {{substr($art->subtitle,0,$caracteres_subtitle).((strlen($art->subtitle)>$caracteres_subtitle)?"...":"")}}
                                </div>
                            </a>
                            <div class="rn">
                                
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
                
                </div>
                <div class="MS-controls">
                        <button class="MS-left" style="padding: 0 0rem;"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                        <button class="MS-right" style="padding: 0 0rem;"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection

@push('js')
<script src="{{ asset('assets/plugins/js/multislider.js') }}"></script>
<script>
    $('#mixedSlider').multislider({
      duration: 750,
      interval: 0
    });        
</script>
@endpush