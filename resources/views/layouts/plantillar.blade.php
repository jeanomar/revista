<!DOCTYPE html>
<html lang="es">
<head>
    <!--- basic page needs
    ================================================== -->
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="description" content="Somos la revista líder de Cajamarca. En papel y en formato digital, para las personas más exigentes.">
    <meta name="keywords" content="">
    <meta name="author" content="">
    
    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    
    <!-- CSS
    ================================================== --> 
    <link href="{{ asset('assets/plugins/css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/micss.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/Gatopardo_files/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/custom-styles.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/fonts.css') }}" rel="stylesheet" />
    
    @stack('css')

    <!-- script
    ================================================== -->
    <script src="{{ asset('assets/plugins/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/pubads_impl_rendering_263.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/show_companion_ad.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/sdk.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/widgets.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/beacon.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/eapps.js') }}"></script>
</head>

<body class="home blog headerfix et_divi_builder et-pb-theme-travesías 2017 et-db et_minified_js et_minified_css">
    <div id="single_id_id" data-id="0"></div>
    <div id="brand_h"></div>
    <div id="page_wrap" class="head">
        @yield('menu')
    </div>
    <!-- /43823490/Travesias_Rediseño_D/Takeover -->
    <div id="div-gpt-ad-1490814078288-0" data-google-query-id="CJyH58WY-t0CFTb54QodrhEB7A" style="display: none;">
        
    </div>
    
    @yield('banner')
    @yield('content')
    <div id="page_wrap" class="limit">
        @include("fixed.footer")
    </div>
    <!-- Java Script
    ================================================== -->
    
    <script>
        $('.close-mobile-menu').click(function(event) {
           $('.sub-menu').css({display: 'none'});
        });

        window.onscroll = function() {myFunction()};

        var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
          if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky");
          } else {
            navbar.classList.remove("sticky");
          }
        }

        $('.has-children').mouseover(function(event){
            $('.menus').css({'margin-bottom': '200px'});
        }).mouseout(function() {
            $('.menus').css({'margin-bottom': '0px', 'transition': 'margin 700ms'});
        });
        $('.sub-menu').mouseover(function(event){
           $('.menus').css({'margin-bottom': '200px'});
        }).mouseout(function() {
            $('.menus').css({'margin-bottom': '0px', 'transition': 'margin 700ms'});
        });
    </script>

    @stack('js')
</body>

</html>