<style>
    .slider-container {
        margin-top:35px;
        height:100%;
    }

    .carousel-captions {
        position: absolute;
        top: 0;
        text-align: left;
        left: inherit;
        right: inherit;
        width: 100%;
        color:#000;
        bottom:-95px;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-control {
        background: none repeat scroll 0 0 #000 !important;
        bottom: 0;
        color: #fff;
        font-size: 20px;
        height: 50px;
        opacity: 0.5;
        position: absolute;
        
        right: 108px !important;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
        top: 296px;
        width: 70px;
        z-index: 1 !important;
    }

    #sliderpri {
        padding-left: 0px;
        padding-right: 0px;
    }

    .lrslider1 {
        margin-top: -600px;
    }
    

    #slider_captionspri {
        top: -250px;
    }
    .sltxt1 {
        font-size: 34px;
    }
    .sltxt2 {
        font-size: 21px;
    }
    .imgslide {
        height: 420px !important;width: 700px;
    }
    
    #page_wrap3 {
        height: 420px;margin-top: 0px;
    }
    .s-pageheader {
        margin-bottom: -50px;
    }
    #top {
       margin-top: 0px;
    }
    @media only screen and (min-width:1300px) {
        #page_wrap3 {
            margin-top: 25px;
        }
    }
    @media only screen and (max-width:1300px) {
        .contendedorpat{
            width: 94%;
        }
        #page_wrap{
            width: 94%;padding-left: 20px;padding-right: 20px;
        }
    }
    @media only screen and (min-width:1200px) {
        .carousel-control{
            top: 370px !important;
        }
    }
    @media only screen and (max-width:1200px) {
        .carousel-control{
            top: 390px !important;
        }
        #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 900px;
            height: 380px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
       #page_wrap3 {
            height: 385px;
        }
    }
    @media only screen and (max-width:1166px) {
        #page_wrap{
            margin-top: 40px;
        }
    }
    @media only screen and (max-width:1100px) {
        .post-thumb{
            width: 15% !important;
        }
        .post-title{
            width: 83% !important;
        }
        .descpat{
            width: 83% !important;
        }
         #page_wrap3 {
            height: 380px;
        }

    }
    @media only screen and (max-width:1000px) {
        .carousel-control{
            top: 405px !important;
        }
        #expert{
            width: 90% !important;margin-left: 5%;margin-right: 5%;
        }

         #sliderpri{
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
        .imgslide {
            max-width: 1000px;
            height: 350px !important;
            width: 100% !important;
        }
        .header__nav-wrap{
            margin-top: 0px;
        }
        .tb.andes{
            margin-left: 0px;margin-right: 0px;
            width: 100% !important;
        }
        #page_wrap3 {
            height: 345px;
        }
    }
    @media only screen and (max-width:990px) {
        .header__nav-wrap{
            margin-top: 50px;
        }
        #page_wrap{
            margin-top: 100px;
        }
    }

    @media only screen and (max-width:900px) {
        .carousel-control{
            top: 405px !important;
        }
        .lrslider {
            margin-top: -500px !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }

        .imgslide {
            max-width: 900px;
            height: 340px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -105px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        #page_wrap{
            margin-top: 100px !important;
        }
        #page_wrap3 {
            height: 335px;
        }
        
    }

    @media only screen and (max-width:800px) {
        .desc-acordeon{
            width: 100% !important;
            float: none  !important;
        }
        .mapa-acordeon{
            width: 100% !important;
            float: none  !important;
        }
        .carousel-control{
            top: 400px !important;
        }
        .lrslider {
            margin-top: -450px !important;
        }
        .imgslide {
            max-width: 800px;
            height: 330px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .header {
            top: -90px;
        }
        #top {
            margin-top: 100px;
        }
        #page_wrap{
            margin-top: 0px !important;
        }
        .s-pageheader--home{
            min-height: 15px;
            height: 15px;
        }
        #page_wrap3 {
            height: 310px;
        }
    }

    @media only screen and (max-width:700px) {
        .carousel-control{
            top: 450px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
        .imgslide {
            max-width: 700px;
            height:300px !important;
            width: 100% !important;
        }
         #sliderpri {
            padding-left: 5% !important;
            width: 100%;
            padding-right: 5% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    }

    @media only screen and (max-width:600px) {
        .carousel-control{
            top: 470px !important;
        }
       
        .imgslide {
            max-width: 600px;
            height: 225px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .post-thumb{
            width: 100% !important;
        }
        .post-title{
            width: 100% !important;
        }
        .descpat{
            width: 100% !important;
        }
         #page_wrap3 {
           height: 150px;
            margin-top: 50px;
            margin-top: 0px !important;
        }
    }
    @media only screen and (max-width:500px) {
        .carousel-control{
            top: 500px !important;
        }
        .imgslide {
            max-width: 600px;
            height: 180px !important;
            width: 100% !important;
        }
    }

    @media only screen and (max-width:400px) {
        .carousel-control {
            top: 85px !important;
        }

        #sliderpri {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .imgslide {
            max-width: 400px;
            height: 150px !important;
            width: 100% !important;
        }
        .lrslider1{
            margin-top: -385px;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
        }
        #slider_captionspri {
            top: 150px !important;
        }
        #page_wrap3 {
           height: 150px;
            margin-top: 50px;
            margin-top: 0px !important;
        }
    }

    .left carousel-control {
        left: 5%;
    }
    
    .slider {
    max-width: 700px;
    }
    
</style>
<div id="page_wrap3" class="slider">
<div id="sliderpri" class="carousel slide col-sm-12  padd-lt0" data-ride="carousel" data-interval="10000">
    <div class="carousel-inner">

            <div class="item active">
                <img alt=" slide" src="\img\andres\IMG_1537.jpg" class="imgslide" style="max-height: 700px; max-width: 700px; width: 100% !important; height: 100%">   
            </div>
             <div class="item">
                <img alt=" slide" src="\img\andres\IMG_1540.jpg" class="imgslide" style="max-height: 700px; max-width: 700px; width: 100% !important; height: 100%">   
            </div>
        
    </div>
    <div class="col-sm-12 col-md-12 col-xs-12 lrslider1">
        <a data-slide="prev" role="button" class="left carousel-control" href="#sliderpri">
            <span class="glyphicon glyphicon-chevron-left" style="left: 25px;top: 15px;"></span>
        </a>
        <a data-slide="next" role="button" href="#sliderpri" class="right carousel-control" style="float: left;padding-right: 0px !important;right: 0px !important;">
            <span class="glyphicon glyphicon-chevron-right" style="left: 25px;top: 15px;"></span>
        </a>
    </div>
    

</div>


</div>
<script type="text/javascript">
 $("#sliderpri").on('slide.bs.carousel', function(evt) {

     var step = $(evt.relatedTarget).index();

     $('#slider_captionspri .carousel-captions:not(#captions-'+step+')').fadeOut('fast', function() {

        $('#captions-'+step).fadeIn();

     });

  });

</script>