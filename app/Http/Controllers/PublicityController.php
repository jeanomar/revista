<?php

namespace App\Http\Controllers;
use Omar\Entities\Publicity;
use Illuminate\Http\Request;
use File;
use FileUpload;

class PublicityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos =  Publicity::withTrashed()->get();
        return view('admin.publicidad',compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public $folder2 = '/uploads';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->get('id')>0) {
            $dato = Publicity::find($request->get('id'));
        }else{
            $dato = new Publicity;
        } 


        $dato->title = $request->get('title');
        $dato->article_id = $request->get('article_id');
        $dato->patrocinador_id = $request->get('patrocinador_id');
        $dato->state = $request->get('statea');
        $dato->state_ini = $request->get('statei');
        $dato->position = $request->get('position');

        if (count($_FILES) > 0){
            // create upload path if it does not exist
            $path = public_path($this->folder2."/publicidad/");
            if(!File::exists($path)) {
                File::makeDirectory($path);
            };
            // Simple validation (max file size 2MB and only two allowed mime types)
            $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']);
            // Simple path resolver, where uploads will be put
            $pathresolver = new FileUpload\PathResolver\Simple($path);
            // The machine's filesystem
            $filesystem = new FileUpload\FileSystem\Simple();
            // FileUploader itself
            $fileupload = new FileUpload\FileUpload($_FILES['url'], $_SERVER);
            $slugGenerator = new FileUpload\FileNameGenerator\Slug();
            // Adding it all together. Note that you can use multiple validators or none at all
            $fileupload->setPathResolver($pathresolver);
            $fileupload->setFileSystem($filesystem);
            $fileupload->addValidator($validator);
            $fileupload->setFileNameGenerator($slugGenerator);
            
            // Doing the deed
            list($files, $headers) = $fileupload->processAll();
            // Outputting it, for example like this
            foreach($headers as $header => $value) {
                header($header . ': ' . $value);
            }
            foreach($files as $file){
                //Remember to check if the upload was completed
                if ($file->completed) {
                    // set some data
                    $filename = $file->getFilename();
                    $url = $this->folder2."/publicidad/". $filename;
                    $dato->url = $url;
                }
            }
        }
        
        $dato->save();
        if (!($request->get('state')>0)) {
            $dato->delete();
        }
        
        $publicitys =Publicity::withTrashed()->where('patrocinador_id',$request->get('patrocinador_id'))->get();
        return  $publicitys->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $publicitys =Publicity::withTrashed()->where('patrocinador_id',$request->get('id'))->get();
        return  $publicitys->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p =Publicity::withTrashed()->find($id);
        $pid = $p->patrocinador_id;
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        
        $publicitys =Publicity::withTrashed()->where('patrocinador_id',$pid)->get();
        return  $publicitys->toArray();
    }
}
