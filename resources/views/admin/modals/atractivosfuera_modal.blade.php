
<div id="modal" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            {!! Form::open(['method'=>'POST', 'route'=>'travel.atractivosfuera', 'id'=>"articleform", 'class'=>'form-horizontal', 'enctype'=>"multipart/form-data", 'files' => true ]) !!}
                <div class='modal-body'>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Nombre</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Nombre' type='text' name='name' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Lugar</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Lugar' type='text' name='lugar' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Ubicación</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Ubicación del atractivo' type='text' name='ubication' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Tiempo caminando</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Tiempo caminando' type='text' name='t1' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Tiempo bicicleta</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Tiempo bicicleta' type='text' name='t2' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Tiempo auto</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Tiempo auto' type='text' name='t3' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Tiempo bus</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Tiempo bus' type='text' name='t4' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Articulo de referencia</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Articulo de referencia' type='text' name='article'>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Estado</label>
                        <div class='col-sm-9'>
                            <input type='checkbox' name='state' checked value="1"> Activo</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Descripción</label>
                        <div class='col-sm-9'>
                            <textarea class='form-control' rows="10" name='descripcion' required></textarea>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'></label>
                        <div class="col-sm-9">
                                                <div class="file-upload" style="width: 100%;">
                                                  <button class="file-upload-btn d3" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Agregar imagen</button>

                                                  <div class="image-upload-wrap">
                                                    <input name="imgdatos" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                                    <div class="drag-text">
                                                      <h3>Arrastra y suelta un archivo o selecciona Agregar imagen</h3>
                                                    </div>
                                                  </div>
                                                  <div class="file-upload-content">
                                                    <img name="img_datos" class="file-upload-image" src="#" alt="your image" style="width: 100%;"/>
                                                    <div class="image-title-wrap">
                                                      <button type="button" onclick="removeUpload()" class="remove-image d3">Quitar <span class="image-title">imagen</span></button>
                                                    </div>
                                                  </div>
                                                </div>

                                        </div>
                                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                    <button type='submit' class='btn btn-flat bg-aqua'>Aceptar</button>
                </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>
