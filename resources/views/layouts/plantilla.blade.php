<!DOCTYPE html>
<html class="no-js" lang="es">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="author" content="© José Eduardo Díaz Camacho (Director) © J. Omar Ludeña Chávez (Desarrollador)">
    <meta name="keywords" content="Cajamarca donde todo empezó, Cajamarca donde todo empezo, Revista Cajamarca donde todo empezó, revista cajamarca">
    <meta name="google-site-verification" content="i6XPBsTfQ6EM01GtOSNU5HvgY64foYci22OM2Q6kaD8" />
    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== --> 
    <link href="{{ asset('assets/plugins/css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/custom-styles.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/image-effects.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/font-awesome-ie7.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/base.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/vendor.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/css/micss.css') }}" rel="stylesheet" />

    @stack('css')
    <!-- script
    ================================================== -->
        <script src="{{ asset('assets/plugins/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/modernizr.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/pace.min.js') }}" async="async"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/pubads_impl_rendering_263.js') }}" async="async"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/show_companion_ad.js') }}" async="async"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/sdk.js') }}" async="async"></script>
  
    <script src="{{ asset('assets/plugins/Gatopardo_files/beacon.js') }}"></script>
    <script src="{{ asset('assets/plugins/Gatopardo_files/eapps.js') }}" async="async"></script>
    
    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

</head>

<body id="top" style="padding-top: 15px;">

    <section id="pagehead" class="s-pageheader s-pageheader--home">
        @yield('menu')
        @yield('banner')
    </section> <!-- end s-pageheader -->
    @yield('content')

    @include("fixed.footer")

    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
            <div class="line-scale">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>


    <!-- Java Script
    ================================================== -->

    <script src="{{ asset('assets/plugins/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/aos.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/main.min.js') }}"></script>
    
    <script>
        $('.close-mobile-menu').click(function(event) {
           $('.sub-menu').css({display: 'none'});
        });

        //window.onscroll = function() {myFunction()};

        /*var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
          if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky");
          } else {
            navbar.classList.remove("sticky");
          }
        }*/

        $('.has-children').mouseover(function(event){
            $('.menus').css({'margin-bottom': '200px'});
        }).mouseout(function() {
            $('.menus').css({'margin-bottom': '0px', 'transition': 'margin 700ms'});
        });
        $('.sub-menu').mouseover(function(event){
           $('.menus').css({'margin-bottom': '200px'});
        }).mouseout(function() {
            $('.menus').css({'margin-bottom': '0px', 'transition': 'margin 700ms'});
        });
    </script>
    @stack('js')
</body>

</html>