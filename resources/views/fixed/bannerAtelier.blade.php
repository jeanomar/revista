<style>
    .slider-container {
        margin-top:35px;
        height:100%;
    }

    .carousel-caption {
        position: absolute;
        top: 0;
        text-align: left;
        left: inherit;
        right: inherit;
        width: 100%;
        color:#000;
        bottom:-95px;
    }

    .carousel-caption:not(#caption-0) {
        display: none;
    }

    .carousel-caption:not(#caption-0) {
        display: none;
    }

    .carousel-control2 {
        background: none repeat scroll 0 0 #000 !important;
        bottom: 0;
        color: #fff;
        font-size: 20px;
        height: 50px;
        opacity: 0.5;
        position: absolute;
        
        right: 108px !important;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
        top: 296px;
        width: 70px;
        z-index: 9999;
         width: 40px;
    }

    #slider {
        padding-left: 0px;
        padding-right: 0px;
    }

    @media only screen and (max-width:1600px) {
        .carousel-inner{
            max-width: 1200px;
            height: 650px !important;
            width: 100% !important;
        }
        .carousel-control2{
            top: 60px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
    }
    
    @media only screen and (max-width:1200px) {
        .fusion{
            margin-top: 200px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
    }
    @media only screen and (max-width:1000px) {
        .lrslider {
            margin-top: -400px !important;
        }
        #expert{
            width: 90% !important;margin-left: 5%;margin-right: 5%;
        }

         #slider {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        
        #slider_captions{
            margin-left: 5% !important;
            margin-right: 5% !important;
        }
        .fusion{
            margin-top: 100px !important;
        }
    }

    @media only screen and (max-width:900px) {
        .lrslider {
            margin-top: -450px !important;
        }
         #slider {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }
        .fusion{
            margin-top: 150px !important;
        }
        #slider{
            max-width: 900px;
            width: 100%;
        }
        #imgatel{
            max-width:900px !important;
            width: 100% !important;
            height: 600px !important;
        }
        
    }

    @media only screen and (max-width:800px) {
        .lrslider {
            margin-top: -450px !important;
        }
        .carousel-control2{
            top: 250px !important;
        }
        .carousel-inner{
            max-width: 800px;
            height: 330px !important;
            width: 100% !important;
        }
         #slider{
            max-width: 800px;
            width: 100%;
        }
        #imgatel{
            max-width:800px !important;
            width: 100% !important;
            height: 350px !important;
        }
    }

    @media only screen and (max-width:700px) {
        .lrslider {
            margin-top: -400px !important;
        }
        .carousel-control2{
            top: 250px !important;
        }
        .carousel-inner{
            max-width: 700px;
            height: 300px !important;
            width: 100% !important;
        }
        #slider{
            max-width: 700px;
            width: 100%;
        }
        #imgatel{
            max-width: 700px !important;
            width: 100% !important;
            height: 300px !important;
        }
       
    }
    @media only screen and (max-width:600px) {
        .carousel-control2{
            top: 250px !important;
        }
        .carousel-inner {
            max-width: 600px;
            height: 250px !important;
            width: 100% !important;
        }
        #slider{
            max-width: 600px;
            width: 100%;
        }
        #imgatel{
            max-width: 600px !important;
            width: 100% !important;
            height: 250px !important;
        }

    }

    @media only screen and (max-width:400px) {
        #imgatel{
            max-width: 400px !important;
            width: 100% !important;
            height: 170px !important;
        }
        
        .carousel-control2{
            top: 60px !important;

        }
        .left.carousel-control2{
            margin-left: 5%;
        }
        .right.carousel-control2{
            margin-right: 5%;
        }
        .lrslider {
            position: static;
            max-width: 400px;
            width: 100%;
        }

         #slider {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }

        .carousel-inner{
            max-width: 400px;
            height: 170px !important;
            width: 100% !important;
        }
        #slider_captions{
           margin-top: 170px;
        }
        .carousel-captions{
            background-color: #fff !important;
        }
        
    }

    .left carousel-control2 {
        left: 5%;
    }
    
    .lrslider {
        margin-top: -500px;
    }

    .sltxt3 {
        font-size: 34px;
    }
    .sltxt4 {
        font-size: 21px;
    }
    #slider_captions {
        margin-bottom: 100px;
    }
    
</style>

<div id="slider" class="carousel slide col-sm-12  padd-lt0" data-ride="carousel" data-interval="10000">

    <div class="carousel-inner col-md-12" style="padding-left: 0px;padding-right: 0px;">

        @php
            $l=0;
        @endphp
        @foreach($andes as $sub)
            @foreach($sub->articulos->take(2)->sortByDesc('id') as $art)
                @foreach($art->images as $img)
                    @if($img->type =="p")
                    <div class="item {{($l == 0)?'active':''}} itemimg">
                        <img id="imgatel" style="background-image:url(<?=Croppa::url(asset($img->url), 970,650)?>); max-height: 700px; max-width: 1200px; width: 100% !important; height: 650px; background-position: center;background-size: cover;" >
                    </div>
                        @php
                            $l++;
                        @endphp
                    @endif
                @endforeach
            @endforeach
        @endforeach
    </div>
    <div class="col-sm-12 col-md-12 col-xs-12 lrslider">
        <a data-slide="prev" role="button" href="#slider" class="left carousel-control2" href="#slider" style="left: 0px;">
            <span class="glyphicon glyphicon-chevron-left" style="top: 15px;left: 0px;left: 0px !important;"></span>
        </a>
        <a data-slide="next" role="button" href="#slider" class="right carousel-control2" style="float: left;padding-right: 0px !important;right: 0px !important;" href="#slider">
            <span class="glyphicon glyphicon-chevron-right" style="top: 15px;left: 0px;left: 0px !important;"></span>
        </a>
    </div>
    

</div>
<div class="col-sm-12 col-xs-12">
<div  class="col-sm-12 col-xs-12" style="padding-left: 0px;">
<div  class="col-sm-12 col-xs-12" style="padding-left: 0px;">  
<div id="slider_captions" style="padding-left: 0px;padding-right: 0px;">
    <div  style="margin-left: 0px;margin-right: 0px;">
        @php
            $k=0;
        @endphp
        @foreach($andes as $sub)
            @foreach($sub->articulos->take(2)->sortByDesc('id') as $art)
                <div style="width: 90%;" id="caption-{{$k}}" class="carousel-caption" style=" bottom: auto;">
                  <a href="{{URL::action('ArticleController@ver', $art->id) }}" class="sinsub">
                     @php
                        $cadena1 = "";
                        $cadena2 = "";
                        $cadena = $art->title;
                        $max=0;
                        for($i=0;$i<(strlen ($cadena));$i++){
                            if (ctype_upper(substr($cadena,$i,1))) {
                                $max=$i;
                            }
                        } 
                        $cadena1 = ((strlen ($cadena)-1) == $max) ? $cadena : substr($cadena,0,$max);
                        $cadena2 = ((strlen ($cadena)-1) == $max) ? "" : substr($cadena, $max);
                    @endphp
                    <h3 class="sltxt3" style="margin-top: 10px;margin-bottom: 5px; font-family: lora;color: #8d6b41e6 !important;text-shadow: none !important;font-size: 28px !important;line-height: 1;">{{$cadena1}}</h3>
                    @if($cadena2 != '')
                    <h3 class="sltxt3" style="margin-top: -5px;margin-bottom: 5px; font-family: lora;color: #8d6b41e6 !important;text-shadow: none !important;font-size: 28px !important;line-height: 1;">{{$cadena2}}</h3>
                    @endif
                    <div>
                        <p class="sltxt4" style='font-family: "calibri !important";font-size: 16px;text-shadow: none !important;margin-bottom: 7px;'>{{$art->subtitle}}</p>
                    </div>
                    
                  </a>  
                  <div id="author" class="rn">
                    <a href="{{URL::action('ArticleController@verxcategorydetail', $art->category->id) }}" class="sinsub" style="font-weight: bold;text-shadow: none !important;">
                        {{$art->category->title}}
                    </a>
                </div>
                </div>
                
                @php
                    $k++;
                @endphp
            @endforeach
        @endforeach
        
    </div>
</div>
 </div>   
</div>
</div>
<script type="text/javascript">
 $("#slider").on('slide.bs.carousel', function(evt) {

     var step = $(evt.relatedTarget).index();

     $('#slider_captions .carousel-caption:not(#caption-'+step+')').fadeOut('fast', function() {

        $('#caption-'+step).fadeIn();

     });

  });

</script>