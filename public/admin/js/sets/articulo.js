/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */

 // set the csrf-token for all AJAX requests
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
});

$('#fileupload').bind('fileuploadsubmit', function (e, data) {
        // The example input, doesn't have to be part of the upload form:

        var input = data.context.find('input[name=description]');
        var idart = $('input[name=id]');
        var typeimg = data.context.find('input:radio[name="typeimg"]');
        
        data.formData = {description: input.val(), id:idart.val(), typeimg:typeimg[0].checked};
        if (!data.formData.description) {
          data.context.find('button').prop('disabled', false);
          data.context.find('input[name=description]').focus();
          return false;
        }
    });

$(function(){
    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $("#articleform")[0].reset();
        $('.modal-title').text("Nuevo Artículo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});
        $('input[name=kode]').inputmask({mask: "99999999999"});

        $("#modal").on('hidden.bs.modal', function(e){
        //            $('.modal').remove();
        });
    });
    //EDIT
    $('#dataTable tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        var subtitle = $(this).data('subtitle');
        var descripcion = $(this).data('descripcion');
        var state = $(this).data('state');
        var category_id  = $(this).data('category_id');
        var principal  = $(this).data('principal');

        $('.modal-title').text("Editar Artículo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=title]").val(title);
        $("#modal").find("textarea[name=subtitle]").val(subtitle);
        $("#modal").find("textarea[name=descripcion]").val(descripcion);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modal").find('input:checkbox[name="principal"]').prop("checked",principal);
        $("#modal").find("select[name=subcat]").val(category_id);

        $("#modal").on('hidden.bs.modal', function(e){
            $("#modal").find("input[name=id]").remove();
        });
    });

    $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: 'articulos/img/upload'
    });

    var tx=0;
    //photos
    $('#dataTable tbody').on('click', 'tr td button#btn-img', function(e) {
        e.preventDefault();
        $('#btnSimpan').removeAttr('disabled','disabled');
        tx=0;
        var id = $(this).data('id');
        var title = $(this).data('title');
        var subtitle = $(this).data('subtitle');
        var descripcion = $(this).data('descripcion');
        var state = $(this).data('state');
        var category_id  = $(this).data('category_id');

        $('.modal-title').text("Imágenes");
        $('#modal_img').modal({keyboard: false, backdrop: 'static'});
        $("input[name=id]").remove();
        $("#modal_img").find("#fileupload").append('<input type="hidden" name="id">');

        $("#modal_img").find("input[name=id]").val(id);
        $("#modal_img").find("label[name=title]").text(title);

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            data: {
                    'id': id
                },
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
            e.preventDefault();
        });

        $("#modal_img").on('hidden.bs.modal', function(e){
            $("#tb_imgs").find("tbody").empty();
            tx=0;
        });

        $("#btnSimpan").on('click', function(e){
            tx++;
            if (tx == 1) {
                $(this).attr("disabled", "disabled");
                var token = $("input[name=_token]").val();
                $.ajax({
                    url: window.location.href+'/img/principal',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'post',
                    datatype: 'json',
                    data: {id: $('input[name=typeimg]:checked').val()},
                    success:function(response){
                        $('#modal_img').modal('toggle');
                    },
                    error:function(msj){

                    }
                });  
            }
            
        });

    });

});
