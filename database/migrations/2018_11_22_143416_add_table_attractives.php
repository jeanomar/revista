<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAttractives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attractive', function (Blueprint $table) {
            $table->dropColumn('horario');
            $table->string('t1', 500)->after('ubication');
            $table->string('t2', 500)->after('t1');
            $table->string('t3', 500)->after('t2');
            $table->string('t4', 500)->after('t3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attractive', function (Blueprint $table) {
            //
        });
    }
}
