/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */

$(function(){

    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $("#articleform")[0].reset();
        $('.modal-title').text("Nuevo atractivo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});  
        $("#modal").find("img[name=img_datos]").attr('src', '#');
    });

    //EDIT
    $('#dataTable tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var descripcion = $(this).data('descripcion');
        var state = ($(this).data('state') == "")?true:false;
         var url  = $(this).data('url');
        $('.modal-title').text("Editar atractivo");
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input class="idf" type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=name]").val(name);
        $("#modal").find("textarea[name=descripcion]").val(descripcion);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modal").find("img[name=img_datos]").attr('src', url);
        
        $("#modal").on('hidden.bs.modal', function(e){
           $("#modal").find("input[name=id]").remove();
        });
    });

});
