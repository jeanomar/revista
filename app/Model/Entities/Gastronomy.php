<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Gastronomy extends Model
{
	use SoftDeletes;
    protected $table = 'gastronomy';
    public $timestamps = true;
    protected $fillable = ['id', 'name', 'url', 'descripcion', 'user_id'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
