<?php

use Illuminate\Database\Seeder;
use Omar\Entities\Menu;

class SubmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $revista = $menu->where('title', 'La Revista')->pluck('id')->first();
        $tradicion = $menu->where('title', 'Tradición')->pluck('id')->first();
        $fusion = $menu->where('title', 'Fusión cultural')->pluck('id')->first();
        $andes = $menu->where('title', 'Andes World')->pluck('id')->first();
        $gente = $menu->where('title', 'Gente')->pluck('id')->first();
        $patrocinadores = $menu->where('title', 'Patrocinadores')->pluck('id')->first();
        $travel = $menu->where('title', 'Travel')->pluck('id')->first();

        $submenu1 = new Menu();
        $submenu1->title = 'Artesanía';
        $submenu1->url = 'artesania';
        $submenu1->menu_id = $tradicion;
        $submenu1->save();

        $submenu2 = new Menu();
        $submenu2->title = 'Mística y Arqueología';
        $submenu2->url = 'mistica-arqueologia';
        $submenu2->menu_id = $andes;
        $submenu2->save();

        $submenu3 = new Menu();
        $submenu3->title = 'Relatos';
        $submenu3->url = 'relatos';
        $submenu3->menu_id = $fusion;
        $submenu3->save();

        $submenu4 = new Menu();
        $submenu4->title = 'Historia';
        $submenu4->url = 'historia';
        $submenu4->menu_id = $fusion;
        $submenu4->save();

        $submenu5 = new Menu();
        $submenu5->title = 'Gente';
        $submenu5->url = 'gente';
        $submenu5->menu_id = $andes;
        $submenu5->save();

        $submenu6 = new Menu();
        $submenu6->title = 'Artistas';
        $submenu6->url = 'artistas';
        $submenu6->menu_id = $gente;
        $submenu6->save();

        $submenu7 = new Menu();
        $submenu7->title = 'Conmemoraciones';
        $submenu7->url = 'Conmemoraciones';
        $submenu7->menu_id = $tradicion;
        $submenu7->save();

        $submenu8 = new Menu();
        $submenu8->title = 'Paisajes';
        $submenu8->url = 'paisajes';
        $submenu8->menu_id = $andes;
        $submenu8->save();

        $submenu9 = new Menu();
        $submenu9->title = 'Alrededores';
        $submenu9->url = 'alrededores';
        $submenu9->menu_id = $andes;
        $submenu9->save();

        $submenu10 = new Menu();
        $submenu10->title = 'Fiestas';
        $submenu10->url = 'fiestas';
        $submenu10->menu_id = $tradicion;
        $submenu10->save();


        $submenu11 = new Menu();
        $submenu11->title = 'Artesanos';
        $submenu11->url = 'artesanos';
        $submenu11->menu_id = $gente;
        $submenu11->save();

        $submenu12 = new Menu();
        $submenu12->title = 'Personajes';
        $submenu12->url = 'personajes';
        $submenu12->menu_id = $gente;
        $submenu12->save();

    }
}
