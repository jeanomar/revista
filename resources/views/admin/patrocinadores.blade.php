@extends('admin.layouts.plantilla')
@push('css')
    <!-- DataTables CSS -->
    <link href="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet" />

    <style>
        .resumen{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis; 
        }
        .form-group {
            margin-bottom: 15px !important;
        }
        .inp{
            width: 100% !important;
        }


.panel-default>.panel-heading {
  color: #333;
  background-color: #fff;
  border-color: #e4e5e7;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
}

.panel-default>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  background-color: #eee;
}

.accordion-option {
  width: 100%;
  float: left;
  clear: both;
  margin: 15px 0;
}

.accordion-option .title {
  font-size: 20px;
  font-weight: bold;
  float: left;
  padding: 0;
  margin: 0;
}

.accordion-option .toggle-accordion {
  float: right;
  font-size: 16px;
  color: #6a6c6f;
}

    </style>

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="{{ asset('admin/js/jQuery-File-Upload-9.27.0/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/js/jQuery-File-Upload-9.27.0/css/jquery.fileupload-ui.css') }}">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <link href="{{ asset('admin/dist/css/imgupload.css') }}" rel="stylesheet" />
@endpush
@section('content')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Patrocinadores</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Listado de patrocinadores
                            <button id="btn-tambah" class="btn btn-default pull-right" style="padding-top: 4px;padding-bottom: 4px;margin-top: -5px;">
                                Nuevo patrocinador <i class="glyphicon glyphicon-new-window"></i>
                            </button>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Web</th>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($patrocinadores as $item)
                                    <tr >
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td><p class="resumen" style="max-width: 300px">{{$item->description}}</p></td>
                                        <td>{{$item->web}}</td>
                                        <td><p class="resumen" style="max-width: 200px">{{$item->address}}</p></td>
                                        <td>{{($item->deleted_at == null)?'Activo':'Inactivo'}}</td>
                                        <td class="center" style="min-width: 108px;">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['auspiciadores.state', $item->id], 'class' => 'FormDeleteTime form-group']) !!}
                                                @if($item->deleted_at == null)
                                                    <button type="button" id="btn-edit" class="btn btn-default btn-circle btn-xs" title="Editar" data-id="{{$item->id}}" data-name="{{$item->name}}" data-web="{{$item->web}}" data-descripcion="{{$item->description}}" data-state="{{$item->deleted_at}}" data-email="{{$item->email}}" data-tlf1="{{$item->telephone1}}" data-tlf2="{{$item->telephone2}}" data-address="{{$item->address}}" data-ubication="{{$item->ubication}}"> 
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" id="btn-img" class="btn btn-default btn-circle btn-xs" title="Imágenes" data-id="{{$item->id}}" data-title="{{$item->name}}">
                                                        <i class="fa fa-file-photo-o"></i>
                                                    </button>
                                                    <button type="button" id="btn-pub" class="btn btn-default btn-circle btn-xs" title="Publicidad" data-id="{{$item->id}}" data-title="{{$item->name}}"
                                                        data-toggle="modal" data-target="#modalpub">
                                                        <i class="glyphicon glyphicon-bookmark fa-fw"></i>
                                                    </button>
                                                @endif

                                                {!! Form::button('<i class="fa '.(($item->deleted_at == null)?'fa-ban':'fa-check-circle ').'"></i>', array('type' => 'submit', 'class' => 'specialButton btn btn-default btn-circle btn-xs', 'title'=>(($item->deleted_at == null)?'Desactivar':'Activar') )) !!}

                                            {!! Form::close() !!}

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
  
    @include("admin.modals.patrocinadores_modal")

@endsection

@push('js')
<!-- DataTables JavaScript -->
    <script src="{{ asset('admin/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('admin/js/jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    

    <script>
         $('#dataTable').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]],
            createdRow:function(row, data, dataindex){
                if (data[5]== 'Inactivo') {
                    $(row).css({'color':"rgb(150, 0, 0)"});
                }
            }
        });


        $(".FormDeleteTime").submit(function (event) {
            var x = confirm("Desea "+(($(this).data("item")==0)?'activar':'desactivar')+' este patrocinador?');
            if (x) {
                return true;
            }
            else {
                event.preventDefault();
                return false;
            }
        });

        $(document).ready(function() {

  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});

        function show2(){
          document.getElementById('div1').style.display ='none';
          document.getElementById('div2').style.display = 'block';
        }
        function show1(){
          document.getElementById('div1').style.display = 'block';
          document.getElementById('div2').style.display ='none';
        }

    </script>
    
    <script src="{{ asset('admin/js/sets/patrocinador.js') }}"></script>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                <div class="form-group" style="margin-bottom: 0px;margin-top: 5px;">

                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio"  name="typeimg"></label>
                </div>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!i && !o.options.autoUpload) { %}

                    <button class="btn btn-primary start" disabled>

                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Subir</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.type == 'p') { %}
                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio" value="{%=file.value%}" name="typeimg" checked></label>
                {% } else { %}
                    <label class="col-sm-12" name="lbltypeimg">Principal <input type="radio" value="{%=file.value%}" name="typeimg"></label>
                {% } %}
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Eliminar</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle">
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>

    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/vendor/jquery.ui.widget.js') }}"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/tmpl.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/canvas-to-blob.min.js') }}"></script>
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.blueimp-gallery.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-image.js') }}"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-audio.js') }}"></script>
    <!-- The File Upload video preview plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-video.js') }}"></script>
    <!-- The File Upload validation plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-validate.js') }}"></script>
    <!-- The File Upload user interface plugin -->
    <script src="{{ asset('admin/js/jQuery-File-Upload-9.27.0/js/jquery.fileupload-ui.js') }}"></script>

@endpush

