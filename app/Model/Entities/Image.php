<?php

namespace Omar\Entities;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'url',  'type', 'creditos', 'user_id', 'state', 'article_id', 'banner'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];


    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }

}
