<style>
    .slider-container {
        margin-top:35px;
        height:100%;
    }

    .carousel-captions {
        position: absolute;
        top: 0;
        text-align: left;
        left: inherit;
        right: inherit;
        width: 100%;
        color:#000;
        bottom:-95px;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-captions:not(#captions-0) {
        display: none;
    }

    .carousel-control {
        background: none repeat scroll 0 0 #fff !important;
        bottom: 0;
        color: #fff;
        font-size: 20px;
        height: 50px;
        opacity: 0.5;
        position: absolute;
        
        right: 108px !important;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
        top: 296px;
        width: 70px;
        z-index: 1 !important;
        width: 40px;
    }

    #sliderpri {
        padding-left: 0px;
        padding-right: 0px;
    }

    .lrslider1 {
        margin-top: -600px;
    }
    

    #slider_captionspri {
        top: -250px;
    }
    .sltxt1 {
        font-size: 34px;
    }
    .sltxt2 {
        font-size: 21px;
    }
    .imgslide {
        height: 700px !important;width: 1200px;
    }
    


    @media only screen and (min-width:1200px) {
         .imgslide {
            max-width: 1200px;
            height: 650px !important;
            width: 100% !important;
        }
    }
    
    @media only screen and (max-width:1200px) {
        #sliderpri{
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin-left: 20px !important;
            margin-right: 20px !important;
        }
        .imgslide {
            max-width: 1200px;
            height: 520px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 0px;
          margin-right: 0px;
          width: 90% !important;
       }
       .entry__header.pri{
            margin-left: 20px !important;
            margin-right:20px !important;
            margin-top: 15px !important;
        }
        #page_wrap3{
             margin-top: 0px !important;
             margin-left: 0px !important;
            margin-right: 0px !important;
            padding-right: 40px;
        }
        #slider_captionspri{
            margin-left: 20px !important;
            margin-right: 20px !important;
            width: 100% !important;
        }
    }
    @media only screen and (max-width:1000px) {
        #expert{
            width: 90% !important;margin-left: 5%;margin-right: 5%;
        }

         #sliderpri{
            padding-left: 0% !important;
            padding-right: 0% !important;
        }
        #slider_captionspri{
          margin-left: 5%;
          margin-right: 5%;
          width: 90% !important;
       }
        .imgslide {
            max-width: 1000px;
            height: 550px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 0px;
          margin-right: 0px;
          width: 100% !important;
        }
    }
    @media only screen and (max-width:900px) {
        .lrslider {
            margin-top: -500px !important;
        }
         #sliderpri {
            padding-left: 0% !important;
            padding-right: 0% !important;
        }

        .imgslide {
            max-width: 900px;
            height: 520px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -105px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        
    }

    @media only screen and (max-width:800px) {
        #page_wrap3 {
           padding-right: 0px !important;
        }
        .carousel-control{
            top: 400px !important;
        }
        .lrslider {
            margin-top: -450px !important;
        }
        .imgslide {
            max-width: 800px;
            height: 470px !important;
            width: 100% !important;
        }
        #sliderpri {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 100% !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        #slider_captionspri{
          margin-left: 0px;
          margin-right: 0px;
          width: 100% !important;
          margin-left: 0px !important;
          margin-right: 0px !important;
       }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .header__nav{
        margin-left: 0px !important;
        margin-right: 0px !important;
        width: 100% !important;
       }
       .entry__header.pri{
            padding-left: 0px;
            margin-top: 60px !important;
            margin-bottom: 20px !important;
            margin-left: 0 !important;
            margin-right: 0 !important;
        }
    }

    @media only screen and (max-width:700px) {
        .carousel-control{
            top: 450px !important;
        }
        .lrslider {
            margin-top: -400px !important;
        }
        .imgslide {
            max-width: 700px;
            height: 400px !important;
            width: 100% !important;
        }

        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .entry__header.pri{
            padding-left: 0px;
            margin-top: 40px !important;
            margin-bottom: 10px !important;
        }
    }
    @media only screen and (min-width:600px) {
        #slider_captionspri {
            top: 0px !important;
        }
    }

    @media only screen and (max-width:600px) {
        .carousel-control{
            top: 450px !important;
        }
       
        .imgslide {
            max-width: 600px;
            height: 250px !important;
            width: 100% !important;
        }

        #slider_captionspri {
            top: -90px !important;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        #slider_captionspri {
            top: 0px !important;
        }
        #sliderpri {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 100% !important;
        }
        #slider_captionspri{
          margin-left: 0px;
          margin-right: 0px;
          width: 100% !important;
       }
        
       .entry__header.pri{
            margin-top: 40px !important;
            margin-bottom: 25px;
        }

    }

    @media only screen and (max-width:400px) {
        .carousel-control{
            top: 300px !important;
        }

        #sliderpri {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .imgslide {
            max-width: 400px;
            height: 170px !important;
            width: 100% !important;
        }
        .lrslider1{
            margin-top: -385px;
        }
        .sltxt1 {
            font-size: 24px;
        }
        .sltxt2 {
            font-size: 16px;
        }
        #slider_captionspri {
            top: 168px !important;
        }
        #page_wrap3 {
           /*height: 300px;*/
            margin-top:0px !important;
        }
        #slider_captionspri{
          margin-left: 0px;
          margin-right: 0px;
          width: 100% !important;
       }
        .header__nav{
        margin-left: 0px !important;
        margin-right: 0px !important;
        width: 100% !important;
       }
        .entry__header.pri{
            padding-left: 0px;
            margin-top: 35px !important;
            margin-bottom: 15px !important;
            padding-bottom: 0px;
        }

    }

    .left carousel-control {
        left: 5%;
    }
    
    
    
</style>
   <div class="pageheader-content row" style="margin-top: 15px;">     
        <div class="col-full">
            
            <div id="page_wrap3" class="slider" style="margin-top: 0px;margin-bottom: 50px; margin-top:0px !important">
                <div id="sliderpri" class="carousel slide col-sm-12  padd-lt0" data-ride="carousel" data-interval="0">
                    <div class="carousel-inner">
                        @php
                            $i=0;
                        @endphp
                        @foreach($articulo->images as $img)
                            <div class="item {{($i == 0)?'active':''}}">
                                <img class="imgslide" style="background-image:url(<?=Croppa::url(asset($img->url), 1160,650)?>); max-height: 650px; max-width: 1200px; width: 100% !important; height: 100%; background-position: center;background-size: cover;">   
                            </div>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                        
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 lrslider1">
                        <a data-slide="prev" role="button" class="left carousel-control" href="#sliderpri">
                            <span class="glyphicon glyphicon-chevron-left" style="left: 10px;top: 15px;"></span>
                        </a>
                        <a data-slide="next" role="button" href="#sliderpri" class="right carousel-control" style="float: left;padding-right: 0px !important;right: 0px !important;">
                            <span class="glyphicon glyphicon-chevron-right" style="left: 10px;top: 15px;"></span>
                        </a>
                    </div>
                </div>
                <div id="slider_captionspri" class="col-sm-12 col-md-12 col-xs-12" style="top: -130px;">
                    <div  style="margin-left: 3%;margin-right: 3%;">
                        @php
                            $j=0;
                        @endphp
                        @foreach($articulo->images as $img)
                            <div id="captions-{{$j}}" class="carousel-captions" style="background-color: rgba(0, 0, 0, 0.84); left: 0px; padding-top: 0px; margin-bottom: 0px; padding-bottom: 10px; border-bottom-style: solid; border-bottom-width: 0px; bottom: auto;">
                                <span class="sinsub">
                                    <h3 class="text-center sltxt1" style="color: #fff !important;margin-top: 10px;text-align: left;padding-left: 40px;font-family: calibri;font-size: 18px;padding-right: 40px;padding-bottom: 7px;">{{$img->title}}</h3>
                                </span>
                            </div>
                            @php
                                $j++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
$( window ).on( "load", function() { 

    $("#sliderpri").on('slide.bs.carousel', function(evt) {

        var step = $(evt.relatedTarget).index();

        $('#slider_captionspri .carousel-captions:not(#captions-'+step+')').fadeOut('fast', function() {
            $('#captions-'+step).fadeIn();
            
            $('#page_wrap3').height($('.active').height()+$('#captions-'+step).height());
            $('#slider_captionspri').height($('#page_wrap3').height());
            //$('#pagehead').height($('#titlees').height()+$('#page_wrap3').height()+100);
             autoHeightAnimate($('#pagehead'), 0);
        });

    });
    $('#page_wrap3').height($('.active').height()+$('#captions-0').height());
    $('#slider_captionspri').height($('#page_wrap3').height());
    //$('#pagehead').height($('#titlees').height()+$('#page_wrap3').height()+100);
    autoHeightAnimate($('#pagehead'), 0);

    /* Function to animate height: auto */
    function autoHeightAnimate(element, time){
        var curHeight = $('#page_wrap3').height()+260, // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({ height: curHeight }, time); // Animate to Auto Height
    }
});
</script>