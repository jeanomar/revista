@extends('admin.layouts.plantilla')
@push('css')
<style>
	.tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#369;
    font-weight:700;
    position:relative
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#fff;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#369;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#369;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
</style>
@endpush
@section('content')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menu</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

						<div class="container" style="margin-top:30px;margin-bottom: 30px;">
						    <div class="row">
						        <div class="col-md-6 col-xs-12">
						            <ul id="tree1">
						            	@foreach($datos as $item)
						                <li style="color:{{($item->deleted_at==null)?'#369':'#8a1d31'}}">
						                	<a href="#" style="color:{{($item->deleted_at==null)?'#369':'#8a1d31'}}">{{$item->title}}</a>
						                    <ul>
						                    	<li>
						                    		{!! Form::open(['method' => 'DELETE', 'route' => ['menudel', $item->id]]) !!}
						                    			@if($item->deleted_at==null)
						                    			<button class="btnedit" style="width: 20px;" data-id="{{$item->id}}" data-menu_id="{{$item->menu_id}}" data-title="{{$item->title}}" title="Editar"><i class="fa fa-pencil"></i></button>
						                    			@endif
						                    			<button  style="width: 20px;color:{{($item->deleted_at==null)?'#369':'#8a1d31'}}" title="{{($item->deleted_at==null)?'Desactivar':'Activar'}}"><i class="fa {{($item->deleted_at==null)?'fa-ban':'fa-check-circle'}}"></i></button>
						                    		{!! Form::close() !!}
						                    	</li>
						                    	@if($item->deleted_at==null)
						                    	@foreach($item->asubmenus as $s)
						                        <li style="height: 34px; color:{{($s->deleted_at==null)?'#369':'#8a1d31'}}">
						                        	<div class="col-md-10 col-xs-8" style="padding-left: 0px;">
						                    			{{$s->title}}
						                    		</div>
						                    		<div class="col-md-2">
						                    			{!! Form::open(['method' => 'DELETE', 'route' => ['menudel', $s->id]]) !!}
						                    			<button class="pull-right" style="width: 20px;color:{{($s->deleted_at==null)?'#369':'#8a1d31'}}" title="{{($s->deleted_at==null)?'Desactivar':'Activar'}}"><i class="fa {{($s->deleted_at==null)?'fa-ban':'fa-check-circle'}}"></i></button>
						                    			@if($s->deleted_at==null)
						                    			<button class="pull-right btnedit" style="width: 20px;" data-id="{{$s->id}}" data-menu_id="{{$s->menu_id}}" data-title="{{$s->title}}" title="Editar"><i class="fa fa-pencil"></i></button>
						                    			@endif
						                    			{!! Form::close() !!}
						                    		</div>
						                        </li>
						                        @endforeach
						                        <li style="height: 34px">
						                        	{!! Form::open(['method'=>'POST', 'route'=>'menu' ]) !!}
						                    		<div class="col-md-11 col-xs-10" style="padding-left: 0px;">
						                    			<input type="hidden" name="menu_ids" value="{{$s->menu_id}}">
						                    			<input type="text" class="form-control" name="titles" placeholder="Nombre de subcategoría">
						                    		</div>
						                    		<div class="col-md-1">
						                    			<button class="pull-right" style="width: 20px;" title="Guardar"><i class="fa fa-save"></i></button>
						                    		</div>
						                    		{!! Form::close() !!}
						                    	</li>
						                    	@endif
						                    </ul>
						                </li>
						                @endforeach
						                <li>
						                	<ul>
						                        	{!! Form::open(['method'=>'POST', 'route'=>'menu', 'style'=>'margin-top: -25px;margin-left: 10px;' ]) !!}
						                    		<div class="col-md-11 col-xs-10" style="padding-left: 0px;">
						                    			<input type="hidden" name="menu_ids" >
						                    			<input type="text" class="form-control" name="titles" placeholder="Nombre de categoría padre">
						                    		</div>
						                    		<div class="col-md-1">
						                    			<button class="pull-right" style="width: 20px;" title="Guardar"><i class="fa fa-save"></i></button>
						                    		</div>
						                    		{!! Form::close() !!}
						                	</ul>
						                </li>
						            </ul>
						            
						        </div>
                                <div class="col-xs-12 hidden-sm hidden-md hidden-lg"> <br></div>
                                <div class="col-md-6 col-xs-12">
						        	<div id="editform" class="panel panel-info">
				                        <div class="panel-heading">
				                            Editar
				                        </div>
				                        <!-- /.panel-heading -->
				                        {!! Form::open(['method'=>'POST', 'route'=>'menu' ]) !!}
				                        <div class="panel-body">
				                        	<div class='form-group'>
						                        <label class='col-sm-2 control-label'>Categoría</label>
						                        <div class='col-sm-10'>
						                        	<input type="hidden" name="id">
						                        	<input type="hidden" name="menu_id">
						                            <input type="text" class='form-control' name='title'>
						                        </div>
						                    </div>
						                    
				                        </div>
				                        <div class="panel-footer">
				                        	<div class='form-group'>
						                        <button type="submit" class="btn btn-info form-control">Guardar</button>
						                    </div>
				                        </div>
				                        {!! Form::close() !!}
				                     </div>
						        </div>
						    </div>
						</div>

                    </div>
                </div>
			</div>
		</div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
@endsection

@push('js')
<script>

$("#editform").hide();	

//EDIT
$('.btnedit').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    var menu_id = $(this).data('menu_id');
    var title = $(this).data('title');

    $("input[name=id]").val(id);
    $("input[name=menu_id]").val(menu_id);
    $("input[name=title]").val(title);

    $("#editform").show();	
});

$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();


</script>
@endpush