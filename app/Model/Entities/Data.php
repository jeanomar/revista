<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use SoftDeletes;
    protected $table = 'data';
    public $timestamps = true;
    protected $fillable = ['id', 'name', 'valor1',  'valor2', 'type_id', 'father_id', 'user_id'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];


    public function categorysub()
    {
        return $this->belongsTo(Menu::class, 'type_id');
    }

    public function padre()
    {
        return $this->belongsTo(Data::class, 'father_id');
    }

    public function hijos()
    {
        return $this->hasMany(Data::class, 'father_id');
    }
}
