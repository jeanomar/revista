<?php

namespace Omar\Entities;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use SoftDeletes;
    protected $table = 'menu';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'access', 'url', 'user_id', 'menu_id', 'state','description'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];


    public function submenus()
    {
        return $this->hasMany(Menu::class, 'menu_id');
    }

    public function asubmenus()
    {
        return $this->hasMany(Menu::class, 'menu_id')->withTrashed();
    }

    public function datos()
    {
        return $this->hasMany(Data::class, 'type_id');
    }

    public function padre()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function articulos()
    {
        return $this->hasMany(Article::class, 'category_id');
    }
}
