@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')

@push('css')
<link href="{{ asset('assets/plugins/css/travel.min.css') }}" rel="stylesheet" />
<style>
    .andes{
        padding-top: 0px;
    }
    .sizm3{
        padding-top: 0px;
    }
    .s-pageheader{
      margin-bottom: -100px !important;
    }
    #page_wrap{
      max-width: 1160px;
    }
    .s-pageheader--home{
        min-height: 100px;
        height: 120px;
        padding-top: 0px;
        margin-bottom: 0px !important;
    }
    .contentpersonal{
        max-width: 1160px;
        margin-left: auto;
        margin-right: auto;
    }
    .prop-img{
        float: left;
    }
    .prop-desc{
        float: right; 
    }
    .s-footer{
        width: 100% !important;
        max-width: 1160px;
        margin-left: auto;
        margin-right: auto;
        float: none;
    }
    p{
        font-family: calibri !important;
    }
    p.site-director{
        font-family: lora !important;
    }
    p.site-address{
        font-family: lora !important;
    }
    p.site-copyright{
        font-family: lora !important;
    }
    @media screen and (min-width:1200px) {
        .row{
            width: 100%;
        }
        [class*="col-"] {
            padding: 0 0;
        }
    }
    @media screen and (min-width:1170px) {
        .tb.andes{
            margin-top: 0px !important;
        }
    }
  
    @media screen and (max-width:1200px) {
        .contentpersonal{
            max-width: 90%;
            margin-left: auto;
            margin-right: auto;
        }
        #page_wrap{
            max-width: 90% !important;
        }
    }
    @media screen and (max-width:910px) {
        p {
            margin: 2px 0 !important;
        }
        .row.site-name.sizm3{
            margin-top: 10px;
        }
    }
    @media screen and (max-width:800px) {
        .sizm31 {
            margin-top: 20px !important;
        }
        .tb.andes{
            margin-top: 0px !important;
        }
    }
    @media screen and (max-width:600px) {
        .accordionBgMod > img, .accordionFullMod > img {
            width: 100%;
        }
        .viewResult{
            padding-left: 0px !important;
        }
        img{
            margin-bottom: 5px !important;
        }
        .viewResult{
            margin-top: -20px !important;
        }
        .accordionFullMod.sectionInfo.clearfix{
            margin-bottom: 40px !important;
        }
    }
    @media screen and (max-width:400px) {
        .trss{
            font-size: 20px;
        }
        .sizm31 {
            margin-top: 15px !important;
        }
    }
    @media screen and (max-width:350px) {
        .trss{
            font-size: 18px;
        }
    }
    @media screen and (max-width:300px) {
        .trss{
            font-size: 16px;
        }
    }

</style>
@endpush

@section('menu')
    @include("fixed.menu2")
@endsection


@section('content')
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left; margin-left: 0px; margin-right: 0px; width: 100% !important; margin-top: 25px; margin-bottom: 35px !important;">
        <a class="sinsub trss" id="dirtravel1">{{$m->submenus[0]->title}}</a>
  </h3>
</div>
<div class="contentpersonal">
    <div class="level2" style="display: block;">
        <div class="dato clearfix">
            <img class="datoImg" src="<?=Croppa::url(asset($data->find(13)->valor1), 598,null)?>" style="max-width: 598px; width: 100%; height: auto;">
            <h3 class="datoTitle">{{$m->submenus[0]->submenus[0]->title}}</h3>
            <p class="h7" style="color: #9e2840;">{{$data->find(1)->name}}</p>
            <p class="datoinfo">{{$data->find(1)->valor1}}</p>
            <span class="datoNum"> 
                <sub>{{$data->find(2)->name}}: {{$data->find(2)->valor1}} ({{$data->find(2)->valor2}})</sub>
            </span> 
            <span class="datoNum" style="margin-top: 0px;">
                <em class="blue"></em>
                <sub>{{$data->find(3)->name}}: {{$data->find(3)->valor1}} ({{$data->find(3)->valor2}})</sub>
            </span>
        </div>
        <div class="dato clearfix">
            <h3 class="datoTitle">{{$m->submenus[0]->submenus[1]->title}}</h3>
            <p class="h7 datoText" style="color: #9e2840;">{{$data->find(4)->name}}</p>
            <p class="datoText">{{$data->find(4)->valor1}}</p>
            <p class="h7" style="color: #9e2840;">{{$data->find(5)->name}}</p>
            <div style="max-width: 600px">
                @foreach($data->where('father_id',5) as $t)
                    <div style="max-width: 598px; width: 100%; float: left; margin-bottom: 15px;font-family: calibri;">
                        <span class="datoNum" style="margin-top: 0px;">
                            <div style="max-width: 698px; width: 100%">{{$t->name}} {{$t->valor1}} {{$t->valor2}}</div>
                        </span>
                    </div> 
                @endforeach
            </div>
        </div>
        
        
    </div>
</div>
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left; margin-left: 0px; margin-right: 0px; width: 100% !important; margin-top: 25px; margin-bottom: 35px !important;">
      <a class="sinsub trss" id="dirtravel2">{{$m->submenus[1]->title}}</a>
  </h3>
</div>
<div class="contentpersonal">
    <div class="level2" style="display: block;">
        <table style="border-collapse:collapse;" cellspacing="0">
            <tbody>
                @foreach($dentro as $d)
                <tr>
                    <td>
                        <div class="accordionFullMod sectionInfo clearfix" >
                            <img src="<?=Croppa::url(asset($d->url), 120,120)?>" width="120" height="120">
                            <p class="sectionTitle" style="margin-bottom: 3px; font-family: lora !important;">
                                <span>{{$d->name}}</span>
                            </p>
                            <p style="margin-bottom: 4px;">
                                <span class="h7">
                                    Ubicación :
                                </span>
                                <span>{{substr($d->ubication, 0,47).((strlen($d->ubication)>47)?"...":"")}}</span>
                            </p>
                            <p class="desc-hor" style="margin-bottom: 3px;">
                                <span class="h7">
                                    Lugar :
                                </span>
                                <span>{{$d->lugar}}</span>
                            </p>
                            <p>
                                <span id="desc{{$d->id}}" class="desc-resu">
                                    {{substr($d->descripcion, 0,125)."..."}}
                                </span>
                            </p>
                            <br>
                            <div class="viewResult" style="padding-left: 120px; margin-top: -15px;">
                                <a title="{{$d->name}}" class="vermascontent sinsub" data-toggle="modal" data-target="#atractivodentro{{$d->id}}" style="cursor: pointer;font-style: italic; font-weight: bold;font-size: 13px">
                                    Ver más
                                </a>
                            </div>
                            
                           
                            <!-- Modal -->

                            <div class="modal fade" id="atractivodentro{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header" style="border-bottom-width: 0px;">
                                   <span aria-hidden="true" data-dismiss="modal" class="pull-right" style="cursor: pointer;"><i class="fa fa-times"></i></span>
                                  </div>
                                  <div class="modal-body">
                                        
                                    <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
                                        <div class="fancybox-outer">
                                            <div class="fancybox-inner" style="overflow: auto; width: auto; height: auto;">
                                                <div style="display: block; width: auto;">
                                                    <img src="<?=Croppa::url(asset($d->url), 600,600)?>" alt="imagen" class="noimg inner-fancy-img" width="200px" height="auto" style="width: 240px">
                                                    <h4 style="font-size: 19px;">
                                                        {{$d->name}}
                                                    </h4>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            Ubicación :</span>
                                                        {{$d->ubication}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        {{$d->lugar}}
                                                    </p>
                                                    
                                                    <p class="descp" style="font-size: 15px;">
                                                        {{$d->descripcion}}
                                                        @if($d->article != "")
                                                        <a href="{{$d->article}}">Leer artículo</a>
                                                        @endif
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
                @if($dentro->count() == 0)
                <tr>
                    <td>
                    <span class="noRecords">
                    ¡No se han encontrado lugares en la ciudad!</span>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left; margin-left: 0px; margin-right: 0px; width: 100% !important; margin-top: 35px; margin-bottom: 35px !important;">
      <a class="sinsub trss" id="dirtravel3">{{$m->submenus[2]->title}}</a>
  </h3>
</div>
<div class="contentpersonal">
    <div class="level2" style="">
        <table style="border-collapse:collapse;" cellspacing="0">
            <tbody>
                @foreach($fuera as $f)
                <tr>
                    <td>
                        <div class="accordionFullMod sectionInfo clearfix">
                            <img src="<?=Croppa::url(asset($f->url), 600,600)?>" width="120" height="105">
                            <p class="sectionTitle" style="margin-bottom: 3px; font-family: lora !important;">
                                <span>{{$f->name}}</span>
                            </p>
                            <p style="margin-bottom: 3px;">
                                <span class="h7">
                                    Ubicación :
                                </span>
                                <span>
                                    {{substr($f->ubication, 0,47).((strlen($f->ubication)>47)?"...":"")}}
                                </span>
                            </p>
                            <p style="margin-bottom: 4px;">
                                <span class="h7">
                                    Lugar : </span>
                                    {{$f->lugar}}
                                
                            </p>
                            <p>
                                <span>{{substr($f->descripcion, 0,125)."..."}}</span>
                            </p>
                            <br>
                            <div class="viewResult"  style="padding-left: 130px; margin-top: -15px;">
                                <a class="vermascontent sinsub" title="{{$f->name}}" data-toggle="modal" data-target="#atractivofuera{{$f->id}}" style="cursor: pointer;font-style: italic; font-weight: bold;font-size: 13px">
                                    Ver más
                                </a>
                            </div>
                                
                            <!-- Modal -->

                            <div class="modal fade" id="atractivofuera{{$f->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header" style="border-bottom-width: 0px;">
                                   <span aria-hidden="true" data-dismiss="modal" class="pull-right" style="cursor: pointer;"><i class="fa fa-times"></i></span>
                                  </div>
                                  <div class="modal-body">
                                        
                                    <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
                                        <div class="fancybox-outer">
                                            <div class="fancybox-inner" style="overflow: auto; width: auto; height: auto;">
                                                <div style="display: block; width: auto;">
                                                    <img src="<?=Croppa::url(asset($f->url), 600,600)?>" alt="imagen" class="noimg inner-fancy-img" width="200px" height="auto" style="width: 240px">
                                                    <h4 style="font-size: 19px;">
                                                        {{$f->name}}
                                                    </h4>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            Ubicación :</span>
                                                        {{$f->ubication}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        {{$f->lugar}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <i class="fa fa-male"></i> :
                                                        </span>
                                                        {{$f->t1}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <i class="fa fa-bicycle"></i> :
                                                        </span>
                                                        {{$f->t2}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <i class="fa fa-taxi"></i> :
                                                        </span>
                                                        {{$f->t3}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <i class="fa fa-bus"></i> :
                                                        </span>
                                                        {{$f->t4}}
                                                    </p>
                                                    <p class="descp" style="font-size: 15px;">
                                                        {{$f->descripcion}}
                                                        @if($f->article != "")
                                                        <a href="{{$f->article}}">Leer artículo</a>
                                                        @endif
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                        </div>
                    </td>
                </tr>
                @endforeach
                @if($fuera->count() == 0)
                <tr>
                    <td>
                    <span class="noRecords">
                    ¡No se han encontrado lugares en la ciudad!</span>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left; margin-left: 0px; margin-right: 0px; width: 100% !important; margin-top: 35px; margin-bottom: 35px !important;">
      <a class="sinsub" id="dirtravel4">{{$m->submenus[3]->title}}</a>
  </h3>
</div>
<div class="contentpersonal">
    <div class="level2" style="display: block;">
        <table class="gastroTbl" style="border-collapse:collapse;" cellspacing="0">
            <tbody>
                @php
                    $c=0;
                @endphp
                @foreach($gastronomia as $g)
                @php
                    $c++;
                @endphp
                @if($c ==1)
                   <tr>
                @endif
                    <td>
                        <div class="accordionSmMod sectionInfo" id="divDining">
                            <img src="<?=Croppa::url(asset($g->url), 188,133)?>" width="180">
                            <p class="sectionTitle" style="margin-top: 5px; font-family: lora !important;">
                                {{$g->name}}
                            </p>
                            <p>{{$g->descripcion}}</p>
                        </div>
                    </td>   
                @if($c ==3)
                   </tr>
                @endif
                @if($c ==3)
                    @php
                        $c=0;
                    @endphp
                @endif
                @endforeach

                <tr>
                    <td colspan="3">
                        <span style="display:none" class="noRecords">
                            ¡No se han encontrado restaurantes!</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="page_wrap" class="limit">
  <h3 class="tb andes" style="text-align: left; margin-left: 0px; margin-right: 0px; width: 100% !important; margin-top: 35px; margin-bottom: 35px !important;">
      <a class="sinsub" id="dirtravel5">{{$m->submenus[4]->title}}</a>
  </h3>
</div>
<div class="contentpersonal">
    <div class="level2" style="">
        <table style="border-collapse:collapse;" cellspacing="0">
            <tbody>
                @foreach($festividades as $f)
                <tr>
                    <td>
                        <div class="accordionFullMod sectionInfo clearfix">
                            <img src="<?=Croppa::url(asset($f->url), 600,600)?>" class="noimg" width="120" height="105">
                            <p class="sectionTitle" style="margin-bottom: 3px; font-family: lora !important;">
                                {{$f->name}}
                            </p>
                            <p class="h7" style="margin-bottom: 4px;">
                                @php
                                    setlocale(LC_TIME, 'es_PE');
                                    echo strftime("%d - %b ",strtotime($f->desde)) ." - ";
                                    echo strftime("%d - %b ",strtotime($f->hasta));
                                @endphp
                                
                            </p>
                            <p>
                            {{substr($f->descripcion, 0,194)." ..."}}
                            </p>
                            
                            <br>
                            <div class="viewResult"  style="padding-left: 130px;margin-top: -12px;">
                                <a class="vermascontent sinsub" title="{{$f->name}}" data-toggle="modal" data-target="#fiesta{{$f->id}}" style="cursor: pointer;font-style: italic; font-weight: bold;font-size: 13px">
                                    Ver más
                                </a>
                            </div>
                                
                            <!-- Modal -->

                            <div class="modal fade" id="fiesta{{$f->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header" style="border-bottom-width: 0px;">
                                   <span aria-hidden="true" data-dismiss="modal" class="pull-right" style="cursor: pointer;"><i class="fa fa-times"></i></span>
                                  </div>
                                  <div class="modal-body">
                                        
                                    <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
                                        <div class="fancybox-outer">
                                            <div class="fancybox-inner" style="overflow: auto; width: auto; height: auto;">
                                                <div style="display: block; width: auto;">
                                                    <img src="<?=Croppa::url(asset($f->url), 600,600)?>" alt="imagen" class="noimg inner-fancy-img" width="200px" height="auto" style="width: 240px">
                                                    <h4 style="font-size: 19px;">
                                                        {{$f->name}}
                                                    </h4>
                                                    <p style="font-size: 15px;">
                                                        {{$f->lugar}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <strong>Desde: </strong></span>
                                                        {{strftime("%d - %B ",strtotime($f->desde))}}
                                                    </p>
                                                    <p style="font-size: 15px;">
                                                        <span class="h7">
                                                            <strong>Hasta: </strong></span>
                                                        {{strftime("%d - %B ",strtotime($f->hasta))}}
                                                    </p>
                                                    <p class="descp" style="font-size: 15px;">
                                                        {{$f->descripcion}}
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td>
                        <span style="display:none" class="noRecords">
                            ¡No se han encontrado eventos!
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>



@endsection

@push('js')
<script type="text/javascript">
    var dir = {!! json_encode($dir) !!};
    if (dir ==19) {
        $('html,body').animate({
            scrollTop: $("#dirtravel2").offset().top
        }, 2000);
    }else if (dir ==20) {
        $('html,body').animate({
            scrollTop: $("#dirtravel3").offset().top
        }, 2000);
    }else if (dir ==21) {
        $('html,body').animate({
            scrollTop: $("#dirtravel4").offset().top
        }, 2000);
    }else if (dir ==22) {
        $('html,body').animate({
            scrollTop: $("#dirtravel5").offset().top
        }, 2000);
    }

    /*$('.vermascontent').click(function(event) {
         $('#exampleModal').modal('show');
    });*/
</script>
@endpush