<!-- pageheader
    ================================================== -->

        <header class="header">
            <div class="header__content row">
                <ul class="header__nav pull-right">
                    <li class="red-of leftred">
                            <a href="{{config('app.fb')}}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li class="red-of">
                            <a href="{{config('app.insta')}}"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </li>
                        <li class="red-of">
                            <a  href="#0" class="header__search-trigger"></a>
                        </li>
                </ul>
                <div class="header__logo" style="margin-top: 0px;padding-top: 0px;">
                    <a class="logo" href="/">
                        <img src="<?=Croppa::url(asset(config('app.logoimg')), 600,137)?>" alt="">
        
                    </a>
                    
                </div> <!-- end header__logo -->

                <div class="header__search">

                    {!! Form::open(['method'=>'GET', 'route'=>'search', 'class'=>'header__search-form' ]) !!}
                        <label>
                            <span class="hide-content">Buscar:</span>
                            <input type="search" class="search-field" placeholder="Type Keywords" value="" name="s" title="Search for:" autocomplete="off">
                        </label>
                        <input type="submit" class="search-submit" value="Search">
                    {!!Form::close() !!}
        
                    <a href="#0" title="Close Search" class="header__overlay-close">Close</a>

                </div>  <!-- end header__search -->
                <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>
                <nav class="header__nav-wrap"><!--id="navbar"-->
                    <h1 class="header__nav-heading h6"></h1>

                    <ul class="header__nav menus" style="background-color: #fff; padding-left: 0px; padding-right: 0px; margin-bottom: 0px; transition: margin 700ms ease 0s;">
                        @foreach($menu as $m)
                            <li class="{{ ($m->url == $_SERVER["REQUEST_URI"])?'current':(($m->submenus->count()>0)?'':'') }}"><!--has-children-->
                                <a class="menuletter mayusc sinsub menupr" href="{{($m->submenus->count()>0)?URL::action('ArticleController@verxcategory', $m->id):URL::action('ArticleController@verxcategorydetail', $m->id) }}" title="" style="font-size: 14px;border-bottom: 1px solid #91d8f6;">
                                    {{$m->title}}
                                </a>
                                @if($m->submenus->count() >0 )
                                <ul class="sub-menu">
                                    @foreach($m->submenus as $sm)
                                    <li><a class="menuletter sinsub menupr" href="{{URL::action('ArticleController@verxcategorydetail', $sm->id) }}">{{$sm->title}}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                        @endforeach
                        
                    </ul> <!-- end header__nav -->
                    
                        
                    <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

                </nav> <!-- end header__nav-wrap -->

            </div> <!-- header-content -->
        </header> <!-- header -->
