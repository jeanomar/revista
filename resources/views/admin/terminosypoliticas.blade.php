@extends('admin.layouts.plantilla')
@push('css')
    
@endpush
@section('content')
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Términos de uso</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        {!! Form::open(['method'=>'POST', 'route'=>'terminosypoliticas.update']) !!}
                            <div class="panel-heading" style="height: 55px;">
                                <button id="edit1" type="button" class="btn btn-default pull-right" >Editar <i class="fa fa-edit"></i></button>
                                <button id="save1" type="submit" class="btn btn-primary pull-right" hidden="true">Guardar <i class="fa fa-save"></i></button>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">

                                        <input type="hidden" value="{{$terminos->id}}" name="id">
                                        <div class="form-group">
                                            <label>Título</label>
                                            <input id="title1" name="title" class="form-control" placeholder="Escriba un título" value="{{$terminos->title}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea id="descripcion1" name="descripcion" class="form-control" rows="20" disabled>{{$terminos->descripcion}}</textarea>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                        {!!Form::close() !!}
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@push('js')
<script>
    $("#title1").prop('disabled', true);
    $("#subtitle1").prop('disabled', true);
    $("#descripcion1").prop('disabled', true);
    $("#save1").hide();
    $("#edit1").click( function() {
        $("#save1").show();
        $("#edit1").hide();
        $("#title1").prop('disabled', false);
        $("#descripcion1").prop('disabled', false);
    });

    $("#title2").prop('disabled', true);
    $("#subtitle2").prop('disabled', true);
    $("#descripcion2").prop('disabled', true);
    $("#save2").hide();
    $("#edit2").click( function() {
        $("#save2").show();
        $("#edit2").hide();
        $("#title2").prop('disabled', false);
        $("#descripcion2").prop('disabled', false);
    });
</script>
@endpush