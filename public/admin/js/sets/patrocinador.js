/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */

var urlimg;

if ($('.file-upload-image').attr('src') != "#") {
      $('.image-upload-wrap').hide();

      $('.file-upload-content').show();

}
 
    function readURL(input) {
        if (input.files[0].size/1024 > 2000) {
            removeUpload();
            alert("Solo imagenes menores a 2 Mb.");
            $('.image-title').html('Error');
        }
        else if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
              $('.image-upload-wrap').hide();

              $('.file-upload-image').attr('src', e.target.result);
              $('.file-upload-content').show();

              $('.image-title').html(input.files[0].name);
            };
            urlimg =input.files[0];
            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }


function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
  urlimg="";
}
$('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
});

$(function(){
    
    $('input[name=tlf1]').inputmask({mask: "999999999"});
    $('input[name=tlf2]').inputmask({mask: "(999)999999"});
    $('input[name=email]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
          pastedValue = pastedValue.toLowerCase();
          return pastedValue.replace("mailto:", "");
        },
        definitions: {
          '*': {
            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
            casing: "lower"
          }
        }
    });

    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $("#articleform")[0].reset();
        $('.modal-title').text("Nuevo patrocinador");
        $('#modal').modal({keyboard: false, backdrop: 'static'});  
    });

    //EDIT
    $('#dataTable tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var web = $(this).data('web');
        var descripcion = $(this).data('descripcion');
        var state = ($(this).data('state') == "")?true:false;
        var address  = $(this).data('address');
        var tlf1  = $(this).data('tlf1');
        var tlf2  = $(this).data('tlf2');
        var email  = $(this).data('email');
        var ubication  = $(this).data('ubication');

        $('.modal-title').text("Editar patrocinador");
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=name]").val(name);
        $("#modal").find("input[name=web]").val(web);
        $("#modal").find("textarea[name=descripcion]").val(descripcion);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modal").find("input[name=address]").val(address);
        $("#modal").find("input[name=tlf1]").val(tlf1);
        $("#modal").find("input[name=tlf2]").val(tlf2);
        $("#modal").find("input[name=email]").val(email);
        $("#modal").find("input[name=ubication]").val(ubication);
        
        $("#modal").on('hidden.bs.modal', function(e){
            $("#modal").find("input[name=id]").remove();
        });
    });

    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'auspiciadores/img/upload'
    });

    var tx=0;
    //photos
    $('#dataTable tbody').on('click', 'tr td button#btn-img', function(e) {
        e.preventDefault();
        $('#btnSimpan').removeAttr('disabled','disabled');
        tx=0;
        var id = $(this).data('id');
        var title = $(this).data('title');
        var subtitle = $(this).data('subtitle');
        var descripcion = $(this).data('descripcion');
        var state = $(this).data('state');
        var category_id  = $(this).data('category_id');

        $('.modal-title').text("Imágenes");
        $('#modal_img').modal({keyboard: false, backdrop: 'static'});
        $("input[name=id]").remove();
        $("#modal_img").find("#fileupload").append('<input type="hidden" name="id">');

        $("#modal_img").find("input[name=id]").val(id);
        $("#modal_img").find("label[name=title]").text(title);

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            data: {
                    'id': id
                },
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
            e.preventDefault();
        });

        $("#modal_img").on('hidden.bs.modal', function(e){
            $("#tb_imgs").find("tbody").empty();
            tx=0;
        });

        $("#btnSimpan").on('click', function(e){
            tx++;
            if (tx == 1) {
                $(this).attr("disabled", "disabled");
                var token = $("input[name=_token]").val();
                $.ajax({
                    url: window.location.href+'/img/principal',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'post',
                    datatype: 'json',
                    data: {id: $('input[name=typeimg]:checked').val()},
                    success:function(response){
                        $('#modal_img').modal('toggle');
                        x=0;
                    },
                    error:function(msj){

                    }
                }); 
            }
             
        });

    });

    function actualizaPub(response){
        var det="";
        for (var i = response.length - 1; i >= 0; i--) {
            if (response[i].deleted_at ) {
                det += '<tr style="color: red;">';
            }else{
                det += '<tr>';
            }
           
            det += '<td scope="row">'+response[i].id+'</td> ';
            det += '<td style="max-width: 100px">';
            det += ' <p class="resumen" >'+response[i].title+'</p>';
            det += '</td> ';
            det += '<td>';
            det +=  ' <img src="'+response[i].url+'" style="max-width: 80px;height: auto;">';
            det += '</td>' ;
            det += '<td class="center">'+response[i].action;
            det += '</td> ';
            det += ' </tr>';
        }
        $('#detPublicidad').html(det);
    }

    //TAMBAH
    $("#btn-tambahp").on("click", function(e){
        e.preventDefault();
        $("#articleformp")[0].reset();
        $('input[name=id]').val("");
        removeUpload();
    });

    $("#btnSaveP").on('click', function(e){
            if (!$('input[name=title]').val()) {
               $('input[name=title]').focus();
            }else if ($('input:radio[name=statei]:checked').val() == 0 && !$('input[name=articleid]').val()) {
                $('input[name=articleid]').focus();
            }else if ($('input:radio[name=statei]:checked').val() == 1 && !$('input[name=position]').val()) {
                $('input[name=position]').focus();
            }else{
                var token = $("input[name=_token]").val();
                var formData = new FormData();
                formData.append('id',  $('input[name=id]').val());
                formData.append('title', $('input[name=title]').val());
                formData.append('article_id', $('input[name=articleid]').val());
                formData.append('position', $('input[name=position]').val());
                formData.append('patrocinador_id', $('input[name=patrocinadorid]').val());
                formData.append('state', $('input:checkbox[name=statep]:checked').val());
                formData.append('statea', $('select[name=statea]').val());
                formData.append('statei', $('input:radio[name=statei]:checked').val());
                formData.append('url', urlimg);

                $.ajax({

                    url: '/publicity',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'post',
                    datatype: 'json',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success:function(response){
                        removeUpload();
                        $("#articleformp")[0].reset();
                        document.getElementById('div2').style.display = 'block';
                        document.getElementById('div1').style.display ='none';
                        actualizaPub(response);
                    },
                    error:function(msj){

                    }
                }); 
            }
             
        });

    //Publicidad
    $('#dataTable tbody').on('click', 'tr td button#btn-pub', function(e) {
        e.preventDefault();
        $("#collapseOne").addClass('in');
        $("#collapseTwo").removeClass('in');
        $('.modal-title').text("Publicidad");
        var title = $(this).data('title');
        var id = $(this).data('id');
        $("#modalpub").find("input[name=patrocinador]").val(title);
        $("#modalpub").find("input[name=patrocinadoridp]").val(id);
        $("#modalpub").find("input[name=patrocinadorid]").val(id);

        var token = $("input[name=_token]").val();
        $.ajax({
            url: 'publicity/show',
            headers: {'X-CSRF-TOKEN': token},
            type: 'get',
            datatype: 'json',
            data: {id: id},
            success:function(response){
                actualizaPub(response);
            },
            error:function(msj){

            }
        });
    });

    //EDIT
    $('#dataTable2 tbody').on('click', 'tr td button#btn-editp', function(e) {
        e.preventDefault();
        
        var id = $(this).data('id');
        var statea = $(this).data('statea');
        var title = $(this).data('title');
        var state = ($(this).data('state') == 1)?true:false;
        var url  = $(this).data('url');
        var position  = $(this).data('position');
        var articleid  = $(this).data('articleid');
        var patrocinadorid  = $(this).data('patrocinadorid');
        var statei  = $(this).data('statei');

        $("#modalpub").find("input[name=id]").val(id);
        $("#modalpub").find("select[name=statea]").val(statea);
        $("#modalpub").find("input[name=title]").val(title);
        $("#modalpub").find('input:checkbox[name="state"]').prop("checked",state);
        $("#modalpub").find("img[name=img_datos]").attr('src', url);
        $("#modalpub").find("input[name=position]").val(position);
        $("#modalpub").find("input[name=articleid]").val(articleid);
        $("#modalpub").find("input[name=patrocinadorid]").val(patrocinadorid);
        $("#modalpub").find("input:radio[id=statei"+statei+"]").prop("checked",true);
        if (statei==0) {
            show1();
        }else{
            show2();
        }
        if ($('.file-upload-image').attr('src') != "#") {
              $('.image-upload-wrap').hide();

              $('.file-upload-content').show();

        }
         
    });

     //EDIT
    $('#dataTable2 tbody').on('click', 'tr td button#btn-deletp', function(e) {
        e.preventDefault();
        
        var id = $(this).data('id');
        var state = ($(this).data('state') == 1)?true:false;
        var token = $("input[name=_token]").val();
        $.ajax({
            url: 'publicity/publicidadstate/'+id,
            headers: {'X-CSRF-TOKEN': token},
            type: 'delete',
            datatype: 'json',
            
            success:function(response){
                actualizaPub(response);
            },
            error:function(msj){

            }
        });
    });
       
    $('input[name=position]').inputmask('Regex', {regex: "[0-9]{10}"});
    $('input[name=articleid]').inputmask('Regex', {regex: "[0-9]{10}"});


});
