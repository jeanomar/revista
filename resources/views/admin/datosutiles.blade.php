@extends('admin.layouts.plantilla')
@push('css')
<link href="{{ asset('admin/dist/css/imgupload.css') }}" rel="stylesheet" />
@endpush
@section('content')
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Datos Útiles</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-10">
                    <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-info">
                                            {!! Form::open(['method'=>'POST', 'route'=>'clima.update']) !!}
                                            <div class="panel-heading">
                                                
                                                    <h4>Clima</h4>
                                                
                                                    <button id="edit1" type="button" class="btn btn-default pull-right" style="margin-top: -35px;">
                                                        Editar <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button id="save1" type="submit" class="btn btn-primary pull-right" hidden="true" style="margin-top: -35px;">
                                                        Guardar <i class="fa fa-save"></i>
                                                    </button>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label>Tipo</label>
                                                    <input id="title" name="title" class="form-control d1" placeholder="Escriba un título" value="{{$datos[0]->name}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Descripción</label>
                                                    <textarea id="descripcion" name="descripcion" class="form-control d1" rows="3" disabled>{{$datos[0]->valor1}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-12">{{$datos[1]->name}}</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Valor 1</strong></span>
                                                            <input id="t1" name="t1" class="form-control d1" placeholder="Escriba un título" value="{{$datos[1]->valor1}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Valor 2</strong></span>
                                                            <input id="t2" name="t2" class="form-control d1" placeholder="Escriba un título" value="{{$datos[1]->valor2}}" disabled>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-12">{{$datos[2]->name}}</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Valor 1</strong></span>
                                                            <input id="t3" name="t3" class="form-control d1" placeholder="Escriba un título" value="{{$datos[2]->valor1}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Valor 2</strong></span>
                                                            <input id="t4" name="t4" class="form-control d1" placeholder="Escriba un título" value="{{$datos[2]->valor2}}" disabled>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            {!!Form::close() !!}
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h4>Vias de Acceso</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label>{{$datos[3]->name}}</label>
                                                    {!! Form::open(['method'=>'POST', 'route'=>'accesoaereo.update']) !!}
                                                    <div class="form-group input-group">
                                                        <input name="name" class="form-control d2" placeholder="Escriba una descripción" value="{{$datos[3]->valor1}}" disabled>
                                                        <span class="input-group-btn">
                                                            <button id="edit2" type="button" class="btn btn-default pull-right"">
                                                                Editar <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button id="save2" type="submit" class="btn btn-primary pull-right" hidden="true">
                                                                Guardar <i class="fa fa-save"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    {!!Form::close() !!}
                                                </div>
                                                <div class="form-group">
                                                    <label>{{$datos[4]->name}}</label>

                                                </div>
                                                @foreach($datos->where('father_id',5) as $t)
                                                <div class="form-group" style="height: 32px;">
                                                    <div class="col-md-5 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Lugar</strong></span>
                                                            <input class="form-control" value="{{$t->name}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Km</strong></span>
                                                            <input class="form-control" placeholder="Escriba un título" value="{{$t->valor1}}" disabled>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Horas</strong></span>
                                                            <input class="form-control" placeholder="Escriba un título" value="{{$t->valor2}}" disabled>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-1 col-xs-12">
                                                        {{ Form::open(['route' => ['deletedato', $t->id], 'method' => 'delete']) }}
                                                        <button type="submit" class="btn btn-default pull-right"">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                                @endforeach
                                                {!! Form::open(['method'=>'POST', 'route'=>'accesoter']) !!}
                                                <div class="form-group">
                                                    <div class="col-md-5 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Lugar</strong></span>
                                                            <input id="lugar" name="lugar" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Km</strong></span>
                                                            <input id="km" name="km" class="form-control" >
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><strong>Horas</strong></span>
                                                            <input id="tiempo" name="tiempo" class="form-control" >
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-1 col-xs-12">
                                                        <button type="submit" class="btn btn-default pull-right"">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                {!!Form::close() !!}
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            {!! Form::open(['method'=>'POST', 'route'=>'imgdatos', 'enctype'=>"multipart/form-data", 'files' => true]) !!}
                                            <div class="panel-heading">
                                                <h4>Foto</h4>
                                                <button id="edit3" type="button" class="btn btn-default pull-right" style="margin-top: -35px;">
                                                    Editar <i class="fa fa-edit"></i>
                                                </button>
                                                <button id="save3" type="submit" class="btn btn-primary pull-right" hidden="true" style="margin-top: -35px;">
                                                    Guardar <i class="fa fa-save"></i>
                                                </button>
                                            </div>
                                            <div class="panel-body">
                                                <div class="file-upload" style="width: 100%;">
                                                  <button class="file-upload-btn d3" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Agregar imagen</button>

                                                  <div class="image-upload-wrap">
                                                    <input name="imgdatos" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                                    <div class="drag-text">
                                                      <h3>Arrastra y suelta un archivo o selecciona Agregar imagen</h3>
                                                    </div>
                                                  </div>
                                                  <div class="file-upload-content">
                                                    <img class="file-upload-image" src="{{$datos->find(13)->valor1}}" alt="your image" style="width: 100%;"/>
                                                    <div class="image-title-wrap">
                                                      <button type="button" onclick="removeUpload()" class="remove-image d3">Quitar <span class="image-title">imagen</span></button>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                            {!!Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                        
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@push('js')
<script>
    $(".d1").prop('disabled', true);
    $(".d2").prop('disabled', true);
    $(".d3").hide();

    $("#save1").hide();
    $("#edit1").click( function() {
        $("#save1").show();
        $("#edit1").hide();
        $(".d1").prop('disabled', false);
    });

    $("#save2").hide();
    $("#edit2").click( function() {
        $("#save2").show();
        $("#edit2").hide();
        $(".d2").prop('disabled', false);
    });
    $("#subtitle").prop('disabled', false);

    $("#save3").hide();
    $("#edit3").click( function() {
        $("#save3").show();
        $("#edit3").hide();
        $(".d3").show();
    });

    /************************/

if ($('.file-upload-image').attr('src') != "") {
      $('.image-upload-wrap').hide();

      $('.file-upload-content').show();

}

function readURL(input) {
    if (input.files[0].size/1024 > 2000) {
        removeUpload();
        alert("Solo imagenes menores a 2 Mb.");
        $('.image-title').html('Error');
    }
    else if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image-upload-wrap').hide();

          $('.file-upload-image').attr('src', e.target.result);
          $('.file-upload-content').show();

          $('.image-title').html(input.files[0].name);
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload();
    }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
});


</script>
@endpush