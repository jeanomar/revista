@extends('admin.layouts.plantilla')
@push('css')
    
@endpush
@section('content')
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">La Revista</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        {!! Form::open(['method'=>'POST', 'route'=>'larevista.update']) !!}
                            <div class="panel-heading" style="height: 55px;">
                                <button id="edit" type="button" class="btn btn-default pull-right" >Editar <i class="fa fa-edit"></i></button>
                                <button id="save" type="submit" class="btn btn-primary pull-right" hidden="true">Guardar <i class="fa fa-save"></i></button>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        
                                        <div class="form-group">
                                            <label>Título</label>
                                            <input id="title" name="title" class="form-control" placeholder="Escriba un título" value="{{$articulo->title}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea id="descripcion" name="descripcion" class="form-control" rows="20" disabled>{{$articulo->descripcion}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Agradecimientos:</label>
                                            <textarea id="subtitle" name="subtitle" class="form-control" rows="3" disabled>{{$articulo->subtitle}}</textarea>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                        {!!Form::close() !!}
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@push('js')
<script>
    $("#title").prop('disabled', true);
    $("#subtitle").prop('disabled', true);
    $("#descripcion").prop('disabled', true);
    $("#save").hide();
    $("#edit").click( function() {
        $("#save").show();
        $("#edit").hide();
        $("#title").prop('disabled', false);
        $("#subtitle").prop('disabled', false);
        $("#descripcion").prop('disabled', false);
    });
</script>
@endpush