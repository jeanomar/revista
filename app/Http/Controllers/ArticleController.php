<?php

namespace App\Http\Controllers;
use Omar\Entities\Menu;
use Omar\Entities\Article;
use Omar\Entities\Image;
use Omar\Entities\Data;
use Omar\Entities\Attractive;
use Omar\Entities\Festivitie;
use Omar\Entities\Gastronomy;
use Omar\Entities\Publicity;
use Croppa;
use File;
use FileUpload;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public $folder = '/uploads/'; // add slashes for better url handling
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listado(){
        $articulos =  Article::where('id', "!=",21)->where('id', "!=",22)->where('id', "!=",23)->orderBY('id','DESC')->get();
        $subcategorias =  Menu::where('menu_id', "!=",null)->where('menu_id', "!=",18)->where('menu_id', "!=",7)->orWhere('id',5)->orderBY('title','ASC')->get();
        return view('admin.article',compact('articulos', 'subcategorias'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function larevista(){
        $articulo =  Article::find(21);
        return view('admin.larevista', compact('articulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function larevistaupdate(Request $request)
    {
        $title = ($request->get('title'))?$request->get('title'):'';
        $subtitle = ($request->get('subtitle'))?$request->get('subtitle'):'';
        $descripcion = ($request->get('descripcion'))?$request->get('descripcion'):'';

        $articulo =  Article::find(21);
        $articulo->title = $title;
        $articulo->subtitle = $subtitle;
        $articulo->descripcion = $descripcion;
        $articulo->save();
        return redirect()->route('larevista.editar');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editimg(Request $request)
    {
        $img =  Image::find($request->get('id'));
        $img->title = $request->get('name');
        $img->save();
        return 1;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getimg(Request $request)
    {
        // get all pictures
        $pictures = Image::where('article_id',($request->get('id')?$request->get('id'):0))->get();
        
        // add properties to pictures
        $pictures->map(function ($picture) {
            $picture['size'] = File::size(public_path($picture['url']));
            $picture['thumbnailUrl'] = Croppa::url($picture['url'], 80, 80, ['resize']);
            $picture['deleteType'] = 'DELETE';
            $picture['name'] = $picture['title'];
            $picture['type'] = $picture['type'];
            $picture['value'] = $picture->id;
            $picture['deleteUrl'] = route('pictures.destroy', $picture->id);
            return $picture;
        });
        
        // show all pictures
        return response()->json(['files' => $pictures]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function principal(Request $request)
    {
        $picture = Image::find($request->get('id'));
        $pictures = Image::where('article_id',$picture->article_id)->update(['type' => 's']);
        $picture->type = 'p';
        $picture->save();
        return $picture->id;

    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imgUpload(Request $request)
    {
        // create upload path if it does not exist
        $path = public_path($this->folder.$request->get('id')."/");
        if(!File::exists($path)) {
            File::makeDirectory($path);
        };
        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple($path);
        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();
        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['files'], $_SERVER);
        $slugGenerator = new FileUpload\FileNameGenerator\Slug();
        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);
        $fileupload->setFileNameGenerator($slugGenerator);
        
        // Doing the deed
        list($files, $headers) = $fileupload->processAll();
        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }
        foreach($files as $file){
            //Remember to check if the upload was completed
            if ($file->completed) {
                // set some data
                $filename = $file->getFilename();
                $url = $this->folder.$request->get('id')."/". $filename;
                
                if ($request->get('typeimg') == 'true') {
                    $affectedRows = Image::where('article_id', $request->get('id'))->update(['type' => 's']);
                }

                // save data
                $picture = Image::create([
                    'name' => $filename,
                    'url' => $url,
                    'title' => $request->get('description'),
                    'article_id' => $request->get('id'),
                    'creditos' => "",
                    'type' => ($request->get('typeimg') == 'true')?'p':'s'
                ]);
                
                // prepare response
                $data[] = [
                    'size' => $file->size,
                    'name' => $request->get('description'),
                    'type' => ($request->get('typeimg')== 'true')?'p':'s',
                    'url' => $url,
                    'thumbnailUrl' => Croppa::url($url, 80, 80, ['resize']),
                    'deleteType' => 'DELETE',
                    'deleteUrl' => route('pictures.destroy', $picture->id)
                ];
                
                // output uploaded file response
                return response()->json(['files' => $data]);
            }
        }
        // errors, no uploaded file
        return response()->json(['files' => $files]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $picture)
    {
        Croppa::delete($picture->url); // delete file and thumbnail(s)
        $picture->delete(); // delete db record
        return response()->json([$picture->url]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ver($id)
    {
    	$articulo =  Article::find($id);
        if ($articulo->state =='0' or $id == 21 or $id == 22 or $id == 23) {
           return 'No disponible';
        }
    	$relacionados = Article::where('category_id',$articulo->category_id)->where('id', "!=",$articulo->id)->orderBY('id','DESC')->take(6)->get();
    	$actualidad = Article::where('state',1)->where('id', "!=",$articulo->id)->where('id', "!=",21)->where('id', "!=",22)->where('id', "!=",23)->orderBY('id','DESC')->take(4)->get();
        $menu = Menu::whereNull('menu_id')->where('access','G')->get();
    	$dir = ($articulo->category->id == 5)?$articulo->category->title:$articulo->category->padre->title."/".$articulo->category->title;
        $publicidadi = Publicity::where('article_id',$id)->where('state_ini','0')->where('state',"!=",1)->orderBY('id','DESC')->take(1)->get();
        $publicidadd = Publicity::where('article_id',$id)->where('state_ini','0')->where('state',1)->orderBY('id','DESC')->take(1)->get();
        $data = [
            'dir'  => $dir,
            'articulo' => $articulo,
            'relacionados' => $relacionados,
            'actualidad' => $actualidad,
            'publicidadi' => $publicidadi,
            'publicidadd' => $publicidadd
        ];
        return view('article', compact('menu'))->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verxcategory($id)
    {
    	$m = Menu::find($id);
        if(!$m){return "No existe esa categoría";}
        $menu = Menu::whereNull('menu_id')->get();
        ($id == 1)?$dir ="Recomendados":$dir = $m->title;
        if($id == 1){
            $sub = Menu::whereIn('id',(Article::where('state',1)->where('id','!=',21)->where('id','!=',22)->where('id','!=',23)->orderBY('id','DESC')->take(5)->get()->pluck('category_id')))->get();
        }elseif($id == 7){
            $datas = Data::all();
            $dentro = Attractive::where('type','dentro')->get();
            $fuera = Attractive::where('type','fuera')->get();
            $gastronomia = Gastronomy::all();
            $festividades = Festivitie::all();
            $data = [
                'dir'  => $dir,
                'm' => $m,
                'data' => $datas,
                'dentro' => $dentro,
                'fuera' => $fuera,
                'gastronomia' => $gastronomia,
                'festividades' => $festividades
            ];
            return view('travel', compact('menu'))->with($data);
        }
        else{
            $sub = Menu::where('menu_id',$id)->get();
        } 
        $data = [
            'dir'  => $dir,
            'desc' => $m->description,
            'sub' => $sub
        ];
        return view('category', compact('menu'))->with($data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verxcategorydetail($id)
    {
    	$m = Menu::find($id);
        if(!$m){return "No existe esa subcategoría";}
    	$articulos =  Article::where('category_id',$id)->orderBY('id','DESC')->get();

        $menu = Menu::whereNull('menu_id')->get();
    	$dir = ($m->padre)?$m->padre->title."/".$m->title:$m->title;
        $data = [
            'dir'  => $dir,
            'articulos' => $articulos
        ];
        if ($id == 6) {
           return redirect()->route('patrocinadores');
        }elseif ($id == 1) {
           return redirect()->route('larevista');
        }
        if($m->menu_id == 7){
            $dir = $id;
            $m = Menu::find(7);
            $datas = Data::all();
            $dentro = Attractive::where('type','dentro')->get();
            $fuera = Attractive::where('type','fuera')->get();
            $gastronomia = Gastronomy::all();
            $festividades = Festivitie::all();
            $data = [
                'dir'  => $dir,
                'm' => $m,
                'data' => $datas,
                'dentro' => $dentro,
                'fuera' => $fuera,
                'gastronomia' => $gastronomia,
                'festividades' => $festividades
            ];
            return view('travel', compact('menu'))->with($data);
        }
        return view('category-detail', compact('menu'))->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->get('principal')) {
            $affectedRows = Article::where('principal', '1')->update(['principal' => '0']);
        }
        
        if ($request->get('id')) {
            $articulo =Article::find($request->get('id'));
        }else{
            $articulo = new Article;
        } 
        $articulo->title = $request->get('title');
        $articulo->subtitle = $request->get('subtitle');
        $articulo->descripcion = $request->get('descripcion');
        $articulo->category_id = $request->get('subcat');
        $articulo->state = ($request->get('state'))?'1':'0';
        $articulo->principal = ($request->get('principal'))?'1':'0';
        $articulo->save();

        return back();
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function state($id, Request $request)
    {
        $articulo = Article::find($id);
        $articulo->state = ($request->get('art_id')==0)?'1':'0';
        $articulo->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    
}
