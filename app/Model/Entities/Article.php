<?php

namespace Omar\Entities;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'article';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'subtitle',  'descripcion', 'category_id', 'user_id', 'state','principal'];
    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];


    public function category()
    {
        return $this->belongsTo(Menu::class, 'category_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'article_id');
    }

    public function publicies()
    {
        return $this->hasMany(Image::class, 'article_id');
    }
}
