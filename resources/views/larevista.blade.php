@extends('layouts.plantilla')

@section('title', 'CAJAMARCA donde todo empezó')
@push('css')
    <style>
        .col-full{

        }
        
          .row.masonry-wrap{
                margin-left: 5%;
                margin-right: 5%;
                width: 90%;
          }  

        @media only screen and (max-width:1200px) {
            .row.top{
                padding-left: 35px;padding-right: 35px;
            }
            .descriptc{
                padding-right: 25px !important;
                padding-left: 0px;
            }
            .news-text{
                padding-right: 0px;
                padding-left: 0px;
            }

             .mod-mxm-news{
                height: 1600px !important;
            }
            .w-mxm-items{
                height: 1600px !important;
            }
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                max-width: 1200px;

            }

        }
        @media only screen and (max-width: 900px) {
            .s-content.content{

            }

        }
        @media only screen and (max-width: 800px) {
            .header__content {
                margin-left: 0px;
                margin-right: 0px;
                width: 100%;
            }
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                margin-left: 5% !important;
                margin-right: 5% !important;
                max-width: 800px;
                width: 90%;
            }
            .masonry-wrap {
                max-width: 800px;
            }

            .pageheader-content.row{
                padding-left: 0px;
                padding-right: 0px;
                margin-left: 0px;   
                margin-right: 0px;
                max-width: 800px;
                width: 100%;
            }
            
            .entry.format-standard{
                padding-left: 0px;
            }

            .row .row {
                margin-left: 0px;
                margin-right: 0px;
                padding-left: 0;
                padding-right: 0;
                width: 100%;
                max-width: 800px;
            }
            #relaciona {
                margin-left: 0px !important;
                margin-right: 0px !important;
                width: 100% !important;
            }
            .reli{
                margin-left: 0px !important;
                margin-right: 0px !important;
                width: 100% !important;
            }
            .entry__text{
                padding-left: 0px !important;
            }
            .mod-mxm-news{
                height: 780px !important;
            }
            .w-mxm-items{
                height: 780px !important;
            }
            .mod-mxm-news{
                height: 1500px !important;
            }
            .w-mxm-items{
                height: 1500px !important;
            }
        }
        @media only screen and (max-width:767px) {
            .descriptc{
                padding-right: 0px !important;
                padding-left: 0px !important; 
            }
            .news-text{
                padding-right: 0px !important;
                padding-left: 0px !important;
            }
            .mod-mxm-news{
                height: 780px !important;
            }
            .w-mxm-items{
                height: 780px !important;
            }
        }

        @media only screen and (max-width:600px) {

            .mod-mxm-news{
                height: 800px !important;
            }
            .w-mxm-items{
                height: 800px !important;
            }

        }
        @media only screen and (max-width:500px) {

            .masonry-wrap {
                max-width: 500px;
            }

            .mod-mxm-news{
                height: 850px !important;
            }
            .w-mxm-items{
                height: 850px !important;
            }

        }
        @media only screen and (max-width: 400px) {
            
            .col-full{
                padding-left: 0 !important;
                padding-right:0 !important;
                margin-left: 5% !important;
                margin-right: 5% !important;
                max-width: 800px;
                width: 90% !important;
            }

            .str2{
                margin-top: -5px !important;
            }

            .entry__excerpt{
                padding-left: 0px;
                padding-right: 0px;
                margin-left: 5%;
                margin-right: 5%;
            }
            .publi{
                margin-left: 5%;
                margin-right: 5%;
            }
            .relacionados{
                margin-top: -40px;
            }
            .reli{
                margin-left: 40px;
                margin-right: 40px;
            }
            .row.masonry-wrap{
                margin-left: 0px;
                margin-right: 0px;
            }
            .entry__header{
                margin-left: 5%;
                margin-right: 5%;
                width: 90% !important;
                max-width: 400px;
            }
            #relaciona {
                float: left !important;
            }
            #h3tb{
                margin-top: 45px;
            }
            #desclarevista{
                font-size: 16px !important;
            }
            .capitalLetter {
                font-size: 60px;
                line-height: 47px;
                height: 48px;
            }
        }
       
        
    </style>
    <style>
      .s-pageheader--home{
        min-height: 100px;
        height: 120px;
        padding-top: 0px;
      }
      #page_wrap {max-width: 1160px;}
    </style>
@endpush

@section('menu')
    @include("fixed.menu2")
@endsection

@section('banner')

    
@endsection

@section('content')
<div id="page_wrap" class="limit">
    <h3 class="tb andes" style="text-align: left;">
        <a class="sinsub">{{$dir}}</a>
    </h3>
</div>
    <!-- s-content
    ================================================== -->
    <section class="s-content content" style="padding-top: 5px;padding-left: 0px;">
        
        <div id="textorev" class="row masonry-wrap" style="max-width: 1200px;margin-left: auto;margin-right: auto;">
            <div class="masonry article-content"> 
    
                    <div id="ddse" class="entry__text" style="padding-left: 0px;padding-top: 0px;padding-bottom: 0px;">
                        <div class="entry__header">
                            <h3 class="entry__title str2" style="color:#000000b3;margin-bottom: 15px;"></h3>
                            
                        </div>
                        <article class="entry format-standard col-sm-9 descriptc" data-aos="fade-up" style="padding-right: 0px; margin-bottom: 0px">
                    
                        <div class="entry__excerpt">
                            <p id="desclarevista" style="font-family: 'calibri';font-weight: 400;color: #000000b3;font-size: 18px;margin-bottom: 35px;text-align: left;letter-spacing: 0.07em;margin-top: -15px !important;">
                                
                                @php 
                                    echo (nl2br($articulo->descripcion));
                                @endphp
                            <br>
                            @php 
                                echo (nl2br($articulo->subtitle));
                            @endphp
                            </p>
                        </div>
                        <div class="entry__meta" style="display: none;">
                            <span class="entry__meta-links">
                                <a href=""></a>
                            </span>
                        </div>
                    </div>

                
                </article> <!-- end article -->

                


            </div> <!-- end masonry -->
        </div> <!-- end masonry-wrap -->

    </section> <!-- s-content -->
    
    <div id="page_wrap2" class="limit">
    <div id="zona_9">
        <div id="wrap_inst">
            <h3 class="tb" style="margin-bottom: 10px !important;">
                <a href="" class="sinsub">Ediciones impresas</a>
            </h3>
            
        </div>
    </div>
</div>
<div id="zona_5">
        <div id="page_wrap" class="limit">
            <div id="wrap_v" class="v2">
                <article class="video_otro">
                    <div id="wrap_art">
                         <iframe src="https://e.issuu.com/anonymous-embed.html?u=joseeduardodiazcamacho&d=cajamarca_donde_todo_empezo_iia" width="100%" height="500" frameborder="0" allowfullscreen="true"></iframe>
                    </div>
                </article>
                <article class="video_otro">
                    <div id="wrap_art">
                        <iframe src="https://e.issuu.com/anonymous-embed.html?u=revistaholder&d=cajamarca_donde_todo_empezo__opt" width="100%" height="500" frameborder="0" allowfullscreen="true"></iframe>
                    </div>
                </article>
            </div>
        </div>    
    </div>
</div>
@endsection

@push('js')

    <script>
         // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
        
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();        
        
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

      $(".tab_drawer_heading").removeClass("d_active");
      $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
      
    });
    /* if in drawer mode */
    $(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
      
      $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
      
      $("ul.tabs li").removeClass("active");
      $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
    console.log($('#desclarevista').height());
    $('#textorev').css({'min-height': $('#desclarevista').height()+50});
    $('#ddse').css({'min-height': $('#desclarevista').height()+50});
    console.log($('#textorev').height());
    /* Extra class "tab_last" 
       to add border to right side
       of last tab */
    $('ul.tabs li').last().addClass("tab_last");
    
    </script>
@endpush