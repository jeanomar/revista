<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Omar\Entities\Patrocinador;
use Omar\Entities\ImgPatrocinador;
use Omar\Entities\Menu;
use Croppa;
use File;
use FileUpload;

class AuspiciadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrocinadores =  Patrocinador::withTrashed()->get();
        return view('admin.patrocinadores',compact('patrocinadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->get('id')) {
            $articulo =Patrocinador::find($request->get('id'));
        }else{
            $articulo = new Patrocinador;
        } 
        $articulo->name = $request->get('name');
        $articulo->web = $request->get('web');
        $articulo->email = $request->get('email');
        $articulo->telephone1 = $request->get('tlf1');
        $articulo->telephone2 = $request->get('tlf2');
        $articulo->description = $request->get('descripcion');
        $articulo->address = $request->get('address');
        $articulo->ubication = $request->get('ubication');
        $articulo->deleted_at = ($request->get('state'))?null:date('Y-m-d H:i:s');
        $articulo->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyp($id)
    {
        $p =Patrocinador::withTrashed()->find($id);
        if ($p->deleted_at == null) {
            $p->delete();
        }else{
            $p->restore();
        }
        return back();
    }

    public $folder = '/uploads/patrocinadores/';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imgUpload(Request $request)
    {
        // create upload path if it does not exist
        $path = public_path($this->folder.$request->get('id')."/");
        if(!File::exists($path)) {
            File::makeDirectory($path);
        };
        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple($path);
        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();
        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['files'], $_SERVER);
        $slugGenerator = new FileUpload\FileNameGenerator\Slug();
        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);
        $fileupload->setFileNameGenerator($slugGenerator);
        
        // Doing the deed
        list($files, $headers) = $fileupload->processAll();
        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }
        foreach($files as $file){
            //Remember to check if the upload was completed
            if ($file->completed) {
                // set some data
                $filename = $file->getFilename();
                $url = $this->folder.$request->get('id')."/". $filename;
                
                if ($request->get('typeimg') == 'true') {
                    $affectedRows = ImgPatrocinador::where('patrocinador_id', $request->get('id'))->update(['type' => 's']);
                }

                // save data
                $picture = ImgPatrocinador::create([
                    'url' => $url,
                    'title' => $filename,
                    'patrocinador_id' => $request->get('id'),
                    'creditos' => "",
                    'type' => ($request->get('typeimg') == 'true')?'p':'s'
                ]);
                
                // prepare response
                $data[] = [
                    'size' => $file->size,
                    'name' => $request->get('description'),
                    'type' => ($request->get('typeimg')== 'true')?'p':'s',
                    'url' => $url,
                    'thumbnailUrl' => Croppa::url($url, 80, 80, ['resize']),
                    'deleteType' => 'DELETE',
                    'deleteUrl' => route('picturesp.destroy', $picture->id)
                ];
                
                // output uploaded file response
                return response()->json(['files' => $data]);
            }
        }
        // errors, no uploaded file
        return response()->json(['files' => $files]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getimg(Request $request)
    {
        // get all pictures
        $pictures = ImgPatrocinador::where('patrocinador_id',($request->get('id')?$request->get('id'):0))->get();
        
        // add properties to pictures
        $pictures->map(function ($picture) {
            $picture['size'] = File::size(public_path($picture['url']));
            $picture['thumbnailUrl'] = Croppa::url($picture['url'], 80, 80, ['resize']);
            $picture['deleteType'] = 'DELETE';
            $picture['name'] = $picture['title'];
            $picture['type'] = $picture['type'];
            $picture['value'] = $picture->id;
            $picture['deleteUrl'] = route('picturesp.destroy', $picture->id);
            return $picture;
        });
        
        // show all pictures
        return response()->json(['files' => $pictures]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $picture = ImgPatrocinador::find($id);
        Croppa::delete($picture->url); // delete file and thumbnail(s)
        $picture->delete(); // delete db record
        return response()->json([$picture->url]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function principal(Request $request)
    {
        $picture = ImgPatrocinador::find($request->get('id'));
        $pictures = ImgPatrocinador::where('patrocinador_id',$picture->patrocinador_id)->update(['type' => 's']);
        $picture->type = 'p';
        $picture->save();
        return $picture->id;
    }
}
